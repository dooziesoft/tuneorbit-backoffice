$(function(){
  $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
  });

  $("form[data-confirm]").submit(function() {
    $('body').css('opacity','0.1');
      if (!confirm($(this).attr("data-confirm"))) {
        $('body').css('opacity','1');
        return false;
      }
  });

  // $(document.body).append('<div id="preloader"><div class="cssload-thecube"><div class="cssload-cube cssload-c1"></div><div class="cssload-cube cssload-c2"></div><div class="cssload-cube cssload-c4"></div><div class="cssload-cube cssload-c3"></div></div></div>');
  $(document.body).append('<div id="preloader"><div class="loader"><div class="circle"></div><div class="circle"></div><div class="circle"></div></div></div>');
   $(document).ajaxStop(function () {
        $('#preloader').hide();
    });

    $(document).ajaxStart(function () {
        $('#preloader').show();
    });
  $('#preloader').hide();


  Date.prototype.getMonthName = function(lang) {
      lang = lang && (lang in Date.locale) ? lang : 'en';
      return Date.locale[lang].month_names[this.getMonth()];
  };

  Date.prototype.getMonthNameShort = function(lang) {
      lang = lang && (lang in Date.locale) ? lang : 'en';
      return Date.locale[lang].month_names_short[this.getMonth()];
  };

  Date.locale = {
      en: {
         month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
         month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      }
  };

  Date.prototype.sameDay = function(d) {
  return this.getFullYear() === d.getFullYear()
    && this.getDate() === d.getDate()
    && this.getMonth() === d.getMonth();
  }

});

function getFormatedDate(date)
{
  var d=new Date(date);
  var day=d.getDate();
  var month=d.getMonthNameShort();
  var y=d.getFullYear();

  return (day+'-'+month+'-'+y);

}

function getYearMonth(date)
{
  var d=new Date(date);
  var day=d.getDate();
  // var month=d.getMonthNameShort();
  var m=d.getMonth();
  var y=d.getFullYear();

  return (y+''+(("0" + (m + 1)).slice(-2)));

}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}
function getDateDifferenceInDays(date1,date2){
  date1=new Date(date1);
  date2=new Date(date2);
  var diff = new Date(date1 - date2);
  var days = diff/1000/60/60/24;
  var x=(days.toString()).split('.');
  return x[0];
}
  function convert_number(number)
{
    if ((number < 0) || (number > 999999999)) 
    { 
        return "NUMBER OUT OF RANGE!";
    }
    var Gn = Math.floor(number / 10000000);  /* Crore */ 
    number -= Gn * 10000000; 
    var kn = Math.floor(number / 100000);     /* lakhs */ 
    number -= kn * 100000; 
    var Hn = Math.floor(number / 1000);      /* thousand */ 
    number -= Hn * 1000; 
    var Dn = Math.floor(number / 100);       /* Tens (deca) */ 
    number = number % 100;               /* Ones */ 
    var tn= Math.floor(number / 10); 
    var one=Math.floor(number % 10); 
    var res = ""; 

    if (Gn>0) 
    { 
        res += (convert_number(Gn) + " CRORE"); 
    } 
    if (kn>0) 
    { 
            res += (((res=="") ? "" : " ") + 
            convert_number(kn) + " LAKH"); 
    } 
    if (Hn>0) 
    { 
        res += (((res=="") ? "" : " ") +
            convert_number(Hn) + " THOUSAND"); 
    } 

    if (Dn) 
    { 
        res += (((res=="") ? "" : " ") + 
            convert_number(Dn) + " HUNDRED"); 
    } 


    var ones = Array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX","SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN","FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN","NINETEEN"); 
var tens = Array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY","SEVENTY", "EIGHTY", "NINETY"); 

    if (tn>0 || one>0) 
    { 
        if (!(res=="")) 
        { 
            res += " AND "; 
        } 
        if (tn < 2) 
        { 
            res += ones[tn * 10 + one]; 
        } 
        else 
        { 

            res += tens[tn];
            if (one>0) 
            { 
                res += ("-" + ones[one]); 
            } 
        } 
    }

    if (res=="")
    { 
        res = "zero"; 
    } 
    return res;
}
function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}




