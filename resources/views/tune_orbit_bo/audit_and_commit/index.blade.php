@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'Manage Audit Service Page')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">List of Services for Audit </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>

      <div class="box-body table-responsive no-padding">
        
        <div class="clearfix"></div>
        <div class="col-md-12">
          <!-- <div class="pull-right">

          <a type="button" class="btn bg-navy margin" id="create_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Click here to audit Service Page">
            <i class="glyphicon glyphicon-pencil"></i> Audit Service Page
          </a>            
                  
        </div><div class="clearfix"></div> -->
        <table class="table table-bordered" id="view">
          <thead>
            <tr class="bg-blue">
              <th></th>
              <th>Service</th>
              <th>Sub Service</th>
              <th>Business Name</th>
              <th>Address</th>
              <th>Phone Number</th>
              <th>Website</th>
            </tr>  

            <tr tr class="bg-blue">
            <th></th>
            <th><input type="text" class="form-control filter" data-col="Service"></th>
            <th><input type="text" class="form-control filter" data-col="Sub Service"></th>
            <th><input type="text" class="form-control filter" data-col="Business Name"></th>
            <th><input type="text" class="form-control filter" data-col="Address"></th>
            <th><input type="text" class="form-control filter" data-col="Phone Number"></th>
            <th><input type="text" class="form-control filter" data-col="Website"></th>
             </tr>
        </thead>
          <tbody>
            @foreach($service_pages as $sp)
            <tr>
              <td><a type="button" href="{{URL::to('/')}}/tune_orbit_bo/service_page/audit_and_commit/{{$sp->id}}/edit" class="btn bg-navy margin" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Click to view and audit">
                <i class="glyphicon glyphicon-eye-open"></i>
              </a></td>
              
              <td>{{$sp->service_name}}</td>
              <td>{{$sp->sub_service_name}}</td>
              <td>{{$sp->business_name}}</td>
              <td>{{$sp->address}}</td>
              <td>{{$sp->phone_number}}</td>
              <td>{{$sp->website}}</td>

            </tr>
            @endforeach
          </tbody>
          <div class="clearfix"></div>

        </table>
     

        <div class="clearfix"></div>
        
        
      </div>
      <div class="box-footer">
        {{-- <form action='#'>
        <input type='text' placeholder='New task' class='form-control input-sm' />
      </form> --}}
    </div><!-- /.box-footer-->
  </div><!-- /.box -->
</div><!-- /.col -->


</div><!-- /.row -->
@endsection

@section('script')
@parent

<script type="text/javascript">

 $(function(){
  $('.filter').multifilter({'target':$('#view')});
  $('input[name=ch]:radio').attr('checked',false);
  $('input[name=ch]:radio').change(function(){
    var id=$(this).attr('id');
    var status=$(this).attr('status');
   
    $("#create_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/service_page/audit_and_commit/"+id+"/edit");
    $("#create_btn").attr('onclick',"");
   

    
  });
  @if(Session::has('message'))
  $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
  @endif
});
 function no_select(){
    $.notify("Please select row from below table",{
      type:'danger',
    });
  }
  function confirm_delete(status){
    if (!confirm('Do you really want to '+status+' services?')) {
    return false;
  }
}

</script>
@stop