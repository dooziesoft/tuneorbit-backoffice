@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Audit Service Page')

@section('page_title_sub', 'Audit Service Page')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Audit Service Page Here</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::open(array('route' => array('tune_orbit_bo.service_page.audit_and_commit.store'), 'method' => 'POST','files'=>true,'id'=>'add-form','onsubmit'=>'return validate()'))!!}
            
               <div class="col-md-4">
             {!!Form::label('service_id','Service Name*')!!}
              {!!Form::text('service_name',$UserService->service_name,['class' => 'form-control','disabled'=>'true'])!!}
            </div>

              <div class="col-md-4">
             {!!Form::label('sub_service_name','Sub Service Name*')!!}
              {!!Form::text('sub_service_name',$UserService->sub_service_name,['class' => 'form-control','disabled'=>'true'])!!}
            </div>

              <div class="col-md-4">
             {!!Form::label('business_name','Business Name*')!!}
              {!!Form::text('business_name',$UserService->business_name,['class' => 'form-control','disabled'=>'true'])!!}
            </div>

             <div class="col-md-4">
             {!!Form::label('business_name','Address *')!!}
              {!!Form::input('textarea','business_name',$UserService->address,['class' => 'form-control','disabled'=>'true','row'=>3])!!}
            </div>

            
     


             {!!Form::hidden('checkpoint_array',null,['id'=>'checkpoint_array'])!!}
             {!!Form::hidden('user_service_id',$user_service_id,['id'=>'checkpoint_array'])!!}
       {!!Form::hidden('update_array',null,['id'=>'update_array'])!!}


                <div class="col-md-10" style="margin-top: 20px;">
        <table class="table table-bordered" id="process_table" >
          <thead>
            <tr class="bg-blue">
              {{-- <th></th> --}}
              <th style="width:150px">Checkpoint</th>
              <th style="width:50px">Select</th>
              <th style="width:400px;">Details</th>
            </tr>
          </tr>
        </tr>
      </thead>
      <tbody id="checkpoint_row">
        @foreach($checkpoints as $cp)
        <tr>
        <?php $in=$cp->service_feilds ?>
        {{--   <td><input type="checkbox"  name='check_id' id='check_id' class='flat-red'><input type='hidden' id='audit_checkpoint_id' name='audit_checkpoint_id' value='{{$cp->id}}'></td> --}}
           <td>{{$cp->checkpoint}} <i class="fa fa-info-circle info" data-toggle="tooltip" title="{{$cp->info}}"  data-placement="right"></i> 
           <input type='hidden' id='audit_checkpoint_id' name='audit_checkpoint_id' value='{{$cp->id}}'></td>

           <td>
          <input type="radio" name="select_{{$cp->id}}" class="flat-red" value="1" checked="true">Yes<br/>
          <input type="radio" name="select_{{$cp->id}}" class="flat-red" value="0">No
           </td>
          <td>
           @if($cp->type_flag == 'textarea' || $cp->type_flag == 'editor')
           <textarea  style="width:400px;" name="{{$cp->service_feilds}}" id="{{$cp->service_feilds}}">{{$UserService->$in}}</textarea>
           <input type="hidden" name="update_flag" value="1">
        
           @endif

             @if($cp->type_flag == 'number')
           <input type="text" style="width:400px;" name="{{$cp->service_feilds}}" value="{{$UserService->$in}}" class="form-control required number">
           <input type="hidden" name="update_flag" value="1">
           @endif

            @if($cp->type_flag == 'img')
           <img style="height:200px;width:200px" src='{{$UserService->$in}}'>
           <input type="text" style="width:400px;" name="{{$cp->service_feilds}}" value="{{$UserService->$in}}" hidden>
           <input type="hidden" name="update_flag" value="0">
           @endif

            @if($cp->type_flag == 'button')
           <button type="button" onclick="showOpenHoursModal()"> Edit Open Hours</button>
          <input type="text" style="width:400px;" name="{{$cp->service_feilds}}" value="" hidden>
          <input type="hidden" name="update_flag" value="0">
           @endif

          </td>
        </tr>
        @endforeach 
      </tbody>

    </table>
  </div>



<div class='clearfix'></div>
<div class="col-md-2 pull-right">
  <a href="{{URL::route('tune_orbit_bo.service_page.audit_and_commit.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
</div>
<div class="col-md-2 pull-right">
  <div class="form-group">
    {!! Form::submit('Commit', ['class' => 'btn btn-block btn-success btn-block']) !!}
  </div>
</div> 

          {!!Form::close()!!}
        </div>

<div class="modal fade" id="open_hors_modal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog"  style="margin-left: 250px"   >
        <div class="modal-content" style="width:1000px;">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                  Open Hours 
                   <span id="show_from_date"></span> 
                   to <span id="show_to_date"></span>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
              {!!Form::open(array('id'=>'open_hours_modal_form','name'=>'open_hours_modal_form'))!!}
        

               {!!Form::hidden('user_service_id',$user_service_id)!!}

             <div class="col-md-6 table-responsive" >
{!!Form::label('website','Working Days')!!}
  <table class="table table-hover table-bordered">
    <tr>
      <th colspan="5">
        <div class="pull-right">
          <label class="checkbox-inline">
          <input type="checkbox" name="open_24x7" id="open_24x7" onclick="disableTableInputs()" ><b>Open 24x7</b></label>
          </div>
        </th>
      </tr>
      <tr>
        <th><input type="checkbox" id="check_all" name="check_all" ></th>
        <th>Day</th>
        <th>From Time</th>
        <th>To Time</th>
        <th>Open 24 hours</th>
      </tr>
      <tbody id="working_days_row">
        <tr>
          <td><input type="checkbox" id="sun" name="sun"></td>
          <td>Sun</td>
          <td style="padding:2px;">
          <input type="text"
          id="sun_from_time" name="sun_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sun_to_time" name="sun_to_time"  placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sun_open_24_hours" name="sun_open_24_hours" onclick="disableFromToTime('sun')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="mon" name="mon"></td>
          <td>Mon</td>
          <td style="padding:2px;">
          <input type="text" id="mon_from_time" name="mon_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="mon_to_time" name="mon_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="mon_open_24_hours" name="mon_open_24_hours" onclick="disableFromToTime('mon')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="tue" name="tue"></td>
          <td>Tue</td>
          <td style="padding:2px;">
          <input type="text" id="tue_from_time" name="tue_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="tue_to_time" name="tue_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="tue_open_24_hours" name="tue_open_24_hours" onclick="disableFromToTime('tue')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="wed" name="wed"></td>
          <td>Wed</td>
          <td style="padding:2px;">
          <input type="text" id="wed_from_time" name="wed_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="wed_to_time" name="wed_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="wed_open_24_hours" name="wed_open_24_hours" onclick="disableFromToTime('wed')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="thu" name="thu"></td>
          <td>Thu</td>
          <td style="padding:2px;">
          <input type="text" id="thu_from_time" name="thu_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="thu_to_time" name="thu_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="thu_open_24_hours" name="thu_open_24_hours" onclick="disableFromToTime('thu')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="fri" name="fri"></td>
          <td>Fri</td>
          <td style="padding:2px;">
          <input type="text" id="fri_from_time" name="fri_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="fri_to_time" name="fri_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="fri_open_24_hours" name="fri_open_24_hours" onclick="disableFromToTime('fri')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="sat" name="sat"></td>
          <td>Sat</td>
          <td style="padding:2px;">
          <input type="text" id="sat_from_time" name="sat_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sat_to_time" name="sat_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sat_open_24_hours" name="sat_open_24_hours" onclick="disableFromToTime('sat')"></td>
        </tr>
      </tbody>
    </table>
  </div>
 {!!Form::close()!!}
            </div>
            <div class="clearfix"></div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal" id="cancel_modal_btn" onclick="ClearModal">
                            Cancel
                </button>
                <button type="button" class="btn btn-primary" onclick="UpdateOpenHours()">
                    Update Open Hours
                </button>
            </div>
        </div>
    </div>
</div>

        <div class="box-footer">

        </div><!-- /.box-footer-->
      </div><!-- /.box -->
    </div><!-- /.col -->

  </div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
   $('[data-toggle="popover"]').popover(); 

   tinymce.init({
        selector: '#about',
        height: 500,
            theme: 'modern',
            plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
            ],
           toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
           toolbar2: 'print preview media | forecolor backcolor emoticons fontsizeselect jbimages',
           image_advtab: true,
           fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
           templates: [
           { title: 'Test template 1', content: 'Test 1' },
           { title: 'Test template 2', content: 'Test 2' }
           ],
           content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
           ],
            font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
           relative_urls: false
    });



   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif




   });

  var service_page = <?php echo json_encode($UserService) ?>;

var mandatory_checkpoints=<?php echo $mandatory_checkpoints ?>;
var checkpoint_array={},inc=0;
var update_array={},update_inc=0;

  function validate()
  {
   var flag=0;
    if($("#add-form").valid()){
        $('#checkpoint_row > tr').each(function(){
        var x={};
        x=$(this).find('input,textarea,radio').serializeArray();
          console.log("x");
          console.log(x);
            if(x[3]['value'] == 1){
            update_array[update_inc]={};
            update_array[update_inc][x[2]['name']]=x[2]['value'];
            update_inc++;
            }
            checkpoint_array[inc]={};
            checkpoint_array[inc]['audit_checkpoint_id']=x[0]['value'];
            checkpoint_array[inc]['select_flag']=x[1]['value'];
            checkpoint_array[inc][x[2]['name']]=x[2]['value'];
            inc++;
            flag=1;
   
      });
        
   
        if(Number(flag) == 1){
          console.log(checkpoint_array);
        $('#checkpoint_array').val(JSON.stringify(checkpoint_array));
           $('#update_array').val(JSON.stringify(update_array));
        return true;
        }


 
    }else{
      return false;
    }
    

}

function UpdateOpenHours(){

console.log($('#open_hours_modal_form').serialize());
  $.ajax({
    url: '{{URL::route("updateOpenHours")}}',
    type: 'POST',
    dataType: 'json',
    data: $('#open_hours_modal_form').serialize(),
  })
  .done(function(result) {
    $("#open_hors_modal").modal('hide');

  });
}

function showOpenHoursModal(){
$("#open_hors_modal").modal('show');

console.log(service_page);
  if(service_page['open_24x7'] == 1){
        $('#open_24x7').attr('checked',true);
        $('#working_days_row').find('input').attr('disabled',true);
        $("#check_all").attr('disabled',true);
      }else{
        $('#open_24x7').attr('checked',false);
      var working_day_str=service_page['working_days'].split(',');
      var from_time_str=service_page['from_time'].split(',');
      var to_time_str=service_page['to_time'].split(',');
      if(working_day_str[6].split(':').pop() == 'true}"'){
        $('#sun').attr('checked',true);
        if(from_time_str[6].split(':').pop() == 'open_24_hours}'){
          $('#sun_open_24_hours').attr('checked',true);
          $('#sun_from_time').val('---');
          $('#sun_to_time').val('---');
          $('#sun_from_time').attr('disabled',true);
          $('#sun_to_time').attr('disabled',true);
        }else{
          $('#sun_open_24_hours').attr('checked',false);
          $('#sun_from_time').val(from_time_str[6].substring(6).slice(0,-1));
          $('#sun_to_time').val(to_time_str[6].substring(6).slice(0,-1));
        }
      }else{
        $('#sun').attr('checked',false);
      }
      if(working_day_str[0].split(':').pop() == 'true'){
        $('#mon').attr('checked',true);
        if(from_time_str[0].split(':').pop() == 'open_24_hours'){
          $('#mon_open_24_hours').attr('checked',true);
          $('#mon_from_time').val('---');
          $('#mon_to_time').val('---');
          $('#mon_from_time').attr('disabled',true);
          $('#mon_to_time').attr('disabled',true);
        }else{
          $('#mon_open_24_hours').attr('checked',false);
          $('#mon_from_time').val(from_time_str[0].substring(7));
          $('#mon_to_time').val(to_time_str[0].substring(7));
        }
      }else{
        $('#mon').attr('checked',false);
      }
      if(working_day_str[1].split(':').pop() == 'true'){
        $('#tue').attr('checked',true);
        if(from_time_str[1].split(':').pop() == 'open_24_hours'){
          $('#tue_open_24_hours').attr('checked',true);
          $('#tue_from_time').val('---');
          $('#tue_to_time').val('---');
          $('#tue_from_time').attr('disabled',true);
          $('#tue_to_time').attr('disabled',true);
        }else{
          $('#tue_open_24_hours').attr('checked',false);
          $('#tue_from_time').val(from_time_str[1].substring(6));
          $('#tue_to_time').val(to_time_str[1].substring(6));
        }
      }else{
        $('#tue').attr('checked',false);
      }
      if(working_day_str[2].split(':').pop() == 'true'){
        $('#wed').attr('checked',true);
        if(from_time_str[2].split(':').pop() == 'open_24_hours'){
          $('#wed_open_24_hours').attr('checked',true);
          $('#wed_from_time').val('---');
          $('#wed_to_time').val('---');
          $('#wed_from_time').attr('disabled',true);
          $('#wed_to_time').attr('disabled',true);
        }else{
          $('#wed_open_24_hours').attr('checked',false);
          $('#wed_from_time').val(from_time_str[2].substring(6));
          $('#wed_to_time').val(to_time_str[2].substring(6));
        }
      }else{
        $('#wed').attr('checked',false);
      }
      if(working_day_str[3].split(':').pop() == 'true'){
        $('#thu').attr('checked',true);
        if(from_time_str[3].split(':').pop() == 'open_24_hours'){
          $('#thu_open_24_hours').attr('checked',true);
          $('#thu_from_time').val('---');
          $('#thu_to_time').val('---');
          $('#thu_from_time').attr('disabled',true);
          $('#thu_to_time').attr('disabled',true);
        }else{
          $('#thu_open_24_hours').attr('checked',false);
          $('#thu_from_time').val(from_time_str[3].substring(6));
          $('#thu_to_time').val(to_time_str[3].substring(6));
        }
      }else{
        $('#thu').attr('checked',false);
      }
      if(working_day_str[4].split(':').pop() == 'true'){
        $('#fri').attr('checked',true);
        if(from_time_str[4].split(':').pop() == 'open_24_hours'){
          $('#fri_open_24_hours').attr('checked',true);
          $('#fri_from_time').val('---');
          $('#fri_to_time').val('---');
          $('#fri_from_time').attr('disabled',true);
          $('#fri_to_time').attr('disabled',true);
        }else{
          $('#fri_open_24_hours').attr('checked',false);
          $('#fri_from_time').val(from_time_str[4].substring(6));
          $('#fri_to_time').val(to_time_str[4].substring(6));
        }
      }else{
        $('#fri').attr('checked',false);
      }
      if(working_day_str[5].split(':').pop() == 'true'){
        $('#sat').attr('checked',true);
        if(from_time_str[5].split(':').pop() == 'open_24_hours'){
          $('#sat_open_24_hours').attr('checked',true);
          $('#sat_from_time').val('---');
          $('#sat_to_time').val('---');
          $('#sat_from_time').attr('disabled',true);
          $('#sat_to_time').attr('disabled',true);
        }else{
          $('#sat_open_24_hours').attr('checked',false);
          $('#sat_from_time').val(from_time_str[5].substring(6));
          $('#sat_to_time').val(to_time_str[5].substring(6));
        }
      }else{
        $('#sat').attr('checked',false);
      }
    }
}
function disableTableInputs(){
  if($('#open_24x7').is(':checked')){
    $('#check_all').attr('disabled',true);
    $('#check_all').attr('checked',false);
     $('#working_days_row').find('input').attr('disabled',true);
     $('#working_days_row').find('input').val('');
     $('#working_days_row').find('input[type=checkbox]:checked').attr('checked',false);
   }else{
    $('#check_all').attr('disabled',false);
   $('#working_days_row').find('input').attr('disabled',false);
 }         
}

function disableFromToTime(day){
    var str=day+'_open_24_hours';
    var from_str=day+'_from_time';
    var to_str=day+'_to_time';
    if($('#'+str).is(':checked')){
      $('#'+from_str).attr('disabled',true);
      $('#'+from_str).val('');
      $('#'+to_str).attr('disabled',true);
      $('#'+to_str).val('');
    }else{
      $('#'+from_str).attr('disabled',false);
      $('#'+from_str).val('');
      $('#'+to_str).attr('disabled',false);
      $('#'+to_str).val('');
    }
  }

































































function showDetails(audit_checkpoint_id,type,service_feilds){
 
var s=<?php echo json_encode($UserService)  ?>;
var value=s[service_feilds];

 var select=$("#select_"+audit_checkpoint_id).val();
if(Number(select) == 1 ){
 if(type == 'textarea'){
  var dis='';
      dis+='<textarea  style="width:400px;" name="'+service_feilds+'">'+value+'</textarea>';
      $("#show_block_"+audit_checkpoint_id).html(dis);
 }
 if(type == 'number'){
  var dis='';
      dis+='<input  style="width:200px;" name="'+service_feilds+'" value='+value+'> ';
      $("#show_block_"+audit_checkpoint_id).html(dis);
 } 
 if(type == 'img'){
  var dis='';
      dis+='<img style="height:200px;width:200px" src='+value+'>';
      $("#show_block_"+audit_checkpoint_id).html(dis);
 } 
 if(type == 'editor'){

  var dis='';
      dis+='<textarea  style="width:400px;" name="'+service_feilds+'"  id="'+service_feilds+'">'+value+'</textarea>';
      $("#show_block_"+audit_checkpoint_id).html(dis);

       tinymce.init({
        selector: '#about',
        height: 500,
            theme: 'modern',
            plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
            ],
           toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
           toolbar2: 'print preview media | forecolor backcolor emoticons fontsizeselect jbimages',
           image_advtab: true,
           fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
           templates: [
           { title: 'Test template 1', content: 'Test 1' },
           { title: 'Test template 2', content: 'Test 2' }
           ],
           content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
           ],
            font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
           relative_urls: false
    });

 }

}
else{

 var dis='';
      dis+='<textarea style="width:400px;" name="'+service_feilds+'" hidden></textarea>';
      $("#show_block_"+audit_checkpoint_id).html(dis);



}
}




















function showRemarks(audit_checkpoint_id){
console.log(audit_checkpoint_id);
var select=$("#select_"+audit_checkpoint_id).val();
if(Number(select) == 1 ){
 $.ajax({
              type:'POST',
              url:"{{route('getspecificServiceAuditRemark')}}",
              data:{audit_checkpoint_id:audit_checkpoint_id,user_service_id:{!! $user_service_id !!}},
               
            }).done(function(result){
               console.log(result);
               if(result['checkpoints']){
          $("#remarks_"+audit_checkpoint_id).text(result['checkpoints']['remarks']);
    
    $("#remarks_"+audit_checkpoint_id).addClass('required');
    $("#service_audit_id_"+audit_checkpoint_id).val(result['checkpoints']['service_audit_id']);

    
  }
       
            });
}else{
  $("#remarks_"+audit_checkpoint_id).text('');
  $("#remarks_"+audit_checkpoint_id).removeClass('required');
  $("#service_audit_id_"+audit_checkpoint_id).val(0);

}

}
  function Old_validate()
  {
    var flag=1;
    var length=$("input[type='checkbox'][name='check_id']:checked").length;
    if(length>0){
var checked_mandatory=0;
      $('#checkpoint_row > tr').each(function(){
        var x={};
        x=$(this).find('input,textarea').serializeArray();
          console.log("x");
          console.log(x);
        if (x[0]['value']=='on'){
          if(x[2]['value']==1){
            checkpoint_array[inc]={};
            checkpoint_array[inc]['audit_checkpoint_id']=x[1]['value'];
            checkpoint_array[inc]['remarks']=x[3]['value'];
            inc++;
            checked_mandatory++;
          }else{
            checkpoint_array[inc]={};
            checkpoint_array[inc]['audit_checkpoint_id']=x[1]['value'];
            checkpoint_array[inc]['remarks']=x[3]['value'];
            inc++;
          }
        }
      });

      if(checked_mandatory!=mandatory_checkpoints){
          $.notify({message: 'Please select all mandatory(*) checkpoint from below table'},{ type: 'danger'});
          flag=0;
          return false;
      }

  }else{

    $.notify({message: 'Please select checkpoint from below table'},{ type: 'danger'});
    flag=0;
    return false;

  }



if(flag){
  $('#checkpoint_array').val(JSON.stringify(checkpoint_array));
    return true;
}else{
  return false;
}

}
 </script>
 @stop
