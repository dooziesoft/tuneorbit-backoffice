<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/tune_orbit_bo/admin-lte/dist/img/TuneOrbit_logo.png") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{!! Auth::user()->name !!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        {{-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
<span class="input-group-btn">
  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
</span>
            </div>
        </form> --}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{URL::route('dashboard')}}"><span>Dashboard</span></a></li>

            <li ><a href="{{URL::route('tune_orbit_bo.excel_import.import.index')}}"><span> Excel Import Services</span></a></li>
      
            <li><a href="{{URL::route('tune_orbit_bo.service_page.services.index')}}">New Services</a></li>
            
            <li><a href="{{URL::route('tune_orbit_bo.service_page.view_and_commit.index')}}">Commit Service Page</a></li>
            
            <li><a href="{{URL::route('tune_orbit_bo.service_page.audit_and_commit.index')}}">Audit Service Page</a></li>
            
            <li><a href="{{URL::route('tune_orbit_bo.service_page.manage_services.index')}}">Manage Services</a></li>
                
            

          

             <li class="treeview">
                <a href="#"><span><i class=" fa fa-user"></i> Masters</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                <li><a href="{{URL::route('tune_orbit_bo.masters.services.index')}}">Services</a></li>
                    <li><a href="{{URL::route('tune_orbit_bo.masters.sub_services.index')}}">Sub Services</a></li>
                   
                </ul>
            </li>


             
                   
                </ul>
    </section>
    <!-- /.sidebar -->
</aside>