@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'Manage Service Page')

@section('content')
    <div class="clearfix"></div>
    <div class='row'>
      <div class="col-md-12" id="filter-box">

          <div class="box box-danger">

              <div class="box-header">
                 <div class="pull-right box-tools">
                    <button class="btn btn-danger btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse">

                          <i class="fa fa-minus"></i>

                      </button>

                  </div>

                  <!-- /. tools -->

                  <i class="fa fa-cloud"></i>



                  <h3 class="box-title">Filter</h3>

              </div>

              <!-- /.box-header -->
    
              <div class="box-body no-padding">
                    {!!Form::open(array('route' => array('service_page.manage_services.filter'), 'method' => 'POST','files'=>true,'id'=>'add-form','onsubmit'=>'return validate()'))!!}
                 <div class="col-md-4">
 <div class="form-group @if($errors->first('service_id')) has-error @endif">

   {!!Form::label('service_id','Service Name*')!!}
   {!! Form::servicesSelect('service_id',null) !!}
   <small class="text-danger">{{ $errors->first('service_id') }}</small>

 </div>
</div>

<div class="col-md-4" id="sub_service_div">
 <div class="form-group @if($errors->first('sub_service_id')) has-error @endif">

   {!!Form::label('sub_service_id','Sub Service Name*')!!}
   {!!Form::select('sub_service_id', array(),null, ['class' => 'form-control required','id'=>'sub_service_id', 'data-live-search' => 'true']) !!} 
   <small class="text-danger">{{ $errors->first('sub_service_id') }}</small>

 </div>
</div>

<div class="col-md-4">
  <div class="form-group @if($errors->first('business_name')) has-error @endif">
   {!!Form::label('business_name','Business Name')!!}
   {!!Form::text('business_name',Input::old('business_name'),['class' => 'form-control required','id'=>'business_name',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Business Name","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('business_name') }}</small>
 </div>
</div>
       
                 <div class="clearfix"></div>
           
                 
          
              </div>

              <!-- /.box-body -->

              <div class="clearfix"></div>

              <div class="box-footer">

                  <div class="row">
                  <div class="col-md-2 pull-right">
                          <a href="{{URL::route('tune_orbit_bo.service_page.manage_services.index')}}">{!! Form::button('Clear', ['class' => 'btn btn-sm btn-danger btn-block']) !!}</a>
                        </div>

<div class="col-md-2 pull-right">
                    {!! Form::submit('Search', ['class' => 'btn btn-sm btn-success btn-block']) !!}
                 </div>
                 
                     {!!Form::close()!!}
                        


                  </div>

              </div>

              <!-- /.box-footer --> </div>

          <!-- /.box --> </div>


  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">List of Services</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>

      <div class="box-body table-responsive no-padding">
        
        <div class="clearfix"></div>
        <div class="col-md-12">
          <div class="pull-right">

            

           <a type="button" class="btn bg-navy margin" id="edit_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Edit">
            <i class="glyphicon glyphicon-eye-open"></i> View and Update 
          </a>

           <a type="button" class="btn bg-purple margin" id="push_pull_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Push/Pull Website">
            <i class="glyphicon glyphicon-briefcase"></i> Push/Pull Website 
          </a>
                  
        </div><div class="clearfix"></div>
        <table class="table table-bordered" id="view">
          <thead>
            <tr class="bg-blue">
              <th></th>
              <th>Service</th>
              <th>Sub Service</th>
              <th>Business Name</th>
              <th>Address</th>
              <th>Locality</th>
              <th>Phone Number</th>
              <th>Website</th>
              <th>Aduit</th>
              <th>Status</th>
            </tr>  

            <tr tr class="bg-blue">
            <th></th>
            <th><input type="text" class="form-control filter" data-col="Service"></th>
            <th><input type="text" class="form-control filter" data-col="Sub Service"></th>
            <th><input type="text" class="form-control filter" data-col="Business Name"></th>
            <th><input type="text" class="form-control filter" data-col="Address"></th>
            <th><input type="text" class="form-control filter" data-col="Locality"></th>

            <th><input type="text" class="form-control filter" data-col="Phone Number"></th>
            <th><input type="text" class="form-control filter" data-col="Website"></th>
            <th><input type="text" class="form-control filter" data-col="Aduit"></th>
            <th><input type="text" class="form-control filter" data-col="Status"></th>
             </tr>
        </thead>
          <tbody>
            @foreach($service_pages as $sp)
            <tr>
              <td><input type="radio" id='{{$sp->id}}' name='ch' status="{{$sp->in_website}}"></td>
              <td>{{$sp->service_name}}</td>
              <td>{{$sp->sub_service_name}}</td>
              <td>{{$sp->business_name}}</td>
              <td>{{$sp->address}}</td>
              <td>{{$sp->locality}}</td>
              <td>{{$sp->phone_number}}</td>
              <td>{{$sp->website}}</td>
              @if($sp->audit_flag)
              <td><i class="fa fa-check text-green"></i></td>
              @else
              <td><i class="fa fa-times-circle-o text-red"></i></td>
              @endif

              @if($sp->in_website)
              <td style="color: green;">In Website</td>
              @else
              <td style="color: red;">Not In Website</td>
              @endif
            </tr>

            @endforeach
          </tbody>
          
          
          
        </table>
        
   {!! $service_pages->render() !!} 
         
        
        
         
     

        <div class="clearfix"></div>
        
        
      </div>
      <div class="box-footer">
        {{-- <form action='#'>
        <input type='text' placeholder='New task' class='form-control input-sm' />
      </form> --}}
    </div><!-- /.box-footer-->
  </div><!-- /.box -->
</div><!-- /.col -->


</div><!-- /.row -->
</div>
@endsection

@section('script')
@parent

<script type="text/javascript">

 $(function(){
  $('.filter').multifilter({'target':$('#view')});
  $('input[name=ch]:radio').attr('checked',false);
  $('input[name=ch]:radio').change(function(){
    var id=$(this).attr('id');
    var status=$(this).attr('status');
    $("#edit_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/service_page/manage_services/"+id+"/edit");
    $("#edit_btn").attr('onclick',"");
    
    $("#push_pull_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/service_page/manage_services/push_pull_service/"+id);
    if(status == 0){
      $("#push_pull_btn").attr('onclick',"return push_pull_service('Push');");
    }else{
      $("#push_pull_btn").attr('onclick',"return push_pull_service('Pull');");
    }

    
  });
  @if(Session::has('message'))
  $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
  @endif

    $('#service_id').change(function(){
    $('#sub_service_id').val('');
    $('#sub_service_div').html('<label>Sub Service Name*</label><select type="text" class="form-control"  id="sub_service_id" name="sub_service_id" data-live-search="true"></select>')
    var service_id=$('#service_id').val();
    $.ajax({
      type: 'get',
      url:'{{URL::route("service_page.services.getspecificSubservices")}}',
      dataType: 'json',
      data:{service_id:service_id},
    }).done(function(result){
     console.log(result);
     var sub_services=result['sub_services'];
     var dis='<option value=0>Select Sub Services</option>';
     for(var i=0;i<sub_services.length;i++){
      dis+='<option value='+sub_services[i]['id']+'>'+sub_services[i]['name']+'</option>';
     }
     $('#sub_service_id').html(dis).selectpicker();
    });

   })


});
 function no_select(){
    $.notify("Please select row from below table",{
      type:'danger',
    });
  }
  function push_pull_service(status){
    if (!confirm('Do you really want to '+status+' services?')) {
    return false;
  }
}

</script>
@stop