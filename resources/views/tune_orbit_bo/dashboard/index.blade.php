@extends('tune_orbit_bo.layouts.dashboard')

@section('title','Dashboard')

@section('page_title_sub', 'Place where it start\'s')

@section('content')
   <div class='row'>
      <div class='col-md-12'>
       
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Dashboard</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
    
          
            
            
            <div class="col-md-12">
               <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>
                     {{$total_services}}
                  </h3>
                  <p>Total Services</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>
                    {{$pending_commit}}
                  </h3>
                  <p>Pending Commit</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
               
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>
                    {{$pending_audit}}
                  </h3>
                  <p>Pending Audit</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                 
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>
                     {{$service_in_website}}
                  </h3>
                  <p>Services In Website</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
               
              </div>
            </div><!-- ./col -->
          </div> 
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>
                     {{$unaudited_service_in_website}}
                  </h3>
                  <p>Total Unaudited Services In Website</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
               
              </div>
            </div><!-- ./col -->
             
             
             
          </div>    
         
    
           
            
          </div>
          <div class="box-footer">
            
        </div>
      </div>
      </div>
    
    
      </div>  
@endsection

@section('script')
@parent

<script type="text/javascript">

 

</script>
@stop