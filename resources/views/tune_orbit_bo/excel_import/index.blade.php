@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Excel Import')

@section('page_title_sub', 'Manage Excel Import')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">List of Services</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>

      <div class="box-body table-responsive no-padding">

        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::open(array('route' => array('tune_orbit_bo.masters.services.store'), 'method' => 'POST','files'=>true,'id'=>'add-form'))!!}
          <div class="col-md-6" id="attachment_div">
            {!! Form::label('Select Excel') !!}
            <input type="file" class="form-control required" id="file_name" name="file_name">
          </div>

          <div class="col-md-2" style="margin-top:25px;">
            {!! Form::button('Upload Excel', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn']) !!}
          </div>

          {!!Form::close()!!}
        </div>
        <div class="box-footer">
          {{-- <form action='#'>
          <input type='text' placeholder='New task' class='form-control input-sm' />
        </form> --}}
      </div><!-- /.box-footer-->
    </div><!-- /.box -->
  </div><!-- /.col -->


</div><!-- /.row -->
@endsection

@section('script')
@parent

<script type="text/javascript">

 $(function(){

  $('#upload_btn').click(function(){
    if($('#add-form').valid()){
      var formData = new FormData($('#add-form')[0]);

      var ajax = $.ajax({
        type: 'post',
        url:'{{URL::route("tune_orbit_bo.excel_import.import.uploadExcel")}}',

        data: formData,
        contentType: false,
        processData: false,
        async:false
      })
      .done(function(result) {
        console.log(result);
        $('#file_name').val();
        var ajax = $.ajax({
        type: 'post',
        url:'https://www.tuneorbit.com/EXCEL_IMPORT/index.php',

        data: {filename:result['path']},
        async: false,
      })
      .done(function(result) {
        console.log(result);
        $('#file_name').val('');
        $.notify("Excel imported successfully.",{
          type:'success',
        });
      })
      })
      .fail(function() {
        alert("fail");
      });
    }

  });
  
  @if(Session::has('message'))
  $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
  @endif
});

</script>
@stop