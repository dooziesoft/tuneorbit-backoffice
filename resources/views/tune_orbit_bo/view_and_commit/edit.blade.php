@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'Manage Service Page')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">View and Commit</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::model($service_page,array('route' => array('service_page.view_and_commit.updateServicePage', $service_page->id), 'method' => 'PUT','files'=>true,'id'=>'edit-form','onsubmit'=>'return validate()'))!!}
 


          {!!Form::hidden('id',$service_page->id)!!}
          <div class="col-md-4">
           <div class="form-group @if($errors->first('service_id')) has-error @endif">

             {!!Form::label('service_id','Service Name*')!!}
             {!! Form::servicesSelect('service_id',$service_page->service_id) !!}
             <small class="text-danger">{{ $errors->first('service_id') }}</small>

           </div>
         </div>

         <div class="col-md-4">
           <div class="form-group @if($errors->first('sub_service_id')) has-error @endif" id="sub_service_div">

             {!!Form::label('sub_service_id','Sub Service Name*')!!}
             {!!Form::select('sub_service_id', $sub_services,$service_page->sub_service_id, ['class' => 'form-control required','id'=>'sub_service_id', 'notequal' => '0', 'data-live-search' => 'true']) !!} 
             <small class="text-danger">{{ $errors->first('sub_service_id') }}</small>

           </div>
         </div>

         <div class="col-md-4">
          <div class="form-group @if($errors->first('business_name')) has-error @endif">
           {!!Form::label('business_name','Business Name')!!}
           {!!Form::text('business_name',Input::old('business_name'),['class' => 'form-control required','id'=>'business_name',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Business Name","data-placement"=>"bottom",])!!}
           <small class="text-danger">{{ $errors->first('business_name') }}</small>
         </div>
       </div>

       <div class="col-md-4">
        <div class="form-group @if($errors->first('locality')) has-error @endif">
         {!!Form::label('locality','Locality')!!}
         {!!Form::text('locality',Input::old('locality'),['class' => 'form-control required','id'=>'locality',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Locality","data-placement"=>"bottom",])!!}
         <small class="text-danger">{{ $errors->first('locality') }}</small>
       </div>
     </div>

       <div class="col-md-4">
        <div class="form-group @if($errors->first('location_lat')) has-error @endif">
         {!!Form::label('location_lat','Location Latitude')!!}
         {!!Form::text('location_lat',Input::old('location_lat'),['class' => 'form-control required number','id'=>'location_lat',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Location Latitude","data-placement"=>"bottom",])!!}
         <small class="text-danger">{{ $errors->first('location_lat') }}</small>
       </div>
     </div>

     <div class="col-md-4">
      <div class="form-group @if($errors->first('location_long')) has-error @endif">
       {!!Form::label('location_long','Location Longitude')!!}
       {!!Form::text('location_long',Input::old('location_long'),['class' => 'form-control required number','id'=>'location_long',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Location Longitude","data-placement"=>"bottom",])!!}
       <small class="text-danger">{{ $errors->first('location_long') }}</small>
     </div>
   </div>

   <div class="col-md-6">
   <div class="col-md-12">
    <div class="form-group @if($errors->first('address')) has-error @endif">
     {!!Form::label('address','Address')!!}
     {!!Form::textarea('address',Input::old('address'),['class' => 'form-control','id'=>'address',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Address","data-placement"=>"bottom","rows"=>3])!!}
     <small class="text-danger">{{ $errors->first('address') }}</small>
   </div>
 </div>

 <div class="col-md-4" >
 <div class="form-group @if($errors->first('country_code')) has-error @endif">

   {!!Form::label('country_code','Country Code')!!}
   {!!Form::select('country_code', array(),null, ['class' => 'form-control','id'=>'country_code', 'data-live-search' => 'true']) !!}
   <small class="text-danger">{{ $errors->first('country_code') }}</small>

 </div>
</div>

 <div class="col-md-8">
  <div class="form-group @if($errors->first('phone_number')) has-error @endif">
   {!!Form::label('phone_number','Phone Number')!!}
   {!!Form::text('phone_number',Input::old('phone_number'),['class' => 'form-control','id'=>'phone_number',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Phone Number","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('phone_number') }}</small>
 </div>
</div>

<div class="col-md-12">
  <div class="form-group @if($errors->first('website')) has-error @endif">
   {!!Form::label('website','Website')!!}
   {!!Form::text('website',Input::old('website'),['class' => 'form-control','id'=>'website',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Website","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('website') }}</small>
 </div>
</div>
 </div>
 
 <div class="col-md-6 table-responsive" >
{!!Form::label('website','Working Days')!!}
  <table class="table table-hover table-bordered">
    <tr>
      <th colspan="5">
        <div class="pull-right">
          <label class="checkbox-inline">
          <input type="checkbox" name="open_24x7" id="open_24x7" onclick="disableTableInputs()" ><b>Open 24x7</b></label>
          </div>
        </th>
      </tr>
      <tr>
        <th><input type="checkbox" id="check_all" name="check_all" ></th>
        <th>Day</th>
        <th>From Time</th>
        <th>To Time</th>
        <th>Open 24 hours</th>
      </tr>
      <tbody id="working_days_row">
        <tr>
          <td><input type="checkbox" id="sun" name="sun"></td>
          <td>Sun</td>
          <td style="padding:2px;">
          <input type="text"
          id="sun_from_time" name="sun_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sun_to_time" name="sun_to_time"  placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sun_open_24_hours" name="sun_open_24_hours" onclick="disableFromToTime('sun')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="mon" name="mon"></td>
          <td>Mon</td>
          <td style="padding:2px;">
          <input type="text" id="mon_from_time" name="mon_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="mon_to_time" name="mon_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="mon_open_24_hours" name="mon_open_24_hours" onclick="disableFromToTime('mon')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="tue" name="tue"></td>
          <td>Tue</td>
          <td style="padding:2px;">
          <input type="text" id="tue_from_time" name="tue_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="tue_to_time" name="tue_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="tue_open_24_hours" name="tue_open_24_hours" onclick="disableFromToTime('tue')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="wed" name="wed"></td>
          <td>Wed</td>
          <td style="padding:2px;">
          <input type="text" id="wed_from_time" name="wed_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="wed_to_time" name="wed_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="wed_open_24_hours" name="wed_open_24_hours" onclick="disableFromToTime('wed')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="thu" name="thu"></td>
          <td>Thu</td>
          <td style="padding:2px;">
          <input type="text" id="thu_from_time" name="thu_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="thu_to_time" name="thu_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="thu_open_24_hours" name="thu_open_24_hours" onclick="disableFromToTime('thu')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="fri" name="fri"></td>
          <td>Fri</td>
          <td style="padding:2px;">
          <input type="text" id="fri_from_time" name="fri_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="fri_to_time" name="fri_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="fri_open_24_hours" name="fri_open_24_hours" onclick="disableFromToTime('fri')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="sat" name="sat"></td>
          <td>Sat</td>
          <td style="padding:2px;">
          <input type="text" id="sat_from_time" name="sat_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sat_to_time" name="sat_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sat_open_24_hours" name="sat_open_24_hours" onclick="disableFromToTime('sat')"></td>
        </tr>
      </tbody>
    </table>
  </div>

<div class="col-md-8">
  <div class="form-group @if($errors->first('about')) has-error @endif">
   {!!Form::label('about','About')!!}
   {!!Form::textarea('about',Input::old('about'),['class' => 'form-control','id'=>'about',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter About","data-placement"=>"bottom","rows"=>8])!!}
   <small class="text-danger">{{ $errors->first('about') }}</small>
 </div>
</div>

<!-- BANNER START -->
<div class="col-md-6">
  {!! Form::label('Banner Photo') !!}
  <input type="file" class="form-control hidden" id="banner_file_name" name="banner_file_name">
</div>

<div class="col-md-2" style="margin-top:25px;" id="banner_btn_div" hidden>
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_banner_btn', 'type' => 'button']) !!}
</div>

<input type="hidden" class="form-control" id="banner_file_path" name="banner_file_path" value="{{$service_page->banner_image}}">

<div class="col-md-12" style="margin-top:50px;" id="banner_photo_div" >
  <div class="col-md-10">
    <img style="height:auto;width:600px;" src='{{$service_page->banner_image}}' id="banner_photo"><button type="button" id="delete_banner_photo")>X</button>
  </div>
</div>
<div class="clearfix"></div>
<hr>
<!-- BANNER END -->

<div class="clearfix margin"></div>

<!-- THUMBNAIL START -->

<div class="col-md-6" id="thumbnail_input_div">
  {!! Form::label('Thumbnail Photo') !!}
  <input type="file" class="form-control hidden" id="thumbnail_file_name" name="thumbnail_file_name">
</div>

<div class="col-md-2" style="margin-top:25px;" id="thumbnail_btn_div" hidden>
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_thumbnail_btn']) !!}
</div>

<input type="hidden" class="form-control" id="thumbnail_file_path" name="thumbnail_file_path" value="{{$service_page->thumbnail_image}}">

<div class="col-md-12" style="margin-top:50px;" id="thumbnail_photo_div" >
  <div class="col-md-10">
    <img style="height:auto;width:600px;" src='{{$service_page->thumbnail_image}}' id="thumbnail_photo"><button type="button" id="delete_thumbnail_photo")>X</button>
  </div>
</div>
<div class="clearfix"></div>
<hr>
<!-- THUMBNAIL END -->

<div class="clearfix margin"></div>

<!-- PHOTOS START -->

<div class="col-md-6" id="attachment_div">
  {!! Form::label('Photos') !!}
  <input type="file" class="form-control" id="file_name" name="file_name">
</div>

<div class="col-md-2" style="margin-top:25px;">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn', 'type' => 'button']) !!}
</div>

<div class="clearfix"></div>
<div class="col-md-12" id="photo_div" style="margin-top:50px;" >
  @foreach($service_page_photos as $spp)
  <div class="col-md-3" id="photo_frame{{$spp->id}}">
    <img style="height:auto;width:250px;" src='http://tuneorbit.com/server/img/user_service/{{$spp->image_path}}' id="photo_tag"><button type="button" onclick="deletePhoto('{{$spp->image_path}}','{{$spp->id}}')")>X</button>
  </div>
  @endforeach
</div>

<!-- PHOTOS END -->




<div class='clearfix'></div>
<div class="col-md-2 pull-right">
  <a href="{{URL::route('tune_orbit_bo.service_page.view_and_commit.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
</div>
<div class="col-md-2 pull-right">
  <div class="form-group">
    {!! Form::submit('Commit', ['class' => 'btn btn-block btn-success btn-block']) !!}
  </div>
</div>          
<div class="clearfix"></div>
{!!Form::close()!!}
</div>
<div class="box-footer">
  {{-- <form action='#'>
  <input type='text' placeholder='New task' class='form-control input-sm' />
</form> --}}
</div><!-- /.box-footer-->
</div><!-- /.box -->
</div><!-- /.col -->

</div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
    tinymce.init({
        selector: '#about',
        height: 500,
            theme: 'modern',
            plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
            ],
           toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
           toolbar2: 'print preview media | forecolor backcolor emoticons fontsizeselect jbimages',
           image_advtab: true,
           fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
           templates: [
           { title: 'Test template 1', content: 'Test 1' },
           { title: 'Test template 2', content: 'Test 2' }
           ],
           content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
           ],
            font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
           relative_urls: false
    });

   var service_page = <?php echo json_encode($service_page) ?>;

   var countries=[
'Afghanistan',
'Albania',
'Algeria',
'American Samoa',
'Andorra',
'Angola',
'Anguilla',
'Antarctica',
'Antigua and Barbuda',
'Argentina',
'Armenia',
'Aruba',
'Australia',
'Austria',
'Azerbaijan',
'Bahamas',
'Bahrain',
'Bangladesh',
'Barbados',
'Belarus',
'Belgium',
'Belize',
'Benin',
'Bermuda',
'Bhutan',
'Bolivia',
'Bosnia and Herzegovina',
'Botswana',
'Brazil',
'British Indian Ocean Territory',
'British Virgin Islands',
'Brunei',
'Bulgaria',
'Burkina Faso',
'Burundi',
'Cambodia',
'Cameroon',
'Canada',
'Cape Verde',
'Cayman Islands',
'Central African Republic',
'Chad',
'Chile',
'China',
'Christmas Island',
'Cocos Islands',
'Colombia',
'Comoros',
'Cook Islands',
'Costa Rica',
'Croatia',
'Cuba',
'Curacao',
'Cyprus',
'Czech Republic',
'Democratic Republic of the Congo',
'Denmark',
'Djibouti',
'Dominica',
'Dominican Republic',
'East Timor',
'Ecuador',
'Egypt',
'El Salvador',
'Equatorial Guinea',
'Eritrea',
'Estonia',
'Ethiopia',
'Falkland Islands',
'Faroe Islands',
'Fiji',
'Finland',
'France',
'French Polynesia',
'Gabon',
'Gambia',
'Georgia',
'Germany',
'Ghana',
'Gibraltar',
'Greece',
'Greenland',
'Grenada',
'Guam',
'Guatemala',
'Guernsey',
'Guinea',
'Guinea-Bissau',
'Guyana',
'Haiti',
'Honduras',
'Hong Kong',
'Hungary',
'Iceland',
'India',
'Indonesia',
'Iran',
'Iraq',
'Ireland',
'Isle of Man',
'Israel',
'Italy',
'Ivory Coast',
'Jamaica',
'Japan',
'Jersey',
'Jordan',
'Kazakhstan',
'Kenya',
'Kiribati',
'Kosovo',
'Kuwait',
'Kyrgyzstan',
'Laos',
'Latvia',
'Lebanon',
'Lesotho',
'Liberia',
'Libya',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macau',
'Macedonia',
'Madagascar',
'Malawi',
'Malaysia',
'Maldives',
'Mali',
'Malta',
'Marshall Islands',
'Mauritania',
'Mauritius',
'Mayotte',
'Mexico',
'Micronesia',
'Moldova',
'Monaco',
'Mongolia',
'Montenegro',
'Montserrat',
'Morocco',
'Mozambique',
'Myanmar',
'Namibia',
'Nauru',
'Nepal',
'Netherlands',
'Netherlands Antilles',
'New Caledonia',
'New Zealand',
'Nicaragua',
'Niger',
'Nigeria',
'Niue',
'North Korea',
'Northern Mariana Islands',
'Norway',
'Oman',
'Pakistan',
'Palau',
'Palestine',
'Panama',
'Papua New Guinea',
'Paraguay',
'Peru',
'Philippines',
'Pitcairn',
'Poland',
'Portugal',
'Puerto Rico',
'Qatar',
'Republic of the Congo',
'Reunion',
'Romania',
'Russia',
'Rwanda',
'Saint Barthelemy',
'Saint Helena',
'Saint Kitts and Nevis',
'Saint Lucia',
'Saint Martin',
'Saint Pierre and Miquelon',
'Saint Vincent and the Grenadines',
'Samoa',
'San Marino',
'Sao Tome and Principe',
'Saudi Arabia',
'Senegal',
'Serbia',
'Seychelles',
'Sierra Leone',
'Singapore',
'Sint Maarten',
'Slovakia',
'Slovenia',
'Solomon Islands',
'Somalia',
'South Africa',
'South Korea',
'South Sudan',
'Spain',
'Sri Lanka',
'Sudan',
'Suriname',
'Svalbard and Jan Mayen',
'Swaziland',
'Sweden',
'Switzerland',
'Syria',
'Taiwan',
'Tajikistan',
'Tanzania',
'Thailand',
'Togo',
'Tokelau',
'Tonga',
'Trinidad and Tobago',
'Tunisia',
'Turkey',
'Turkmenistan',
'Turks and Caicos Islands',
'Tuvalu',
'U.S. Virgin Islands',
'Uganda',
'Ukraine',
'United Arab Emirates',
'United Kingdom',
'United States',
'Uruguay',
'Uzbekistan',
'Vanuatu',
'Vatican',
'Venezuela',
'Vietnam',
'Wallis and Futuna',
'Western Sahara',
'Yemen',
'Zambia',
'Zimbabwe'
]

var country_codes=[
'93',
'355',
'213',
'1-684',
'376',
'244',
'1-264',
'672',
'1-268',
'54',
'374',
'297',
'61',
'43',
'994',
'1-242',
'973',
'880',
'1-246',
'375',
'32',
'501',
'229',
'1-441',
'975',
'591',
'387',
'267',
'55',
'246',
'1-284',
'673',
'359',
'226',
'257',
'855',
'237',
'1',
'238',
'1-345',
'236',
'235',
'56',
'86',
'61',
'61',
'57',
'269',
'682',
'506',
'385',
'53',
'599',
'357',
'420',
'243',
'45',
'253',
'1-767',
'1-809, 1-829, 1-849',
'670',
'593',
'20',
'503',
'240',
'291',
'372',
'251',
'500',
'298',
'679',
'358',
'33',
'689',
'241',
'220',
'995',
'49',
'233',
'350',
'30',
'299',
'1-473',
'1-671',
'502',
'44-1481',
'224',
'245',
'592',
'509',
'504',
'852',
'36',
'354',
'91',
'62',
'98',
'964',
'353',
'44-1624',
'972',
'39',
'225',
'1-876',
'81',
'44-1534',
'962',
'7',
'254',
'686',
'383',
'965',
'996',
'856',
'371',
'961',
'266',
'231',
'218',
'423',
'370',
'352',
'853',
'389',
'261',
'265',
'60',
'960',
'223',
'356',
'692',
'222',
'230',
'262',
'52',
'691',
'373',
'377',
'976',
'382',
'1-664',
'212',
'258',
'95',
'264',
'674',
'977',
'31',
'599',
'687',
'64',
'505',
'227',
'234',
'683',
'850',
'1-670',
'47',
'968',
'92',
'680',
'970',
'507',
'675',
'595',
'51',
'63',
'64',
'48',
'351',
'1-787, 1-939',
'974',
'242',
'262',
'40',
'7',
'250',
'590',
'290',
'1-869',
'1-758',
'590',
'508',
'1-784',
'685',
'378',
'239',
'966',
'221',
'381',
'248',
'232',
'65',
'1-721',
'421',
'386',
'677',
'252',
'27',
'82',
'211',
'34',
'94',
'249',
'597',
'47',
'268',
'46',
'41',
'963',
'886',
'992',
'255',
'66',
'228',
'690',
'676',
'1-868',
'216',
'90',
'993',
'1-649',
'688',
'1-340',
'256',
'380',
'971',
'44',
'1',
'598',
'998',
'678',
'379',
'58',
'84',
'681',
'212',
'967',
'260',
'263'
];
var str=service_page['phone_number'];
$('#phone_number').val(str.substr(str.indexOf(' ')+1));

var not_found=1;
for(var cc=0;cc<countries.length;cc++){
  if(str.substr(0,str.indexOf(' ')) == country_codes[cc]){
    not_found=0;
    var country_code='<option value='+country_codes[cc]+'>'+countries[cc]+' +'+country_codes[cc]+'</option>';
  }
}

if(not_found){
  var country_code='<option value="0">Select Country Code</option>';
}else{
  country_code+='<option value="0">Select Country Code</option>';
}

for(var cc=0;cc<countries.length;cc++){
  country_code+='<option value='+country_codes[cc]+'>'+countries[cc]+' +'+country_codes[cc]+'</option>';
}
$('#country_code').html(country_code).selectpicker();

console.log(service_page);
console.log("service_page");

    if(service_page['open_24x7'] == 1){
        $('#open_24x7').attr('checked',true);
        $('#working_days_row').find('input').attr('disabled',true);
        $("#check_all").attr('disabled',true);
      }else{
        $('#open_24x7').attr('checked',false);
      var working_day_str=service_page['working_days'].split(',');
      var from_time_str=service_page['from_time'].split(',');
      var to_time_str=service_page['to_time'].split(',');
      if(working_day_str[6].split(':').pop() == 'true}"'){
        $('#sun').attr('checked',true);
        if(from_time_str[6].split(':').pop() == 'open_24_hours}'){
          $('#sun_open_24_hours').attr('checked',true);
          $('#sun_from_time').val('---');
          $('#sun_to_time').val('---');
          $('#sun_from_time').attr('disabled',true);
          $('#sun_to_time').attr('disabled',true);
        }else{
          $('#sun_open_24_hours').attr('checked',false);
          $('#sun_from_time').val(from_time_str[6].substring(6).slice(0,-1));
          $('#sun_to_time').val(to_time_str[6].substring(6).slice(0,-1));
        }
      }else{
        $('#sun').attr('checked',false);
      }
      if(working_day_str[0].split(':').pop() == 'true'){
        $('#mon').attr('checked',true);
        if(from_time_str[0].split(':').pop() == 'open_24_hours'){
          $('#mon_open_24_hours').attr('checked',true);
          $('#mon_from_time').val('---');
          $('#mon_to_time').val('---');
          $('#mon_from_time').attr('disabled',true);
          $('#mon_to_time').attr('disabled',true);
        }else{
          $('#mon_open_24_hours').attr('checked',false);
          $('#mon_from_time').val(from_time_str[0].substring(7));
          $('#mon_to_time').val(to_time_str[0].substring(7));
        }
      }else{
        $('#mon').attr('checked',false);
      }
      if(working_day_str[1].split(':').pop() == 'true'){
        $('#tue').attr('checked',true);
        if(from_time_str[1].split(':').pop() == 'open_24_hours'){
          $('#tue_open_24_hours').attr('checked',true);
          $('#tue_from_time').val('---');
          $('#tue_to_time').val('---');
          $('#tue_from_time').attr('disabled',true);
          $('#tue_to_time').attr('disabled',true);
        }else{
          $('#tue_open_24_hours').attr('checked',false);
          $('#tue_from_time').val(from_time_str[1].substring(6));
          $('#tue_to_time').val(to_time_str[1].substring(6));
        }
      }else{
        $('#tue').attr('checked',false);
      }
      if(working_day_str[2].split(':').pop() == 'true'){
        $('#wed').attr('checked',true);
        if(from_time_str[2].split(':').pop() == 'open_24_hours'){
          $('#wed_open_24_hours').attr('checked',true);
          $('#wed_from_time').val('---');
          $('#wed_to_time').val('---');
          $('#wed_from_time').attr('disabled',true);
          $('#wed_to_time').attr('disabled',true);
        }else{
          $('#wed_open_24_hours').attr('checked',false);
          $('#wed_from_time').val(from_time_str[2].substring(6));
          $('#wed_to_time').val(to_time_str[2].substring(6));
        }
      }else{
        $('#wed').attr('checked',false);
      }
      if(working_day_str[3].split(':').pop() == 'true'){
        $('#thu').attr('checked',true);
        if(from_time_str[3].split(':').pop() == 'open_24_hours'){
          $('#thu_open_24_hours').attr('checked',true);
          $('#thu_from_time').val('---');
          $('#thu_to_time').val('---');
          $('#thu_from_time').attr('disabled',true);
          $('#thu_to_time').attr('disabled',true);
        }else{
          $('#thu_open_24_hours').attr('checked',false);
          $('#thu_from_time').val(from_time_str[3].substring(6));
          $('#thu_to_time').val(to_time_str[3].substring(6));
        }
      }else{
        $('#thu').attr('checked',false);
      }
      if(working_day_str[4].split(':').pop() == 'true'){
        $('#fri').attr('checked',true);
        if(from_time_str[4].split(':').pop() == 'open_24_hours'){
          $('#fri_open_24_hours').attr('checked',true);
          $('#fri_from_time').val('---');
          $('#fri_to_time').val('---');
          $('#fri_from_time').attr('disabled',true);
          $('#fri_to_time').attr('disabled',true);
        }else{
          $('#fri_open_24_hours').attr('checked',false);
          $('#fri_from_time').val(from_time_str[4].substring(6));
          $('#fri_to_time').val(to_time_str[4].substring(6));
        }
      }else{
        $('#fri').attr('checked',false);
      }
      if(working_day_str[5].split(':').pop() == 'true'){
        $('#sat').attr('checked',true);
        if(from_time_str[5].split(':').pop() == 'open_24_hours'){
          $('#sat_open_24_hours').attr('checked',true);
          $('#sat_from_time').val('---');
          $('#sat_to_time').val('---');
          $('#sat_from_time').attr('disabled',true);
          $('#sat_to_time').attr('disabled',true);
        }else{
          $('#sat_open_24_hours').attr('checked',false);
          $('#sat_from_time').val(from_time_str[5].substring(6));
          $('#sat_to_time').val(to_time_str[5].substring(6));
        }
      }else{
        $('#sat').attr('checked',false);
      }
    }
   
   if(Number(service_page['banner_image']) == 0){
      $('#banner_file_name').removeClass('hidden');
      $('#banner_btn_div').show();
      $('#banner_photo_div').hide();
   }else{
      $('#banner_file_name').addClass('hidden');
      $('#banner_btn_div').hide();
      $('#banner_photo_div').show();
   }
   if(Number(service_page['thumbnail_image']) == 0){
      $('#thumbnail_file_name').removeClass('hidden');
      $('#thumbnail_btn_div').show();
      $('#thumbnail_photo_div').hide();
   }else{
      $('#thumbnail_file_name').addClass('hidden');
      $('#thumbnail_btn_div').hide();
      $('#thumbnail_photo_div').show();
   }

   $("#check_all").click(function () {
     $('#sun').not(this).prop('checked', this.checked);
     $('#mon').not(this).prop('checked', this.checked);
     $('#tue').not(this).prop('checked', this.checked);
     $('#wed').not(this).prop('checked', this.checked);
     $('#thu').not(this).prop('checked', this.checked);
     $('#fri').not(this).prop('checked', this.checked);
     $('#sat').not(this).prop('checked', this.checked);
 });

   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif

   $('#sub_service_id').selectpicker();
   

   $('#service_id').change(function(){
    $('#sub_service_id').val('');
    $('#sub_service_div').html('<label>Sub Service Name*</label><select type="text" class="form-control"  id="sub_service_id" name="sub_service_id" data-live-search="true"></select>');
    var service_id=$('#service_id').val();
    $.ajax({
      type: 'get',
      url:'{{URL::route("service_page.services.getspecificSubservices")}}',
      dataType: 'json',
      data:{service_id:service_id},
    }).done(function(result){
     console.log(result);
     var sub_services=result['sub_services'];
     var dis='<option value=0>Select Sub Services</option>';
     for(var i=0;i<sub_services.length;i++){
      dis+='<option value='+sub_services[i]['id']+'>'+sub_services[i]['name']+'</option>';
     }
     $('#sub_service_id').html(dis).selectpicker();
    });

   })

   $('#upload_banner_btn').click(function(){

    var formData = new FormData($('#edit-form')[0]);
    var ajax = $.ajax({
     type: 'POST',
     url:'{{URL::route("tune_orbit_bo.masters.sub_services.editUploadserviceImg")}}',
     data: formData,
     contentType: false,
     processData: false
   }).done(function(result) {
    console.log(result);
    if(result){
      $('#banner_file_name').addClass('hidden');
      $('#banner_btn_div').hide();
      $('#banner_photo_div').show();
      $('#banner_photo').attr('src',result['path']);
      $('#banner_file_name').val('');
      $('#banner_file_path').val(result['path']);
    }else{
      $.notify(" Please Choose file.",{
        type:'danger',
      });
      return false;
    }
  }).fail(function() {
    alert("fail");
  });


});

   $('#delete_banner_photo').click(function(){
    var res=confirm('Do you really want to delete banner photo...?');
    if(res){
      $.ajax({
        type: 'get',
        url:'{{URL::route("service_page.services.deleteSpecificImg")}}',
        dataType: 'json',
        data:{path:$('#banner_photo').attr('src'),service_id:service_page['id'],type:1},
      }).done(function(result){
        $('#banner_file_name').removeClass('hidden');
        $('#banner_btn_div').show();
        $('#banner_photo_div').hide();
        $('#banner_photo').attr('src','');
        $('#banner_file_path').val('');
      });
    }
  })

   $('#upload_thumbnail_btn').click(function(){

    var formData = new FormData($('#edit-form')[0]);
    var ajax = $.ajax({
     type: 'POST',
     url:'{{URL::route("tune_orbit_bo.masters.sub_services.editUploadserviceImg")}}',
     data: formData,
     contentType: false,
     processData: false
   }).done(function(result) {
    console.log(result);
    if(result){
      $('#thumbnail_file_name').addClass('hidden');
      $('#thumbnail_btn_div').hide();
      $('#thumbnail_photo_div').show();
      $('#thumbnail_photo').attr('src',result['path']);
      $('#thumbnail_file_name').val('');
      $('#thumbnail_file_path').val(result['path']);
    }else{
      $.notify(" Please Choose file.",{
        type:'danger',
      });
      return false;
    }
  }).fail(function() {
    alert("fail");
  });


});

   $('#delete_thumbnail_photo').click(function(){
    var res=confirm('Do you really want to delete thumbnail photo...?');
    if(res){
      $.ajax({
        type: 'get',
        url:'{{URL::route("service_page.services.deleteSpecificImg")}}',
        dataType: 'json',
        data:{path:$('#thumbnail_photo').attr('src'),service_id:service_page['id'],type:2},
      }).done(function(result){
        $('#thumbnail_file_name').removeClass('hidden');
        $('#thumbnail_btn_div').show();
        $('#thumbnail_photo_div').hide();
        $('#thumbnail_photo').attr('src','');
        $('#thumbnail_file_path').val('');
      });
    }
  })

 $('#upload_btn').click(function(){

      var formData = new FormData($('#edit-form')[0]);
      var ajax = $.ajax({
       type: 'POST',
       url:'{{URL::route("tune_orbit_bo.masters.sub_services.editUploadserviceImg")}}',
       data: formData,
       contentType: false,
       processData: false
     }).done(function(result) {
        console.log(result);
        if(result){
        var path=result[0]['path'];
        var sp = path.split('/');
        var file = sp[sp.length-1];
        $('#photo_div').append('<div class="col-md-3" id="photo_frame'+result[1]['user_service_img_id']+'"><img style="height:auto;width:150px;" src='+result[0]['path']+' id="photo_'+result[1]['user_service_img_id']+'"><button type="button" id="btn_'+result[1]['user_service_img_id']+'" onclick=deletePhoto("'+encodeURIComponent(file)+'",'+result[1]['user_service_img_id']+')>X</button></div>');
        $('#file_name').val('');
      }else{
        $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
      }
    }).fail(function() {
        alert("fail");
      });


    });

 });

function disableTableInputs(){
  if($('#open_24x7').is(':checked')){
    $('#check_all').attr('disabled',true);
    $('#check_all').attr('checked',false);
     $('#working_days_row').find('input').attr('disabled',true);
     $('#working_days_row').find('input').val('');
     $('#working_days_row').find('input[type=checkbox]:checked').attr('checked',false);
   }else{
    $('#check_all').attr('disabled',false);
   $('#working_days_row').find('input').attr('disabled',false);
 }         
}

function disableFromToTime(day){
    var str=day+'_open_24_hours';
    var from_str=day+'_from_time';
    var to_str=day+'_to_time';
    if($('#'+str).is(':checked')){
      $('#'+from_str).attr('disabled',true);
      $('#'+from_str).val('');
      $('#'+to_str).attr('disabled',true);
      $('#'+to_str).val('');
    }else{
      $('#'+from_str).attr('disabled',false);
      $('#'+from_str).val('');
      $('#'+to_str).attr('disabled',false);
      $('#'+to_str).val('');
    }
  }

  function deletePhoto(path,user_service_img_id){
    var res=confirm('Do you really want to delete this photo...?');
    if(res){
      $.ajax({
        type: 'get',
        url:'{{URL::route("service_page.services.deleteSpecificServicePagePhoto")}}',
        dataType: 'json',
        data:{path:path,user_service_img_id:user_service_img_id},
      }).done(function(result){
        $('#photo_frame'+user_service_img_id).hide();
      });
    }
  }


  function validate()
  {
    var flag=1;
    if($('#edit-form').valid()){
      if($('#service_id').val() == 0){
        $.notify("Select service name.",{
          type:'danger',
        });
        return false;
      }else if($('#sub_service_id').val() == 0){
        $.notify("Select sub service name.",{
          type:'danger',
        });
        return false;
      }else{
        if(!$('#open_24x7').is(':checked') && !$('#sun').is(':checked') && !$('#mon').is(':checked') && !$('#tue').is(':checked') && !$('#wed').is(':checked') && !$('#thu').is(':checked') && !$('#fri').is(':checked') && !$('#sat').is(':checked')){
          flag=0;
        }
        var arr=['sun','mon','tue','wed','thu','fri','sat'];
        for(var i=0;i<arr.length;i++){
          var str=arr[i]+'_open_24_hours';
          var from_str=arr[i]+'_from_time';
          var to_str=arr[i]+'_to_time';
          if($('#'+arr[i]).is(':checked')){
            if((Number($('#'+from_str).val()) == 0 || Number($('#'+to_str).val()) == 0) && !$('#'+str).is(':checked')){
              flag=2;
            }
          }
        }
        if(flag == 1){
          return true;
        }else if(flag == 0){
          $.notify("Please enter working days details!",{
            type:'danger',
          });
          return false;
        }else if(flag == 2){
          $.notify("Please enter proper timings for selected days!",{
            type:'danger',
          });
          return false;

        }
      }
    }else{
      return false;
    }
  }
</script>
@stop
