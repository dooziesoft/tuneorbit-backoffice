@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Manage Gallery Master')

@section('page_title_sub', 'Manage Gallery Master')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Manage Gallery Here</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">

         {!!Form::open(array('route' => array('sub_services.updateServiceImages'), 'method' => 'POST','files'=>true,'id'=>'manage-form','onsubmit'=>'return validate()'))!!}



         <div class="col-md-4" hidden>
          <div class="form-group @if($errors->first('sub_service_id')) has-error @endif">
           {!!Form::label('sub_service_id','sub_service_id*')!!}
           {!!Form::text('sub_service_id',$sub_service_id,['class' => 'form-control','id'=>'sub_service_id',"data-placement"=>"bottom"])!!}
           <small class="text-danger">{{ $errors->first('sub_service_id') }}</small>

         </div>
       </div>


       @if(isset($service_images))
       @foreach($service_images as $si)
       <div class="col-md-3" style="margin-top:25px;" id="photo_div{{$si->id}}">
       <?php
       $str=explode('/', $si->path);
       ?>

       <?php
          $type=1;
       ?>

        <img style="height:auto;width:150px;"  src="{{$si->path}}" id="photo_{{$si->id}}"><button id="btn_{{$si->id}}" type="button" onclick="deleteServiceImage({{$si->id}},'{{$str[4]}}',1)">X</button>
      </div>
      @endforeach
      @endif

      <div class="clearfix"></div>
      <hr/>
      <div class="clearfix"></div>

      <!-- Attach Service Image field -->
      <div class="col-md-4" id="attachment_div">
        {!! Form::label('Service Images') !!}
        <input type="file" class="form-control required" id="file_name" name="file_name">
      </div>

      <div class="col-md-2" style="margin-top:25px;">
        {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn']) !!}
      </div>


      <div class="col-md-12" id="photo_div" style="margin-top:50px;" >

      </div>


      <div class="col-md-4" hidden>
        <div class="form-group @if($errors->first('image_array')) has-error @endif">
         {!!Form::label('image_array','image_array*')!!}
         {!!Form::text('image_array',null,['class' => 'form-control','id'=>'image_array',"data-placement"=>"bottom"])!!}
         <small class="text-danger">{{ $errors->first('image_array') }}</small>

       </div>
     </div>

     <div class="col-md-4" hidden>
        <div class="form-group @if($errors->first('img_file')) has-error @endif">
         {!!Form::label('img_file','img_file*')!!}
         {!!Form::text('img_file',null,['class' => 'form-control','id'=>'img_file',"data-placement"=>"bottom"])!!}
         <small class="text-danger">{{ $errors->first('img_file') }}</small>

       </div>
     </div>


      <!-- <input type="hidden" id="temp_file_path" name="temp_file_path">
      <input type="hidden" id="public_path" name="public_path" value="{{public_path()}}"> -->




      <div class='clearfix' style="margin-top:25px;"></div>
      <div class="col-md-2 pull-right">
        <a href="{{URL::route('tune_orbit_bo.masters.sub_services.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
      </div>
      <div class="col-md-2 pull-right">
        <div class="form-group">
          {!! Form::submit('Save', ['class' => 'btn btn-block btn-success btn-block']) !!}
        </div>
      </div>

      {!!Form::close()!!}
    </div>
    <div class="box-footer">

    </div><!-- /.box-footer-->
  </div><!-- /.box -->
</div><!-- /.col -->

</div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
var image_array={},image_i=0;
  $(function(){
   $('[data-toggle="popover"]').popover(); 

   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif

    

    var image_file={},img_i=0;

    //UPLOAD IMAGE
    $('#upload_btn').click(function(){

      var formData = new FormData($('#manage-form')[0]);

      var ajax = $.ajax({
       type: 'post',
       url:'{{URL::route("tune_orbit_bo.masters.sub_services.uploadserviceImg")}}',

       data: formData,
       contentType: false,
       processData: false
     })
      .done(function(result) {
        console.log(result);
        if(result){
        var path=result['path'];
        var img_files=$('#file_name').val();
        console.log(img_files);

        img_file[image_i]={};
        img_file[image_i]=img_files;
       // img_i++;
        $('#img_file').val(JSON.stringify(img_file));


        console.log("path");
        console.log(path);

        //$('#temp_file_path').val(path);
        var type=2;


         var sp = path.split('/');
        var file = sp[sp.length-1];

        console.log(file);

        $('#photo_div').append('<div class="col-md-3" id="photo_frame'+image_i+'"><img style="height:auto;width:150px;" src='+result['path']+' id="photo_'+image_i+'"><button type="button" id="btn_'+image_i+'" onclick=deleteServiceImage('+image_i+',"'+encodeURIComponent(file)+'",'+type+')>X</button></div>');
        
        //var k=getFileName(path);

       


        image_array[image_i]={};
        image_array[image_i]=file;
        image_i++;
        $('#image_array').val(JSON.stringify(image_array));

        console.log(image_array);
        console.log(image_i);

        $('#file_name').val('');

      }else{
        $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
      }
    })
      .fail(function() {
        alert("fail");
      });


    });

  });


  function validate()
  {
    // if($('#manage-form').valid()){
     // return true;

      if(Number($('#image_array').val()) == 0){
          $.notify("Please upload atleast one image",{
            type:'danger',
          });
          return false;
        }

    // }else{
    //   return false;
    // }
  }


  function deleteServiceImage(id,path,type){
    $.ajax({
      type: 'get',
      url:'{{URL::route("masters.sub_services.deleteSpecificServiceImg")}}',
      dataType: 'json',
      data:{id:id,path:path,type:type},
    }).done(function(result){
      if(type == 2){
        delete image_array[id];
        $('#image_array').val(JSON.stringify(image_array));
      }
      if(result==1){
        $('#photo_frame'+id).hide();
      }else{
        $('#photo_div'+id).hide();
      }

    });

  }


  function getFileName(s)
  {
    var sp = s.split(' \ ');
    var file = sp[sp.length-1];
    alert(file);


    image_array[image_i]={};
    image_array[image_i]=file;
    image_i++;
    $('#image_array').val(JSON.stringify(image_array));

  }

</script>
@stop
