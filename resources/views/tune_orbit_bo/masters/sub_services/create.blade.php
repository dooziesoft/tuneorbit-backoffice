@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Sub Services Master')

@section('page_title_sub', 'Add Sub Services Master')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add Sub Services Here</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::open(array('route' => array('tune_orbit_bo.masters.sub_services.store'), 'method' => 'POST','files'=>true,'id'=>'add-form','onsubmit'=>'return validate()'))!!}
          
          @include('tune_orbit_bo.masters.sub_services._form',['submitButtonText'=>'Save'])


          <div class='clearfix'></div>


          {!!Form::close()!!}
        </div>
        <div class="box-footer">

        </div><!-- /.box-footer-->
      </div><!-- /.box -->
    </div><!-- /.col -->

  </div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
   $('[data-toggle="popover"]').popover(); 

   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif


  

 });


  function validate()
  {
    if($('#add-form').valid()){
      return true;
    }else{
      return false;
    }
  }

</script>
@stop
