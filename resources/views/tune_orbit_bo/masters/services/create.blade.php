@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Services Master')

@section('page_title_sub', 'Add Services Master')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add Services Here</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::open(array('route' => array('tune_orbit_bo.masters.services.store'), 'method' => 'POST','files'=>true,'id'=>'add-form','onsubmit'=>'return validate()'))!!}
          
          @include('tune_orbit_bo.masters.services._form',['submitButtonText'=>'Save'])

          <input type="hidden" id="temp_file_path" name="temp_file_path">
          <input type="hidden" id="public_path" name="public_path" value="{{public_path()}}">


          <div class='clearfix'></div>


          {!!Form::close()!!}
        </div>
        <div class="box-footer">

        </div><!-- /.box-footer-->
      </div><!-- /.box -->
    </div><!-- /.col -->

  </div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
   $('[data-toggle="popover"]').popover(); 

   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif


     //UPLOAD IMAGE
   $('#upload_btn').click(function(){

      var formData = new FormData($('#add-form')[0]);

      var ajax = $.ajax({
        type: 'post',
        url:'{{URL::route("tune_orbit_bo.masters.services.uploadImg")}}',

        data: formData,
        contentType: false,
        processData: false
      })
      .done(function(result) {
        console.log(result);
        if(result){

        console.log($('#public_path').val());
        var public_path=$('#public_path').val();
        
        var path=public_path+result['path'];
        console.log("path");
        console.log(path);
        $('#temp_file_path').val(result['path'].split('/').pop());
        $('#disp_round_attachment').attr('src',result['path']);
           // $('#disp_round_attachment').attr('src',path);

           $('#details_div').show();
           //$('#type_div').show();
         }else{
          $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
         }
         })
      .fail(function() {
        alert("fail");
      });


    });

 });


  function validate()
  {
    if($('#add-form').valid()){
      if(Number($('#temp_file_path').val()) == 0){
        $.notify("Please upload selected file.",{
          type:'danger',
        });
        return false;
      }else{
       return true;
      }
    }else{
      return false;
    }
  }

</script>
@stop
