@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Services Master')

@section('page_title_sub', 'Manage Services Master')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Services Here</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::model($services,array('route' => array('tune_orbit_bo.masters.services.update', $services->id), 'method' => 'PUT','files'=>true,'id'=>'edit-form','onsubmit'=>'return validate()'))!!}

          {!!Form::hidden('id',$services->id)!!}

           <input type="hidden" id="temp_file_path" name="temp_file_path">
          <input type="hidden" id="public_path" name="public_path" value="{{public_path()}}">


 <div class="col-md-4">
  <div class="form-group @if($errors->first('name')) has-error @endif">
   {!!Form::label('name','Name')!!}
   {!!Form::text('name',Input::old('name'),['class' => 'form-control required','id'=>'name',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Name","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('name') }}</small>
 </div>
</div>


<div class="col-md-4">
  <div class="form-group @if($errors->first('description')) has-error @endif">
   {!!Form::label('description','Description')!!}
   {!!Form::textarea('description',Input::old('description'),['class' => 'form-control','id'=>'description',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter description","data-placement"=>"bottom","rows"=>3])!!}
   <small class="text-danger">{{ $errors->first('description') }}</small>
 </div>
</div>

 

 <!-- Attach Image field -->
<div class="col-md-6" id="attachment_div">
  {!! Form::label('Thumbnail Attachment') !!}
    <input type="file" class="form-control" id="file_name" name="file_name">
</div>

<div class="col-md-2" style="margin-top:25px;">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn']) !!}
</div>

<div class="clearfix" ></div>
                           
<div class="col-md-12" >
 <div class="col-md-8" id="details_div" style="margin-top:50px;">
  <?php $attachment_path=getServicesUploadedPath($services->thumbnail_path) ?>
  <img src="{{$attachment_path}}" width="100px" id="disp_image" ><button type="button" id="delete_image" onclick="deleteServiceImage({{$services->id}})">X</button>
</div>
<div class="clearfix margin"></div>
<p style="color:red" id="p_div">* No attachment found!</p>

</div>

<div class="clearfix" style="margin-top:25px;"></div>




<div class='clearfix'></div>
<div class="col-md-2 pull-right">
  <a href="{{URL::route('tune_orbit_bo.masters.services.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
</div>
<div class="col-md-2 pull-right">
  <div class="form-group">
    {!! Form::submit('Update', ['class' => 'btn btn-block btn-success btn-block']) !!}
  </div>
</div>          
           <div class="clearfix"></div>
          {!!Form::close()!!}
        </div>
        <div class="box-footer">
{{-- <form action='#'>
<input type='text' placeholder='New task' class='form-control input-sm' />
</form> --}}
</div><!-- /.box-footer-->
</div><!-- /.box -->
</div><!-- /.col -->

</div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
     

     @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif
var str = <?php echo json_encode($services) ?>;
if(str['thumbnail_path']){
  $('#details_div').show();
  $('#p_div').hide();
}else{
  $('#details_div').hide();
  $('#p_div').show();
}
   $('#upload_btn').click(function(){

    var formData = new FormData($('#edit-form')[0]);
    var ajax = $.ajax({
      type: 'post',
      url:'{{URL::route("tune_orbit_bo.masters.services.uploadImgForEdit")}}',
      data: formData,
      contentType: false,
      processData: false
    })
    .done(function(result) {
      console.log(result);

      console.log($('#public_path').val());
      var public_path=$('#public_path').val();
      var path=public_path+result['path'];
      console.log("path");
      console.log(path);
      $('#temp_file_path').val(result['path'].split('/').pop());
      $('#disp_image').attr('src', result['path']);
      $('#details_div').show();
      $('#p_div').hide();
    });
  });

  });

  function deleteServiceImage(service_id){
    var res=confirm('Do you really want to delete this image permanently...?');
    if(res){
    $.ajax({
      type: 'get',
      url:'{{URL::route("tune_orbit_bo.masters.services.deleteServiceImage")}}',
      dataType: 'json',
      data:{service_id:service_id,path:$('#disp_image').attr('src')},
    }).done(function(result){
      location.reload();
    });
  }
  }


  function validate()
  {
    
    if($('#edit-form').valid()){
      return true;
    }else{
      return false;
    }

  }
</script>
@stop
