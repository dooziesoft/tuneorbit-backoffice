@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Services Master')

@section('page_title_sub', 'Manage Services Master')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">List of Services</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>

      <div class="box-body table-responsive no-padding">
        
        <div class="clearfix"></div>
        <div class="col-md-12">
          <div class="pull-right">

             <a class="btn bg-green margin" href="{{route('tune_orbit_bo.masters.services.create')}}" data-toggle="tooltip" data-placement="bottom" title="Click here to add New Services">
             <i class="glyphicon glyphicon-pencil"></i> Add Services
           </a> 

           <a type="button" class="btn bg-navy margin" id="edit_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Edit">
            <i class="glyphicon glyphicon-edit"></i> Edit Services
          </a>
           
          <a type="button" class="btn bg-red margin" onclick="no_select()" id="deactivate_btn" data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Activate/De-Activate">
            <i class="glyphicon glyphicon-trash"></i> Activate/Deactivate
          </a>

         
        </div><div class="clearfix"></div>
        <table class="table table-bordered" id="view">
          <thead>
            <tr class="bg-blue">
              <th></th>
              <th>Services Name</th>
              <th>Description</th>
              <th>Thumbnail</th>       
              <th>Created by</th>
              <th>Created on</th>
              <th>Updated by</th>
              <th>Updated on</th>
              <th>Status</th>
             
            </tr>  

            <tr tr class="bg-blue">
            <th></th>
            <th><input type="text" class="form-control filter" data-col="Services Name"></th>
            <th><input type="text" class="form-control filter" data-col="Description"></th>
            <th></th>
           <th><input type="text" class="form-control filter" data-col="Created by"></th>
            <th><input type="text" class="form-control filter" data-col="Created on"></th>
            <th><input type="text" class="form-control filter" data-col="Updated by"></th>
            <th><input type="text" class="form-control filter" data-col="Updated on"></th>
            <th></th>
             </tr>
        </thead>
          <tbody>
            @foreach($services as $s)
            <tr>
              <td><input type="radio" id='{{$s->id}}' name='ch'></td>
              
              <td>{{$s->name}}</td>
              <td>{{$s->description}}</td>
              <?php $attachment_path=getServicesUploadedPath($s->thumbnail_path) ?>
              <td><img src="{{$attachment_path}}" width="100px" height="auto" id="disp_image" ></td>
              <td>{{$s->created_by}}</td>
              <td>{{getFormatedDate($s->created_at)}}</td>
              <td>{{$s->updated_by}}</td>
              <td>{{getFormatedDate($s->updated_at)}}</td>
              @if($s->deleted_at==null)
              <td><i class="fa fa-check text-green"></i></td>
              @else
              <td><i class="fa fa-times-circle-o text-red"></i></td>
              @endif

            </tr>
            @endforeach
          </tbody>
          <div class="clearfix"></div>

        </table>
     

        <div class="clearfix"></div>
        
        
      </div>
      <div class="box-footer">
        {{-- <form action='#'>
        <input type='text' placeholder='New task' class='form-control input-sm' />
      </form> --}}
    </div><!-- /.box-footer-->
  </div><!-- /.box -->
</div><!-- /.col -->


</div><!-- /.row -->
@endsection

@section('script')
@parent

<script type="text/javascript">

 $(function(){
  $('.filter').multifilter({'target':$('#view')});
  $('input[name=ch]:radio').attr('checked',false);
  $('input[name=ch]:radio').change(function(){
    var id=$(this).attr('id');
    var status=$(this).attr('status');
    $("#edit_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/masters/services/"+id+"/edit");
    $("#edit_btn").attr('onclick',"");
    $("#deactivate_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/masters/services/deactivate/"+id);
    if(status){
      $("#deactivate_btn").attr('onclick',"return confirm_delete('Activate');");
    }else{
      $("#deactivate_btn").attr('onclick',"return confirm_delete('Deactivate');");
    }
    
  });
  @if(Session::has('message'))
  $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
  @endif
});
 function no_select(){
    $.notify("Please select row from below table",{
      type:'danger',
    });
  }
  function confirm_delete(status){
    if (!confirm('Do you really want to '+status+' services?')) {
    return false;
  }
}

</script>
@stop