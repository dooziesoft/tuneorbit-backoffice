
<div class="col-md-4">
  <div class="form-group @if($errors->first('name')) has-error @endif">
   {!!Form::label('name','Name')!!}
   {!!Form::text('name',Input::old('name'),['class' => 'form-control required','id'=>'name',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Name","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('name') }}</small>
 </div>
</div>


<div class="col-md-4">
  <div class="form-group @if($errors->first('description')) has-error @endif">
   {!!Form::label('description','Description')!!}
   {!!Form::textarea('description',Input::old('description'),['class' => 'form-control','id'=>'description',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter description","data-placement"=>"bottom","rows"=>3])!!}
   <small class="text-danger">{{ $errors->first('description') }}</small>
 </div>
</div>

<!-- Attach Image field -->
<div class="col-md-6" id="attachment_div">
  {!! Form::label('Thumbnail Path') !!}
  <input type="file" class="form-control required" id="file_name" name="file_name">
</div>

<div class="col-md-2" style="margin-top:25px;">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn']) !!}
</div>

<div class="clearfix" ></div>

<div class="col-md-12" id="details_div" hidden style="margin-top:25px;">

 <div class="col-md-8">
  <iframe src="" id="disp_round_attachment" height="650px" width="600px"></iframe>
</div>
</div>


<div class='clearfix' style="margin-top:25px;"></div>
<div class="col-md-2 pull-right">
<a href="{{URL::route('tune_orbit_bo.masters.services.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
</div>
<div class="col-md-2 pull-right">
  <div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-block btn-success btn-block']) !!}
  </div>
</div>


@section('script')
@parent
<script type="text/javascript">
  $(function(){

  });
</script>
@stop