<div class="col-md-4">
 <div class="form-group @if($errors->first('service_id')) has-error @endif">

   {!!Form::label('service_id','Service Name*')!!}
   {!! Form::servicesSelect('service_id',null) !!}
   <small class="text-danger">{{ $errors->first('service_id') }}</small>

 </div>
</div>

<div class="col-md-4" id="sub_service_div">
 <div class="form-group @if($errors->first('sub_service_id')) has-error @endif">

   {!!Form::label('sub_service_id','Sub Service Name*')!!}
   {!!Form::select('sub_service_id', array(),null, ['class' => 'form-control required','id'=>'sub_service_id', 'data-live-search' => 'true']) !!} 
   <small class="text-danger">{{ $errors->first('sub_service_id') }}</small>

 </div>
</div>

<div class="col-md-4">
  <div class="form-group @if($errors->first('business_name')) has-error @endif">
   {!!Form::label('business_name','Business Name')!!}
   {!!Form::text('business_name',Input::old('business_name'),['class' => 'form-control required','id'=>'business_name',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Business Name","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('business_name') }}</small>
 </div>
</div>

<div class="col-md-4">
  <div class="form-group @if($errors->first('locality')) has-error @endif">
   {!!Form::label('locality','Locality')!!}
   {!!Form::text('locality',Input::old('locality'),['class' => 'form-control required','id'=>'locality',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Locality","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('locality') }}</small>
 </div>
</div>

<div class="col-md-4">
  <div class="form-group @if($errors->first('location_lat')) has-error @endif">
   {!!Form::label('location_lat','Location Latitude')!!}
   {!!Form::text('location_lat',Input::old('location_lat'),['class' => 'form-control required number','id'=>'location_lat',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Location Latitude","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('location_lat') }}</small>
 </div>
</div>

<div class="col-md-4">
  <div class="form-group @if($errors->first('location_long')) has-error @endif">
   {!!Form::label('location_long','Location Longitude')!!}
   {!!Form::text('location_long',Input::old('location_long'),['class' => 'form-control required number','id'=>'location_long',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Location Longitude","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('location_long') }}</small>
 </div>
</div>

<div class="col-md-6">
<div class="col-md-12">
  <div class="form-group @if($errors->first('address')) has-error @endif">
   {!!Form::label('address','Address')!!}
   {!!Form::textarea('address',Input::old('address'),['class' => 'form-control required','id'=>'address',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Address","data-placement"=>"bottom","rows"=>3])!!}
   <small class="text-danger">{{ $errors->first('address') }}</small>
 </div>
</div>

<div class="col-md-8">
  <div class="form-group @if($errors->first('zip_code')) has-error @endif">
   {!!Form::label('zip_code','Zip code')!!}
   {!!Form::text('zip_code',Input::old('zip_code'),['class' => 'form-control','id'=>'zip_code',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Zip code","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('zip_code') }}</small>
 </div>
</div>
<div class="clearfix"></div>

<div class="col-md-4" >
 <div class="form-group @if($errors->first('country_code')) has-error @endif">

   {!!Form::label('country_code','Country Code')!!}
   {!!Form::select('country_code', array(),null, ['class' => 'form-control','id'=>'country_code', 'data-live-search' => 'true']) !!}
   <small class="text-danger">{{ $errors->first('country_code') }}</small>

 </div>
</div>

<div class="col-md-8">
  <div class="form-group @if($errors->first('phone_number')) has-error @endif">
   {!!Form::label('phone_number','Phone Number')!!}
   {!!Form::text('phone_number',Input::old('phone_number'),['class' => 'form-control','id'=>'phone_number',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Phone Number","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('phone_number') }}</small>
 </div>
</div>

<div class="col-md-12">
  <div class="form-group @if($errors->first('website')) has-error @endif">
   {!!Form::label('website','Website')!!}
   {!!Form::text('website',Input::old('website'),['class' => 'form-control','id'=>'website',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter Website","data-placement"=>"bottom",])!!}
   <small class="text-danger">{{ $errors->first('website') }}</small>
 </div>
</div>

</div>

<div class="col-md-6 table-responsive" >
{!!Form::label('website','Working Days')!!}
  <table class="table table-hover table-bordered">
    <tr>
      <th colspan="5">
        <div class="pull-right">
          <label class="checkbox-inline">
          <input type="checkbox" name="open_24x7" id="open_24x7" onclick="disableTableInputs()" ><b>Open 24x7</b></label>
          </div>
        </th>
      </tr>
      <tr>
        <th><input type="checkbox" id="check_all" name="check_all" ></th>
        <th>Day</th>
        <th>From Time</th>
        <th>To Time</th>
        <th>Open 24 hours</th>
      </tr>
      <tbody id="working_days_row">
        <tr>
          <td><input type="checkbox" id="sun" name="sun"></td>
          <td>Sun</td>
          <td style="padding:2px;">
          <input type="text"
          id="sun_from_time" name="sun_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sun_to_time" name="sun_to_time"  placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sun_open_24_hours" name="sun_open_24_hours" onclick="disableFromToTime('sun')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="mon" name="mon"></td>
          <td>Mon</td>
          <td style="padding:2px;">
          <input type="text" id="mon_from_time" name="mon_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="mon_to_time" name="mon_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="mon_open_24_hours" name="mon_open_24_hours" onclick="disableFromToTime('mon')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="tue" name="tue"></td>
          <td>Tue</td>
          <td style="padding:2px;">
          <input type="text" id="tue_from_time" name="tue_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="tue_to_time" name="tue_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="tue_open_24_hours" name="tue_open_24_hours" onclick="disableFromToTime('tue')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="wed" name="wed"></td>
          <td>Wed</td>
          <td style="padding:2px;">
          <input type="text" id="wed_from_time" name="wed_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="wed_to_time" name="wed_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="wed_open_24_hours" name="wed_open_24_hours" onclick="disableFromToTime('wed')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="thu" name="thu"></td>
          <td>Thu</td>
          <td style="padding:2px;">
          <input type="text" id="thu_from_time" name="thu_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="thu_to_time" name="thu_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="thu_open_24_hours" name="thu_open_24_hours" onclick="disableFromToTime('thu')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="fri" name="fri"></td>
          <td>Fri</td>
          <td style="padding:2px;">
          <input type="text" id="fri_from_time" name="fri_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="fri_to_time" name="fri_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="fri_open_24_hours" name="fri_open_24_hours" onclick="disableFromToTime('fri')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="sat" name="sat"></td>
          <td>Sat</td>
          <td style="padding:2px;">
          <input type="text" id="sat_from_time" name="sat_from_time" placeholder="09:00 AM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sat_to_time" name="sat_to_time" placeholder="09:00 PM" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sat_open_24_hours" name="sat_open_24_hours" onclick="disableFromToTime('sat')"></td>
        </tr>
      </tbody>
    </table>
  </div>
<div class="cleafix"></div>
<div class="col-md-8">
  <div class="form-group @if($errors->first('about')) has-error @endif">
   {!!Form::label('about','About')!!}
   {!!Form::textarea('about',Input::old('about'),['class' => 'form-control','id'=>'about',"data-toggle"=>"popover","data-trigger"=>"focus","title"=>"","data-content"=>"Enter About","data-placement"=>"bottom","rows"=>8])!!}
   <small class="text-danger">{{ $errors->first('about') }}</small>
 </div>
</div>

<!-- BANNER START -->
<div class="col-md-6">
  {!! Form::label('Banner Photo') !!}
  <input type="file" class="form-control" id="banner_file_name" name="banner_file_name">
</div>

<div class="col-md-2" style="margin-top:25px;" id="banner_btn_div">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_banner_btn', 'type' => 'button']) !!}
</div>

<input type="hidden" class="form-control" id="banner_file_path" name="banner_file_path">

<div class="col-md-12" style="margin-top:50px;" id="banner_photo_div" hidden>
<div class="col-md-10">
<img style="height:auto;width:600px;" src='' id="banner_photo"><button type="button" id="delete_banner_photo")>X</button>
</div>
</div>

<!-- BANNER END -->

<div class="clearfix margin"></div>

<!-- THUMBNAIL START -->

<div class="col-md-6" id="thumbnail_input_div">
  {!! Form::label('Thumbnail Photo') !!}
  <input type="file" class="form-control" id="thumbnail_file_name" name="thumbnail_file_name">
</div>

<div class="col-md-2" style="margin-top:25px;" id="thumbnail_btn_div">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_thumbnail_btn']) !!}
</div>

<input type="hidden" class="form-control" id="thumbnail_file_path" name="thumbnail_file_path">

<div class="col-md-12" style="margin-top:50px;" id="thumbnail_photo_div" hidden>
<div class="col-md-10">
<img style="height:auto;width:600px;" src='' id="thumbnail_photo"><button type="button" id="delete_thumbnail_photo")>X</button>
</div>
</div>

<!-- THUMBNAIL END -->

<div class="clearfix margin"></div>

<!-- PHOTOS START -->

<div class="col-md-6" id="attachment_div">
  {!! Form::label('Photos') !!}
  <input type="file" class="form-control" id="file_name" name="file_name">
</div>

<div class="col-md-2" style="margin-top:25px;">
  {!! Form::button('Upload Image', ['class' => 'btn btn-block btn-primary btn-block', 'id' => 'upload_btn', 'type' => 'button']) !!}
</div>

<div class="clearfix"></div>
<div class="col-md-12" id="photo_div" style="margin-top:50px;" >

</div>


<div class="col-md-4" hidden>
  <div class="form-group @if($errors->first('image_array')) has-error @endif">
   {!!Form::label('image_array','image_array*')!!}
   {!!Form::text('image_array',null,['class' => 'form-control','id'=>'image_array',"data-placement"=>"bottom"])!!}
   <small class="text-danger">{{ $errors->first('image_array') }}</small>

 </div>
</div>

<!-- PHOTOS END -->

<div class='clearfix' style="margin-top:25px;"></div>
<div class="col-md-2 pull-right">
<a href="{{URL::route('tune_orbit_bo.service_page.services.index')}}">{!! Form::button('Cancel', ['class' => 'btn btn-block btn-danger btn-block','id'=>'clr-btn']) !!}</a>
</div>
<div class="col-md-2 pull-right">
  <div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-block btn-success btn-block']) !!}
  </div>
</div>


@section('script')
@parent
<script type="text/javascript">
  $(function(){
  tinymce.init({
        selector: '#about',
        height: 500,
            theme: 'modern',
            plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
            ],
           toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
           toolbar2: 'print preview media | forecolor backcolor emoticons fontsizeselect jbimages',
           image_advtab: true,
           fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
           templates: [
           { title: 'Test template 1', content: 'Test 1' },
           { title: 'Test template 2', content: 'Test 2' }
           ],
           content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
           ],
            font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
           relative_urls: false
    });
  });
</script>
@stop