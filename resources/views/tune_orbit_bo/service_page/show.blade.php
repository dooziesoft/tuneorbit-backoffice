@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'View Service Page')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">View service page details</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::model($service_page,array('route' => array('tune_orbit_bo.service_page.services.show',$service_page->id), 'method' => 'PUT','files'=>true))!!}
          <div class="col-md-4">
           <div class="form-group @if($errors->first('service_name')) has-error @endif">

             {!!Form::label('service_name','Service Name*')!!}
             {!!Form::text('service_name',Input::old('service_name'),['class' => 'form-control','id'=>'service_name','disabled'=>'true'])!!}
             <small class="text-danger">{{ $errors->first('service_name') }}</small>

           </div>
         </div>

         <div class="col-md-4">
           <div class="form-group @if($errors->first('sub_service_name')) has-error @endif">

             {!!Form::label('sub_service_name','Sub Service Name*')!!}
             {!!Form::text('sub_service_name',Input::old('sub_service_name'),['class' => 'form-control','id'=>'sub_service_name','disabled'=>'true'])!!}
             <small class="text-danger">{{ $errors->first('sub_service_name') }}</small>

           </div>
         </div>

         <div class="col-md-4">
          <div class="form-group @if($errors->first('business_name')) has-error @endif">
           {!!Form::label('business_name','Business Name')!!}
           {!!Form::text('business_name',Input::old('business_name'),['class' => 'form-control','id'=>'business_name','disabled'=>'true'])!!}
           <small class="text-danger">{{ $errors->first('business_name') }}</small>
         </div>
       </div>

       <div class="col-md-4">
        <div class="form-group @if($errors->first('locality')) has-error @endif">
         {!!Form::label('locality','Locality')!!}
         {!!Form::text('locality',Input::old('locality'),['class' => 'form-control','id'=>'locality',"disabled"=>"true",])!!}
         <small class="text-danger">{{ $errors->first('locality') }}</small>
       </div>
     </div>

       <div class="col-md-4">
        <div class="form-group @if($errors->first('location_lat')) has-error @endif">
         {!!Form::label('location_lat','Location Latitude')!!}
         {!!Form::text('location_lat',Input::old('location_lat'),['class' => 'form-control number','id'=>'location_lat','disabled'=>'true'])!!}
         <small class="text-danger">{{ $errors->first('location_lat') }}</small>
       </div>
     </div>

     <div class="col-md-4">
      <div class="form-group @if($errors->first('location_long')) has-error @endif">
       {!!Form::label('location_long','Location Longitude')!!}
       {!!Form::text('location_long',Input::old('location_long'),['class' => 'form-control number','id'=>'location_long','disabled'=>'true'])!!}
       <small class="text-danger">{{ $errors->first('location_long') }}</small>
     </div>
   </div>

<div class="col-md-6">
   <div class="col-md-12">
    <div class="form-group @if($errors->first('address')) has-error @endif">
     {!!Form::label('address','Address')!!}
     {!!Form::textarea('address',Input::old('address'),['class' => 'form-control','id'=>'address','disabled'=>'true',"rows"=>3])!!}
     <small class="text-danger">{{ $errors->first('address') }}</small>
   </div>
 </div>
 
 <div class="col-md-12">
  <div class="form-group @if($errors->first('phone_number')) has-error @endif">
   {!!Form::label('phone_number','Phone Number')!!}
   {!!Form::text('phone_number',Input::old('phone_number'),['class' => 'form-control','id'=>'phone_number','disabled'=>'true'])!!}
   <small class="text-danger">{{ $errors->first('phone_number') }}</small>
 </div>
</div>

<div class="col-md-12">
  <div class="form-group @if($errors->first('website')) has-error @endif">
   {!!Form::label('website','Website')!!}
   {!!Form::text('website',Input::old('website'),['class' => 'form-control','id'=>'website','disabled'=>'true'])!!}
   <small class="text-danger">{{ $errors->first('website') }}</small>
 </div>
</div>
</div>

<div class="col-md-6 table-responsive" >
{!!Form::label('website','Working Days')!!}
  <table class="table table-hover table-bordered">
    <tr>
      <th colspan="5">
        <div class="pull-right">
          <label class="checkbox-inline">
          <input type="checkbox" name="open_24x7" id="open_24x7" disabled="true"><b>Open 24x7</b></label>
          </div>
        </th>
      </tr>
      <tr>
        <th></th>
        <th>Day</th>
        <th>From Time</th>
        <th>To Time</th>
        <th>Open 24 hours</th>
      </tr>
      <tbody id="working_days_row">
        <tr>
          <td><input type="checkbox" id="sun" name="sun"></td>
          <td>Sun</td>
          <td style="padding:2px;">
          <input type="text"
          id="sun_from_time" name="sun_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sun_to_time" name="sun_to_time"  style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sun_open_24_hours" name="sun_open_24_hours" onclick="disableFromToTime('sun')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="mon" name="mon"></td>
          <td>Mon</td>
          <td style="padding:2px;">
          <input type="text" id="mon_from_time" name="mon_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="mon_to_time" name="mon_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="mon_open_24_hours" name="mon_open_24_hours" onclick="disableFromToTime('mon')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="tue" name="tue"></td>
          <td>Tue</td>
          <td style="padding:2px;">
          <input type="text" id="tue_from_time" name="tue_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="tue_to_time" name="tue_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="tue_open_24_hours" name="tue_open_24_hours" onclick="disableFromToTime('tue')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="wed" name="wed"></td>
          <td>Wed</td>
          <td style="padding:2px;">
          <input type="text" id="wed_from_time" name="wed_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="wed_to_time" name="wed_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="wed_open_24_hours" name="wed_open_24_hours" onclick="disableFromToTime('wed')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="thu" name="thu"></td>
          <td>Thu</td>
          <td style="padding:2px;">
          <input type="text" id="thu_from_time" name="thu_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="thu_to_time" name="thu_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="thu_open_24_hours" name="thu_open_24_hours" onclick="disableFromToTime('thu')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="fri" name="fri"></td>
          <td>Fri</td>
          <td style="padding:2px;">
          <input type="text" id="fri_from_time" name="fri_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="fri_to_time" name="fri_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="fri_open_24_hours" name="fri_open_24_hours" onclick="disableFromToTime('fri')"></td>
        </tr>
        <tr>
          <td><input type="checkbox" id="sat" name="sat"></td>
          <td>Sat</td>
          <td style="padding:2px;">
          <input type="text" id="sat_from_time" name="sat_from_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td style="padding:2px;">
          <input type="text" id="sat_to_time" name="sat_to_time" style="border:none;margin:0;padding:0;height:40px;text-align:center;">
          </td>
          <td><input type="checkbox" id="sat_open_24_hours" name="sat_open_24_hours" onclick="disableFromToTime('sat')"></td>
        </tr>
      </tbody>
    </table>
  </div>

<div class="col-md-8">
  <div class="form-group @if($errors->first('about')) has-error @endif">
   {!!Form::label('about','About')!!}
   {!!Form::textarea('about',Input::old('about'),['class' => 'form-control','id'=>'about','disabled'=>'true',"rows"=>8])!!}
   <small class="text-danger">{{ $errors->first('about') }}</small>
 </div>
</div>

<!-- BANNER START -->
<div class="col-md-6">
  {!! Form::label('Banner Photo') !!}
  <div class="clearfix"></div>
  <div class="col-md-6">
    <img style="height:auto;width:300px;" src='{{$service_page->banner_image}}' id="banner_photo">
  </div>
</div>

<!-- BANNER END -->

<!-- THUMBNAIL START -->

<div class="col-md-6" id="thumbnail_input_div">
  {!! Form::label('Thumbnail Photo') !!}
  <div class="clearfix"></div>
  <div class="col-md-6">
    <img style="height:auto;width:300px;" src='{{$service_page->thumbnail_image}}' id="thumbnail_photo">
  </div>
</div>

<!-- THUMBNAIL END -->

<div class="clearfix margin"></div>

<!-- PHOTOS START -->

<div class="col-md-12" id="attachment_div">
  {!! Form::label('Photos') !!}
  <div class="clearfix"></div>
  @foreach($service_page_photos as $spp)
  <div class="col-md-3">
    <img style="height:auto;width:150px;" src='http://tuneorbit.com/server/img/user_service/{{$spp->image_path}}' id="banner_photo">
  </div>
  @endforeach
</div>

<!-- PHOTOS END -->

<div class='clearfix' style="margin-top:25px;"></div>
<div class="col-md-2 pull-right">
  <a href="{{URL::route('tune_orbit_bo.service_page.services.index')}}">{!! Form::button('Close', ['class' => 'btn btn-block btn-danger btn-block margin','id'=>'clr-btn']) !!}</a>
</div>
</div><!-- /.box -->
</div><!-- /.col -->

</div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
    var service_page = <?php echo json_encode($service_page) ?>;
    $('#working_days_row').find('input').attr('disabled',true);
    if(service_page['open_24x7'] == 1){
        $('#open_24x7').attr('checked',true);
      }else{
        $('#open_24x7').attr('checked',false);
      var working_day_str=service_page['working_days'].split(',');
      var from_time_str=service_page['from_time'].split(',');
      var to_time_str=service_page['to_time'].split(',');
      if(working_day_str[6].split(':').pop() == 'true}"'){
        $('#sun').attr('checked',true);
        if(from_time_str[6].split(':').pop() == 'open_24_hours}'){
          $('#sun_open_24_hours').attr('checked',true);
          $('#sun_from_time').val('---');
          $('#sun_to_time').val('---');
        }else{
          $('#sun_open_24_hours').attr('checked',false);
          $('#sun_from_time').val(from_time_str[6].substring(6).slice(0,-1));
          $('#sun_to_time').val(to_time_str[6].substring(6).slice(0,-1));
        }
      }else{
        $('#sun').attr('checked',false);
      }
      if(working_day_str[0].split(':').pop() == 'true'){
        $('#mon').attr('checked',true);
        if(from_time_str[0].split(':').pop() == 'open_24_hours'){
          $('#mon_open_24_hours').attr('checked',true);
          $('#mon_from_time').val('---');
          $('#mon_to_time').val('---');
        }else{
          $('#mon_open_24_hours').attr('checked',false);
          $('#mon_from_time').val(from_time_str[0].substring(7));
          $('#mon_to_time').val(to_time_str[0].substring(7));
        }
      }else{
        $('#mon').attr('checked',false);
      }
      if(working_day_str[1].split(':').pop() == 'true'){
        $('#tue').attr('checked',true);
        if(from_time_str[1].split(':').pop() == 'open_24_hours'){
          $('#tue_open_24_hours').attr('checked',true);
          $('#tue_from_time').val('---');
          $('#tue_to_time').val('---');
        }else{
          $('#tue_open_24_hours').attr('checked',false);
          $('#tue_from_time').val(from_time_str[1].substring(6));
          $('#tue_to_time').val(to_time_str[1].substring(6));
        }
      }else{
        $('#tue').attr('checked',false);
      }
      if(working_day_str[2].split(':').pop() == 'true'){
        $('#wed').attr('checked',true);
        if(from_time_str[2].split(':').pop() == 'open_24_hours'){
          $('#wed_open_24_hours').attr('checked',true);
          $('#wed_from_time').val('---');
          $('#wed_to_time').val('---');
        }else{
          $('#wed_open_24_hours').attr('checked',false);
          $('#wed_from_time').val(from_time_str[2].substring(6));
          $('#wed_to_time').val(to_time_str[2].substring(6));
        }
      }else{
        $('#wed').attr('checked',false);
      }
      if(working_day_str[3].split(':').pop() == 'true'){
        $('#thu').attr('checked',true);
        if(from_time_str[3].split(':').pop() == 'open_24_hours'){
          $('#thu_open_24_hours').attr('checked',true);
          $('#thu_from_time').val('---');
          $('#thu_to_time').val('---');
        }else{
          $('#thu_open_24_hours').attr('checked',false);
          $('#thu_from_time').val(from_time_str[3].substring(6));
          $('#thu_to_time').val(to_time_str[3].substring(6));
        }
      }else{
        $('#thu').attr('checked',false);
      }
      if(working_day_str[4].split(':').pop() == 'true'){
        $('#fri').attr('checked',true);
        if(from_time_str[4].split(':').pop() == 'open_24_hours'){
          $('#fri_open_24_hours').attr('checked',true);
          $('#fri_from_time').val('---');
          $('#fri_to_time').val('---');
        }else{
          $('#fri_open_24_hours').attr('checked',false);
          $('#fri_from_time').val(from_time_str[4].substring(6));
          $('#fri_to_time').val(to_time_str[4].substring(6));
        }
      }else{
        $('#fri').attr('checked',false);
      }
      if(working_day_str[5].split(':').pop() == 'true'){
        $('#sat').attr('checked',true);
        if(from_time_str[5].split(':').pop() == 'open_24_hours'){
          $('#sat_open_24_hours').attr('checked',true);
          $('#sat_from_time').val('---');
          $('#sat_to_time').val('---');
        }else{
          $('#sat_open_24_hours').attr('checked',false);
          $('#sat_from_time').val(from_time_str[5].substring(6));
          $('#sat_to_time').val(to_time_str[5].substring(6));
        }
      }else{
        $('#sat').attr('checked',false);
      }
    }
    });
  </script>
@stop
