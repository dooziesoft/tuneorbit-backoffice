@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'Add Service Page')

@section('content')
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add service page details</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <div class="clearfix"></div>
        <div class="col-md-12">
          {!!Form::open(array('route' => array('tune_orbit_bo.service_page.services.store'), 'method' => 'POST','files'=>true,'id'=>'add-form','onsubmit'=>'return validate()'))!!}
          
          @include('tune_orbit_bo.service_page._form',['submitButtonText'=>'Save'])


          <div class='clearfix'></div>


          {!!Form::close()!!}
        </div>
        <div class="box-footer">

        </div><!-- /.box-footer-->
      </div><!-- /.box -->
    </div><!-- /.col -->

  </div>
</div><!-- /.row -->
@endsection
@section('script')
@parent
<script type="text/javascript">
  $(function(){
   $('[data-toggle="popover"]').popover(); 

   @if(Session::has('message'))
   $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
   @endif

   var image_array={},image_i=0;

   $('#service_id').change(function(){
    $('#sub_service_id').val('');
    $('#sub_service_div').html('<label>Sub Service Name*</label><select type="text" class="form-control"  id="sub_service_id" name="sub_service_id" data-live-search="true"></select>')
    var service_id=$('#service_id').val();
    $.ajax({
      type: 'get',
      url:'{{URL::route("service_page.services.getspecificSubservices")}}',
      dataType: 'json',
      data:{service_id:service_id},
    }).done(function(result){
     console.log(result);
     var sub_services=result['sub_services'];
     var dis='<option value=0>Select Sub Services</option>';
     for(var i=0;i<sub_services.length;i++){
      dis+='<option value='+sub_services[i]['id']+'>'+sub_services[i]['name']+'</option>';
     }
     $('#sub_service_id').html(dis).selectpicker();
    });

   })

   $('#upload_banner_btn').click(function(){

      var formData = new FormData($('#add-form')[0]);
      var ajax = $.ajax({
       type: 'post',
       url:'{{URL::route("tune_orbit_bo.masters.sub_services.uploadserviceImg")}}',
       data: formData,
       contentType: false,
       processData: false
     }).done(function(result) {
        console.log(result);
        if(result){
          $('#banner_file_name').hide();
          $('#banner_btn_div').hide();
          $('#banner_photo_div').show();
        var path=result['path'];
        var sp = path.split('/');
        var file = sp[sp.length-1];
        $('#banner_photo').attr('src',result['path']);
        $('#banner_file_name').val('');
        $('#banner_file_path').val(file);
      }else{
        $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
      }
    }).fail(function() {
        alert("fail");
      });


    });

   $('#delete_banner_photo').click(function(){
    console.log($('#banner_photo').attr('src'));
    $.ajax({
      type: 'get',
      url:'{{URL::route("service_page.services.deleteSpecificImg")}}',
      dataType: 'json',
      data:{path:$('#banner_photo').attr('src')},
    }).done(function(result){
      $('#banner_file_name').show();
      $('#banner_btn_div').show();
      $('#banner_photo_div').hide();
      $('#banner_photo').attr('src','');
      $('#banner_file_path').val('');
    });
   })

   $('#upload_thumbnail_btn').click(function(){

      var formData = new FormData($('#add-form')[0]);
      var ajax = $.ajax({
       type: 'post',
       url:'{{URL::route("tune_orbit_bo.masters.sub_services.uploadserviceImg")}}',
       data: formData,
       contentType: false,
       processData: false
     }).done(function(result) {
        console.log(result);
        if(result){
          $('#thumbnail_file_name').hide();
          $('#thumbnail_btn_div').hide();
          $('#thumbnail_photo_div').show();
        var path=result['path'];
        var sp = path.split('/');
        var file = sp[sp.length-1];
        $('#thumbnail_photo').attr('src',result['path']);
        $('#thumbnail_file_name').val('');
        $('#thumbnail_file_path').val(file);
      }else{
        $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
      }
    }).fail(function() {
        alert("fail");
      });


    });

   $('#delete_thumbnail_photo').click(function(){
    console.log($('#thumbnail_photo').attr('src'));
    $.ajax({
      type: 'get',
      url:'{{URL::route("service_page.services.deleteSpecificImg")}}',
      dataType: 'json',
      data:{path:$('#thumbnail_photo').attr('src')},
    }).done(function(result){
      $('#thumbnail_file_name').show();
      $('#thumbnail_btn_div').show();
      $('#thumbnail_photo_div').hide();
      $('#thumbnail_photo').attr('src','');
      $('#thumbnail_file_path').val('');
    });
   })

 $('#upload_btn').click(function(){

      var formData = new FormData($('#add-form')[0]);
      var ajax = $.ajax({
       type: 'post',
       url:'{{URL::route("tune_orbit_bo.masters.sub_services.uploadserviceImg")}}',
       data: formData,
       contentType: false,
       processData: false
     }).done(function(result) {
        console.log(result);
        if(result){
        var path=result['path'];
        var sp = path.split('/');
        var file = sp[sp.length-1];
        $('#photo_div').append('<div class="col-md-4" id="photo_frame'+image_i+'"><img style="height:auto;width:250px;" src='+result['path']+' id="photo_'+image_i+'"><button type="button" id="btn_'+image_i+'" onclick=deletePhoto('+image_i+',"'+encodeURIComponent(file)+'",2)>X</button></div>');
        image_array[image_i]={};
        image_array[image_i]=file;
        image_i++;
        $('#image_array').val(JSON.stringify(image_array));
        $('#file_name').val('');
      }else{
        $.notify(" Please Choose file.",{
          type:'danger',
        });
        return false;
      }
    }).fail(function() {
        alert("fail");
      });


    });

$("#check_all").click(function () {
     $('#sun').not(this).prop('checked', this.checked);
     $('#mon').not(this).prop('checked', this.checked);
     $('#tue').not(this).prop('checked', this.checked);
     $('#wed').not(this).prop('checked', this.checked);
     $('#thu').not(this).prop('checked', this.checked);
     $('#fri').not(this).prop('checked', this.checked);
     $('#sat').not(this).prop('checked', this.checked);
 });

var countries=[
'Afghanistan',
'Albania',
'Algeria',
'American Samoa',
'Andorra',
'Angola',
'Anguilla',
'Antarctica',
'Antigua and Barbuda',
'Argentina',
'Armenia',
'Aruba',
'Australia',
'Austria',
'Azerbaijan',
'Bahamas',
'Bahrain',
'Bangladesh',
'Barbados',
'Belarus',
'Belgium',
'Belize',
'Benin',
'Bermuda',
'Bhutan',
'Bolivia',
'Bosnia and Herzegovina',
'Botswana',
'Brazil',
'British Indian Ocean Territory',
'British Virgin Islands',
'Brunei',
'Bulgaria',
'Burkina Faso',
'Burundi',
'Cambodia',
'Cameroon',
'Canada',
'Cape Verde',
'Cayman Islands',
'Central African Republic',
'Chad',
'Chile',
'China',
'Christmas Island',
'Cocos Islands',
'Colombia',
'Comoros',
'Cook Islands',
'Costa Rica',
'Croatia',
'Cuba',
'Curacao',
'Cyprus',
'Czech Republic',
'Democratic Republic of the Congo',
'Denmark',
'Djibouti',
'Dominica',
'Dominican Republic',
'East Timor',
'Ecuador',
'Egypt',
'El Salvador',
'Equatorial Guinea',
'Eritrea',
'Estonia',
'Ethiopia',
'Falkland Islands',
'Faroe Islands',
'Fiji',
'Finland',
'France',
'French Polynesia',
'Gabon',
'Gambia',
'Georgia',
'Germany',
'Ghana',
'Gibraltar',
'Greece',
'Greenland',
'Grenada',
'Guam',
'Guatemala',
'Guernsey',
'Guinea',
'Guinea-Bissau',
'Guyana',
'Haiti',
'Honduras',
'Hong Kong',
'Hungary',
'Iceland',
'India',
'Indonesia',
'Iran',
'Iraq',
'Ireland',
'Isle of Man',
'Israel',
'Italy',
'Ivory Coast',
'Jamaica',
'Japan',
'Jersey',
'Jordan',
'Kazakhstan',
'Kenya',
'Kiribati',
'Kosovo',
'Kuwait',
'Kyrgyzstan',
'Laos',
'Latvia',
'Lebanon',
'Lesotho',
'Liberia',
'Libya',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macau',
'Macedonia',
'Madagascar',
'Malawi',
'Malaysia',
'Maldives',
'Mali',
'Malta',
'Marshall Islands',
'Mauritania',
'Mauritius',
'Mayotte',
'Mexico',
'Micronesia',
'Moldova',
'Monaco',
'Mongolia',
'Montenegro',
'Montserrat',
'Morocco',
'Mozambique',
'Myanmar',
'Namibia',
'Nauru',
'Nepal',
'Netherlands',
'Netherlands Antilles',
'New Caledonia',
'New Zealand',
'Nicaragua',
'Niger',
'Nigeria',
'Niue',
'North Korea',
'Northern Mariana Islands',
'Norway',
'Oman',
'Pakistan',
'Palau',
'Palestine',
'Panama',
'Papua New Guinea',
'Paraguay',
'Peru',
'Philippines',
'Pitcairn',
'Poland',
'Portugal',
'Puerto Rico',
'Qatar',
'Republic of the Congo',
'Reunion',
'Romania',
'Russia',
'Rwanda',
'Saint Barthelemy',
'Saint Helena',
'Saint Kitts and Nevis',
'Saint Lucia',
'Saint Martin',
'Saint Pierre and Miquelon',
'Saint Vincent and the Grenadines',
'Samoa',
'San Marino',
'Sao Tome and Principe',
'Saudi Arabia',
'Senegal',
'Serbia',
'Seychelles',
'Sierra Leone',
'Singapore',
'Sint Maarten',
'Slovakia',
'Slovenia',
'Solomon Islands',
'Somalia',
'South Africa',
'South Korea',
'South Sudan',
'Spain',
'Sri Lanka',
'Sudan',
'Suriname',
'Svalbard and Jan Mayen',
'Swaziland',
'Sweden',
'Switzerland',
'Syria',
'Taiwan',
'Tajikistan',
'Tanzania',
'Thailand',
'Togo',
'Tokelau',
'Tonga',
'Trinidad and Tobago',
'Tunisia',
'Turkey',
'Turkmenistan',
'Turks and Caicos Islands',
'Tuvalu',
'U.S. Virgin Islands',
'Uganda',
'Ukraine',
'United Arab Emirates',
'United Kingdom',
'United States',
'Uruguay',
'Uzbekistan',
'Vanuatu',
'Vatican',
'Venezuela',
'Vietnam',
'Wallis and Futuna',
'Western Sahara',
'Yemen',
'Zambia',
'Zimbabwe'
]

var country_codes=[
'93',
'355',
'213',
'1-684',
'376',
'244',
'1-264',
'672',
'1-268',
'54',
'374',
'297',
'61',
'43',
'994',
'1-242',
'973',
'880',
'1-246',
'375',
'32',
'501',
'229',
'1-441',
'975',
'591',
'387',
'267',
'55',
'246',
'1-284',
'673',
'359',
'226',
'257',
'855',
'237',
'1',
'238',
'1-345',
'236',
'235',
'56',
'86',
'61',
'61',
'57',
'269',
'682',
'506',
'385',
'53',
'599',
'357',
'420',
'243',
'45',
'253',
'1-767',
'1-809, 1-829, 1-849',
'670',
'593',
'20',
'503',
'240',
'291',
'372',
'251',
'500',
'298',
'679',
'358',
'33',
'689',
'241',
'220',
'995',
'49',
'233',
'350',
'30',
'299',
'1-473',
'1-671',
'502',
'44-1481',
'224',
'245',
'592',
'509',
'504',
'852',
'36',
'354',
'91',
'62',
'98',
'964',
'353',
'44-1624',
'972',
'39',
'225',
'1-876',
'81',
'44-1534',
'962',
'7',
'254',
'686',
'383',
'965',
'996',
'856',
'371',
'961',
'266',
'231',
'218',
'423',
'370',
'352',
'853',
'389',
'261',
'265',
'60',
'960',
'223',
'356',
'692',
'222',
'230',
'262',
'52',
'691',
'373',
'377',
'976',
'382',
'1-664',
'212',
'258',
'95',
'264',
'674',
'977',
'31',
'599',
'687',
'64',
'505',
'227',
'234',
'683',
'850',
'1-670',
'47',
'968',
'92',
'680',
'970',
'507',
'675',
'595',
'51',
'63',
'64',
'48',
'351',
'1-787, 1-939',
'974',
'242',
'262',
'40',
'7',
'250',
'590',
'290',
'1-869',
'1-758',
'590',
'508',
'1-784',
'685',
'378',
'239',
'966',
'221',
'381',
'248',
'232',
'65',
'1-721',
'421',
'386',
'677',
'252',
'27',
'82',
'211',
'34',
'94',
'249',
'597',
'47',
'268',
'46',
'41',
'963',
'886',
'992',
'255',
'66',
'228',
'690',
'676',
'1-868',
'216',
'90',
'993',
'1-649',
'688',
'1-340',
'256',
'380',
'971',
'44',
'1',
'598',
'998',
'678',
'379',
'58',
'84',
'681',
'212',
'967',
'260',
'263'
];
var country_code='<option value="0">Select Country Code</option>';
for(var cc=0;cc<countries.length;cc++){
  country_code+='<option value='+country_codes[cc]+'>'+countries[cc]+' +'+country_codes[cc]+'</option>';
}
$('#country_code').html(country_code).selectpicker();
 });

function disableTableInputs(){
  if($('#open_24x7').is(':checked')){
    $('#check_all').attr('disabled',true);
    $('#check_all').attr('checked',false);
     $('#working_days_row').find('input').attr('disabled',true);
     $('#working_days_row').find('input').val('');
     $('#working_days_row').find('input[type=checkbox]:checked').attr('checked',false);
   }else{
    $('#check_all').attr('disabled',false);
   $('#working_days_row').find('input').attr('disabled',false);
 }         
}

function disableFromToTime(day){
    var str=day+'_open_24_hours';
    var from_str=day+'_from_time';
    var to_str=day+'_to_time';
    if($('#'+str).is(':checked')){
      $('#'+from_str).attr('disabled',true);
      $('#'+from_str).val('');
      $('#'+to_str).attr('disabled',true);
      $('#'+to_str).val('');
    }else{
      $('#'+from_str).attr('disabled',false);
      $('#'+to_str).attr('disabled',false);
    }
  }

 function deletePhoto(id,path,type){
    $.ajax({
      type: 'get',
      url:'{{URL::route("masters.sub_services.deleteSpecificServiceImg")}}',
      dataType: 'json',
      data:{id:id,path:path,type:type},
    }).done(function(result){
        $('#photo_frame'+id).hide();
        delete image_array[id];
        $('#image_array').val(JSON.stringify(image_array));
    });

  }


  /*function validate()
  {
    if($('#add-form').valid()){
      if($('#service_id').val() == 0){
        $.notify("Select service name.",{
          type:'danger',
        });
        return false;
      }else if($('#sub_service_id').val() == 0){
        $.notify("Select sub service name.",{
          type:'danger',
        });
        return false;
      }else if($('#banner_file_path').val().length == 0){
        $.notify("Upload banner photo.",{
          type:'danger',
        });
        return false;
      }else if($('#thumbnail_file_path').val().length == 0){
        $.notify("Upload thumbnail photo.",{
          type:'danger',
        });
        return false;
      }else{
        return true;
      }
    }else{
      return false;
    }
  }*/

  function validate()
  {
    var flag=1;
    if($('#add-form').valid()){
      if($('#service_id').val() == 0){
        $.notify("Select service name.",{
          type:'danger',
        });
        return false;
      }else if($('#sub_service_id').val() == 0){
        $.notify("Select sub service name.",{
          type:'danger',
        });
        return false;
      }else{
        if(!$('#open_24x7').is(':checked') && !$('#sun').is(':checked') && !$('#mon').is(':checked') && !$('#tue').is(':checked') && !$('#wed').is(':checked') && !$('#thu').is(':checked') && !$('#fri').is(':checked') && !$('#sat').is(':checked')){
          flag=0;
        }
        var arr=['sun','mon','tue','wed','thu','fri','sat'];
        for(var i=0;i<arr.length;i++){
          var str=arr[i]+'_open_24_hours';
          var from_str=arr[i]+'_from_time';
          var to_str=arr[i]+'_to_time';
          if($('#'+arr[i]).is(':checked')){
            if((Number($('#'+from_str).val()) == 0 || Number($('#'+to_str).val()) == 0) && !$('#'+str).is(':checked')){
              flag=2;
            }
          }
        }
        if(flag == 1){
          return true;
        }else if(flag == 0){
          $.notify("Please enter working days details!",{
            type:'danger',
          });
          return false;
        }else if(flag == 2){
          $.notify("Please enter proper timings for selected days!",{
            type:'danger',
          });
          return false;

        }
      }
    }else{
      return false;
    }
  }
</script>
@stop
