@extends('tune_orbit_bo.layouts.dashboard')

@section('title', 'Service Page')

@section('page_title_sub', 'Manage Service Page')

@section('content')

          
<div class='row'>
  <div class='col-md-12'>
    <!-- Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">List of Services</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          {{-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>

      <div class="box-body table-responsive no-padding">
        
        <div class="clearfix"></div>
        <div class="col-md-12">
          <div class="pull-right">

             <a class="btn bg-maroon margin" href="{{route('tune_orbit_bo.service_page.services.create')}}" data-toggle="tooltip" data-placement="bottom" title="Click here to add New Service Page">
             <i class="glyphicon glyphicon-pencil"></i> Add Service Page
           </a> 

           <a type="button" class="btn bg-navy margin" id="edit_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Edit">
            <i class="glyphicon glyphicon-edit"></i> Edit Service Page
          </a>

          <a type="button" class="btn bg-aqua margin" id="view_btn" onclick="no_select()"  data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click View">
            <i class="glyphicon glyphicon-eye-open"></i> View Service Page
          </a>
           
          <!-- <a type="button" class="btn bg-red margin" onclick="no_select()" id="deactivate_btn" data-toggle="tooltip" data-placement="bottom" title="Select a row from below table and then Click Activate/De-Activate">
            <i class="glyphicon glyphicon-trash"></i> Activate/Deactivate
          </a> -->

         
        </div><div class="clearfix"></div>
        <table class="table table-bordered" id="view">
          <thead>
            <tr class="bg-blue">
              <th></th>
              <th>Service</th>
              <th>Sub Service</th>
              <th>Business Name</th>
              <th>Address</th>
              <th>Phone Number</th>
              <th>Website</th>
            </tr>  

            <tr tr class="bg-blue">
            <th></th>
            <th><input type="text" class="form-control filter" data-col="Service"></th>
            <th><input type="text" class="form-control filter" data-col="Sub Service"></th>
            <th><input type="text" class="form-control filter" data-col="Business Name"></th>
            <th><input type="text" class="form-control filter" data-col="Address"></th>
            <th><input type="text" class="form-control filter" data-col="Phone Number"></th>
            <th><input type="text" class="form-control filter" data-col="Website"></th>
             </tr>
        </thead>
          <tbody>
            @foreach($service_pages as $sp)
            <tr>
              <td><input type="radio" id='{{$sp->id}}' name='ch'></td>
              
              <td>{{$sp->service_name}}</td>
              <td>{{$sp->sub_service_name}}</td>
              <td>{{$sp->business_name}}</td>
              <td>{{$sp->address}}</td>
              <td>{{$sp->phone_number}}</td>
              <td>{{$sp->website}}</td>

            </tr>
            @endforeach
          </tbody>
          <div class="clearfix"></div>

        </table>
     

        <div class="clearfix"></div>
        
        
      </div>
      <div class="box-footer">
        {{-- <form action='#'>
        <input type='text' placeholder='New task' class='form-control input-sm' />
      </form> --}}
    </div><!-- /.box-footer-->
  </div><!-- /.box -->
</div><!-- /.col -->


</div><!-- /.row -->
@endsection

@section('script')
@parent

<script type="text/javascript">

 $(function(){
  $('.filter').multifilter({'target':$('#view')});
  $('input[name=ch]:radio').attr('checked',false);
  $('input[name=ch]:radio').change(function(){
    var id=$(this).attr('id');
    var status=$(this).attr('status');
    $("#view_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/service_page/services/"+id);
    $("#view_btn").attr('onclick',"");

    $("#edit_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/service_page/services/"+id+"/edit");
    $("#edit_btn").attr('onclick',"");
    
    $("#deactivate_btn").attr('href',"{{URL::to('/')}}/tune_orbit_bo/masters/services/deactivate/"+id);
    if(status){
      $("#deactivate_btn").attr('onclick',"return confirm_delete('Activate');");
    }else{
      $("#deactivate_btn").attr('onclick',"return confirm_delete('Deactivate');");
    }
    
  });
  @if(Session::has('message'))
  $.notify("{{Session::get('message')}}",{
    type:'{{Session::get("er_type")}}',
  });
  @endif
});
 function no_select(){
    $.notify("Please select row from below table",{
      type:'danger',
    });
  }
  function confirm_delete(status){
    if (!confirm('Do you really want to '+status+' services?')) {
    return false;
  }
}

</script>
@stop