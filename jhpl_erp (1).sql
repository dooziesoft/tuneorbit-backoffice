-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2016 at 06:26 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jhpl_erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_master_id` int(11) NOT NULL,
  `cart_quantity` double NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commercial_tax_master`
--

CREATE TABLE IF NOT EXISTS `commercial_tax_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `commercial_tax_master`
--

INSERT INTO `commercial_tax_master` (`id`, `tax_type`, `vat`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'service', 0.6, '', 1, 1, '2016-04-27 05:55:34', '2016-04-28 06:39:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_master`
--

CREATE TABLE IF NOT EXISTS `company_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` double NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weekly_off` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat_register_number` double NOT NULL,
  `tin_number` double NOT NULL,
  `cst_register_number` double NOT NULL,
  `excise_register_number` double NOT NULL,
  `ecc_number` double NOT NULL,
  `excise_range` double NOT NULL,
  `excise_division` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stamps` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_of_excise_commodity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_master`
--

INSERT INTO `company_master` (`id`, `company_name`, `address`, `contact_number`, `logo`, `weekly_off`, `vat_register_number`, `tin_number`, `cst_register_number`, `excise_register_number`, `ecc_number`, `excise_range`, `excise_division`, `stamps`, `name_of_excise_commodity`, `updated_by`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Doozie solution', 'Mysur Road, Benglore', 98776688712, '', 'sunday', 567567, 4543, 3454, 34553, 345534, 34543, '3454', '', 'software', '', '', '0000-00-00 00:00:00', '2016-04-28 07:03:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_vat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ecc_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit_days` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `range` text COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commissionarate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_billing_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_shipping_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `customer_mail`, `customer_contact_number`, `contact_person_name`, `contact_person_number`, `additional_contact_number`, `contact_person_email`, `base_discount`, `customer_vat`, `ecc_number`, `credit_days`, `range`, `division`, `commissionarate`, `customer_billing_address`, `customer_shipping_address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Deepak ss', 'deepakss@gmail.com', '9893213984', 'Akshay joshi', '8954342342', '987543254', 'anandogale789@gmail.com', '2', '22', '2', '2', '44', '3', '3', 'mysore,road,byatarayanpura,benglore-560026\r\nnear byataaraynpura police station', 'mysore,road,byatarayanpura,benglore-560026\r\nnear byataaraynpura police station', '2016-04-28 03:22:52', '2016-04-28 04:42:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_drawing_mapping`
--

CREATE TABLE IF NOT EXISTS `customer_drawing_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `drawing_master_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_manual_po`
--

CREATE TABLE IF NOT EXISTS `customer_manual_po` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_po_id` int(11) NOT NULL,
  `drawing_no` int(11) NOT NULL,
  `revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_no` double NOT NULL,
  `po_price` double NOT NULL,
  `quantity` double NOT NULL,
  `delivery_date` date NOT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_po`
--

CREATE TABLE IF NOT EXISTS `customer_po` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT '1=manual, 2=excel',
  `total_inward` double NOT NULL,
  `total_confirmed` double NOT NULL,
  `close_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_po_items`
--

CREATE TABLE IF NOT EXISTS `customer_po_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_po_id` int(11) NOT NULL,
  `drawing_master_revision_id` int(11) NOT NULL,
  `po_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_price` double NOT NULL,
  `quantity` double NOT NULL,
  `delivery_date` date NOT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CEO', 'test', 0, 0, '2016-04-28 02:41:57', '2016-04-28 06:41:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drawing_attachments`
--

CREATE TABLE IF NOT EXISTS `drawing_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `drawing_master_id` int(11) NOT NULL,
  `revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachments` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_master`
--

CREATE TABLE IF NOT EXISTS `drawing_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `drawing_type_id` int(11) NOT NULL COMMENT '1=Component, 2=SA',
  `parameter_type_id` int(11) NOT NULL,
  `drawing_no` int(11) NOT NULL,
  `revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Latest Revision',
  `drawing_no_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drawing_master_revision`
--

CREATE TABLE IF NOT EXISTS `drawing_master_revision` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `drawing_master_id` int(11) NOT NULL,
  `revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parameter_type_mapping_id` int(11) NOT NULL,
  `parameter_type_value` double NOT NULL,
  `min_tolerance` double NOT NULL,
  `max_tolerance` double NOT NULL,
  `pt_min` double NOT NULL,
  `pt_max` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employment_type_id` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blood_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_address` text COLLATE utf8_unicode_ci,
  `permanent_address` text COLLATE utf8_unicode_ci,
  `ot_applicable` tinyint(1) NOT NULL,
  `ot_times` double(8,2) NOT NULL,
  `esi_applicable` tinyint(1) NOT NULL,
  `ss_revision_id` tinyint(1) NOT NULL,
  `esi_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pan_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode_of_payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appraisal_interval` double(8,2) NOT NULL,
  `alert_frequency` double(8,2) NOT NULL,
  `to_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employee_id`, `first_name`, `middle_name`, `last_name`, `designation`, `department`, `employment_type_id`, `dob`, `doj`, `contact_number`, `emergency_contact_number`, `email`, `blood_group`, `contact_address`, `permanent_address`, `ot_applicable`, `ot_times`, `esi_applicable`, `ss_revision_id`, `esi_no`, `pf_no`, `bank_name`, `account_number`, `ifsc_code`, `pan_no`, `mode_of_payment_id`, `appraisal_interval`, `alert_frequency`, `to_number`, `to_email`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '001', 'Nandan', 'T', 'R', 'CEO', 'IT', 1, '1998-06-11', '2015-01-09', '7878989898', '9898988766', 'nandan123@gamil.com', 'O+', '#21/7, 1st main,5th cross,byataranpura,mysore road,Bangalore,560026', 'mysore,road,byatarayanpura,benglore-560026\r\nnear byataaraynpura police station', 0, 0.00, 0, 0, '987656', '876544', 'HDFC', '676434534', '098', '123213', '1', 1.00, 2.00, '345342534', 'anandogale789@gmail.com', 0, 0, '2016-04-27 04:10:05', '2016-04-27 04:10:05', NULL),
(2, '002', 'ShriRam', 'D', 'Bhat', 'Employee', 'Doozie', 2, '1995-04-09', '2016-02-21', '9876554322', '2342234244', 'sriram@doozisoft.com', 'AB', 'mysore,road,byatarayanpura,benglore-560026\r\nnear byataaraynpura police station', '#21/7, 1st main,5th cross,byataranpura,mysore road,Bangalore,560026', 0, 0.00, 0, 0, '1234567', '123456', 'HDFC', '212313321', '21321', '23123131', '1', 2.00, 2.00, '21231232', 'anandogale789@gmail.com', 0, 0, '2016-04-28 02:55:40', '2016-04-28 03:15:19', '2016-04-28 03:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `employee_histories`
--

CREATE TABLE IF NOT EXISTS `employee_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salary_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `designation_id` int(11) NOT NULL,
  `employment_type_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `esi_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esi_applicable` tinyint(4) NOT NULL,
  `pf_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `con_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blood` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `con_adrs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pem_adrs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pan_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mode_of_payment_id` int(11) NOT NULL,
  `active_flag` int(11) NOT NULL,
  `ss_revision_id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ot_applicable` int(11) NOT NULL,
  `ot_times` double(8,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_leaves`
--

CREATE TABLE IF NOT EXISTS `employee_leaves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_duration` double NOT NULL,
  `leave_date` date NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_leave_eligibility`
--

CREATE TABLE IF NOT EXISTS `employee_leave_eligibility` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `days_alloted` int(11) NOT NULL,
  `year` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_salaries`
--

CREATE TABLE IF NOT EXISTS `employee_salaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `salary_id` int(11) NOT NULL,
  `worked_days` double(8,2) NOT NULL,
  `ot_hours` double(8,2) NOT NULL DEFAULT '0.00',
  `incentive` double(8,2) NOT NULL DEFAULT '0.00',
  `deduction` double(8,2) NOT NULL,
  `revision_id` int(11) DEFAULT NULL,
  `esi_applicable` tinyint(4) NOT NULL DEFAULT '0',
  `manage_flag` int(11) NOT NULL DEFAULT '0',
  `no_of_print` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary_structure`
--

CREATE TABLE IF NOT EXISTS `employee_salary_structure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salary_component_type_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `salary_component_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `revision_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employment_type`
--

CREATE TABLE IF NOT EXISTS `employment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employment_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `used_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employment_type_employment_type_name_unique` (`employment_type_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `employment_type`
--

INSERT INTO `employment_type` (`id`, `employment_type_name`, `description`, `used_flag`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Employee', 'Employee', 0, 1, 0, '2016-04-27 03:57:49', '2016-04-28 06:38:17', '2016-04-28 06:38:17'),
(2, 'HR', 'HR', 0, 1, 0, '2016-04-27 04:07:04', '2016-04-27 04:07:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `excise_tax_master`
--

CREATE TABLE IF NOT EXISTS `excise_tax_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excise_duty` double NOT NULL,
  `surcharge` double NOT NULL,
  `higher_education_cess` double NOT NULL,
  `education_cess` double NOT NULL,
  `additional_charge` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `excise_tax_master`
--

INSERT INTO `excise_tax_master` (`id`, `tax_type`, `excise_duty`, `surcharge`, `higher_education_cess`, `education_cess`, `additional_charge`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'service', 12, 13, 14, 15, 16, '', 1, 1, '2016-04-27 06:28:52', '2016-04-27 06:30:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `freight_master`
--

CREATE TABLE IF NOT EXISTS `freight_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `freight_master`
--

INSERT INTO `freight_master` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Exworks', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'For Destination', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'For Works', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'FOB', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'CIF', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'Other', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE IF NOT EXISTS `holiday` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restricted` int(11) NOT NULL,
  `optional` int(11) NOT NULL,
  `available_optional` int(11) NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `holiday_list`
--

CREATE TABLE IF NOT EXISTS `holiday_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `holiday_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=Restricted,2=Optional',
  `particular_date` date NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE IF NOT EXISTS `item_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minimum_stock_quantity` double NOT NULL,
  `uom_id` int(11) NOT NULL,
  `base_price` double NOT NULL,
  `purchase_price` double NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`id`, `item_name`, `item_description`, `minimum_stock_quantity`, `uom_id`, `base_price`, `purchase_price`, `updated_by`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gold', 'High quality', 2, 2, 2000, 1000, '1', '1', '2016-04-28 05:32:16', '2016-04-28 05:48:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_type`
--

CREATE TABLE IF NOT EXISTS `leave_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leave_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `select_leave_in` int(11) NOT NULL COMMENT '0=Year, 1=Month',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `leave_type`
--

INSERT INTO `leave_type` (`id`, `leave_type`, `description`, `select_leave_in`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Casual', 'test', 0, 1, 0, '2016-04-27 23:10:39', '2016-04-27 23:10:39', NULL),
(2, 'sick', 'test2', 1, 1, 1, '2016-04-27 23:11:00', '2016-04-27 23:31:42', '2016-04-27 23:31:42'),
(3, 'others', '', 1, 1, 0, '2016-04-27 23:19:23', '2016-04-27 23:19:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_03_28_044128_create_users_table', 1),
('2016_03_28_065049_create_salary_component_type', 1),
('2016_03_28_065741_create_salary_component_table', 1),
('2016_03_31_041035_create_designations_table', 1),
('2016_04_04_045519_create_employee_table', 1),
('2016_04_07_104826_create_employment_type_table', 1),
('2016_04_07_112329_create_holiday_table', 1),
('2016_04_07_112725_create_optional_holiday_table', 1),
('2016_04_07_112924_create_restricted_holiday_table', 1),
('2016_04_07_113104_create_year_date_table', 1),
('2016_04_11_051607_create_transport_table', 1),
('2016_04_12_094524_create_customer_table', 1),
('2016_04_13_102500_create_leave_type_master_table', 1),
('2016_04_13_105347_create_region_table', 1),
('2016_04_14_050045_create_process', 1),
('2016_04_14_051907_create_commercial_tax_table', 1),
('2016_04_14_052351_create_type_master_table', 1),
('2016_04_14_052752_create_parameter_type_mapping_table', 1),
('2016_04_14_054428_create_parameter_type_table', 1),
('2016_04_14_054449_create_parameter_master_table', 1),
('2016_04_14_094259_create_uom_table', 1),
('2016_04_14_100250_create_excise_tax_table', 1),
('2016_04_14_114910_create_drawing_master_table', 1),
('2016_04_14_115920_create_drawing_master_parameter_table', 1),
('2016_04_15_041418_create_item_master', 1),
('2016_04_15_042755_create_customer_drawing_mapping_table', 1),
('2016_04_15_050359_create_sales_tax_table', 1),
('2016_04_15_051458_create_sales_tax_form_table', 1),
('2016_04_15_090033_create_supplier_table', 1),
('2016_04_15_121648_create_customer_po_table', 1),
('2016_04_15_121702_create_customer_po_items_table', 1),
('2016_04_16_062146_create_customer_manual_po_table', 1),
('2016_04_16_093841_create_tax_mapping_table', 1),
('2016_04_18_044925_create_cart_master', 1),
('2016_04_18_050409_create_orders_table', 1),
('2016_04_18_064303_create_freight_master_table', 1),
('2016_04_18_112106_create_po_table', 1),
('2016_04_18_112626_create_until_table', 1),
('2016_04_19_043544_create_mrn_table', 1),
('2016_04_19_044057_create_mrn_items_table', 1),
('2016_04_19_045120_create_salaries_table', 1),
('2016_04_19_045821_create_employee_salary_structure_table', 1),
('2016_04_19_045919_create_employee_leaves_table', 1),
('2016_04_19_104814_create_employee_leave_eligibility_table', 1),
('2016_04_20_040050_create_employee_salaries_table', 1),
('2016_04_20_072734_create_raw_material_stock_table', 1),
('2016_04_21_122256_create_company_master', 1),
('2016_04_22_061530_create_drawing_attachments_table', 1),
('2016_04_25_102905_create_salary_component_histories_table', 1),
('2016_04_25_114602_create_employee_histories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mrn`
--

CREATE TABLE IF NOT EXISTS `mrn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrn_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mrn_cost` double NOT NULL,
  `special_instructions` text COLLATE utf8_unicode_ci NOT NULL,
  `close_flag` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrn_items`
--

CREATE TABLE IF NOT EXISTS `mrn_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrn_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `received_qty` double NOT NULL,
  `mrn_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accepted_qty` double NOT NULL,
  `rejected_qty` double NOT NULL,
  `reworks_qty` double NOT NULL,
  `replacement_qty` double NOT NULL,
  `manage_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manage_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `optional_holiday`
--

CREATE TABLE IF NOT EXISTS `optional_holiday` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `holiday_uid` int(11) NOT NULL,
  `optional_date` date NOT NULL,
  `optional_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `item_master_id` int(11) NOT NULL,
  `order_quantity` double NOT NULL,
  `po_id` int(11) NOT NULL,
  `price` double NOT NULL COMMENT 'po price',
  `purchase_price` double NOT NULL COMMENT 'item purchase price',
  `expected_date` date NOT NULL,
  `received_quantity` double NOT NULL,
  `po_status` int(11) NOT NULL,
  `close_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `parameter_master`
--

CREATE TABLE IF NOT EXISTS `parameter_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tolerance_type` int(11) NOT NULL COMMENT '1=Same, 2=Different',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `parameter_master`
--

INSERT INTO `parameter_master` (`id`, `name`, `description`, `tolerance_type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'parameter', 'test1', 2, 1, 1, '2016-04-27 04:23:32', '2016-04-27 04:24:05', NULL),
(2, 'parameter1', 'test2', 1, 1, 1, '2016-04-27 04:40:53', '2016-04-27 04:53:05', '2016-04-27 04:53:05'),
(3, 'parameter', 'test', 1, 1, 0, '2016-04-27 05:28:54', '2016-04-27 05:28:54', NULL),
(4, 'parameter', '', 1, 1, 0, '2016-04-27 05:29:17', '2016-04-27 05:29:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_type`
--

CREATE TABLE IF NOT EXISTS `parameter_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_master_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `parameter_type`
--

INSERT INTO `parameter_type` (`id`, `type_master_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 0, '2016-04-27 05:36:26', '2016-04-28 06:37:46', '2016-04-28 06:37:46'),
(2, 3, 1, 0, '2016-04-27 05:37:25', '2016-04-27 05:37:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_type_mapping`
--

CREATE TABLE IF NOT EXISTS `parameter_type_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parameter_type_id` int(11) NOT NULL,
  `parameter_master_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `parameter_type_mapping`
--

INSERT INTO `parameter_type_mapping` (`id`, `parameter_type_id`, `parameter_master_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE IF NOT EXISTS `po` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_number` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `commercial_tax_id` int(11) NOT NULL,
  `sales_tax_id` int(11) NOT NULL,
  `excise_tax_id` int(11) NOT NULL,
  `freight_id` int(11) NOT NULL,
  `transport_master_id` int(11) NOT NULL,
  `po_credit_days` int(11) NOT NULL,
  `po_payment_terms` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_po_amount` double NOT NULL,
  `print_status` int(11) NOT NULL,
  `close_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `process_master`
--

CREATE TABLE IF NOT EXISTS `process_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `process_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `process_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `process_master`
--

INSERT INTO `process_master` (`id`, `process_name`, `process_description`, `updated_by`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'mixe', 'test3', '1', '1', '2016-04-28 05:09:15', '2016-04-28 05:10:38', NULL),
(2, 'Grinder', 'test1', '', '1', '2016-04-28 05:09:34', '2016-04-28 05:09:34', NULL),
(3, 'cutting', 'test2', '', '1', '2016-04-28 05:10:00', '2016-04-28 05:10:00', NULL),
(4, 'mixer1', '', '1', '1', '2016-04-28 05:10:53', '2016-04-28 05:22:30', '2016-04-28 05:22:30');

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_stock`
--

CREATE TABLE IF NOT EXISTS `raw_material_stock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_master_id` int(11) NOT NULL,
  `stock_quantity` double NOT NULL,
  `block_quantity` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `region_master`
--

CREATE TABLE IF NOT EXISTS `region_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_type` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `region_master`
--

INSERT INTO `region_master` (`id`, `region_type`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'North', 'test3', '2016-04-28 04:48:23', '2016-04-28 06:35:18', NULL),
(2, 'south', 'test', '2016-04-28 04:58:39', '2016-04-28 04:58:54', '2016-04-28 04:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `restricted_holiday`
--

CREATE TABLE IF NOT EXISTS `restricted_holiday` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `holiday_uid` int(11) NOT NULL,
  `restricted_date` date NOT NULL,
  `restricted_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE IF NOT EXISTS `salaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `nwd` double(8,2) NOT NULL,
  `earning_cost_type_id` double(8,2) NOT NULL,
  `earning_amount` double(8,2) NOT NULL,
  `earning_depend_on` int(11) DEFAULT NULL,
  `earning_percent` double(8,2) NOT NULL,
  `deduction_cost_type_id` double(8,2) NOT NULL,
  `deduction_amount` double(8,2) NOT NULL,
  `deduction_depend_on` int(11) DEFAULT NULL,
  `deduction_percent` double(8,2) NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_emply_generated` int(11) NOT NULL,
  `close_flag` int(11) NOT NULL DEFAULT '0',
  `confirm_flag` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `salary_components`
--

CREATE TABLE IF NOT EXISTS `salary_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salary_component_type_id` int(11) NOT NULL,
  `cost_type_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `depend_on` int(11) DEFAULT NULL,
  `percentage` double(8,2) DEFAULT NULL,
  `percentage_component` double(15,8) NOT NULL,
  `employeer_contribution_flag` int(11) NOT NULL,
  `contribution_depend_on` int(11) NOT NULL,
  `contribution_percentage` double(15,8) NOT NULL,
  `contribution_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `salary_components`
--

INSERT INTO `salary_components` (`id`, `salary_component_type_id`, `cost_type_id`, `title`, `depend_on`, `percentage`, `percentage_component`, `employeer_contribution_flag`, `contribution_depend_on`, `contribution_percentage`, `contribution_flag`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 'BASIC & DA', -1, 14.00, 0.00000000, 1, -1, 12.00000000, 0, 0, 0, '2016-04-28 00:48:00', '2016-04-28 00:54:31', NULL),
(2, 2, 2, 'BASIC HRA', 1, 34.00, 0.00000000, 1, 1, 74.00000000, 0, 0, 0, '2016-04-28 00:48:55', '2016-04-28 00:48:55', NULL),
(3, 2, 1, 'BASIC', 2, 23.00, 0.00000000, 1, 2, 89.00000000, 0, 0, 0, '2016-04-28 00:49:31', '2016-04-28 00:58:46', NULL),
(5, 2, 1, 'Basic HR DA', NULL, NULL, 0.00000000, 1, 2, 122.00000000, 0, 0, 0, '2016-04-28 01:21:54', '2016-04-28 01:21:54', NULL),
(6, 1, 2, 'HRA DA', 7, 24.00, 0.00000000, 1, 4, 33.00000000, 0, 0, 0, '2016-04-28 01:28:42', '2016-04-28 01:46:17', NULL),
(7, 2, 2, 'DA HRA', 2, 12.00, 0.00000000, 1, 5, 12.00000000, 0, 0, 0, '2016-04-28 01:33:46', '2016-04-28 01:33:46', NULL),
(8, 1, 2, 'HRA', 2, 10.00, 0.00000000, 1, 6, 12.00000000, 0, 0, 0, '2016-04-28 01:49:18', '2016-04-28 01:49:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `salary_component_histories`
--

CREATE TABLE IF NOT EXISTS `salary_component_histories` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `salary_id` int(11) NOT NULL,
  `salary_component_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost_type` int(11) NOT NULL,
  `depend_on` int(11) DEFAULT NULL,
  `percentage_component` double NOT NULL,
  `contribution_depend_on` int(11) NOT NULL,
  `contribution_percentage` double NOT NULL,
  `contribution_flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `salary_component_types`
--

CREATE TABLE IF NOT EXISTS `salary_component_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salary_component_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `salary_component_types`
--

INSERT INTO `salary_component_types` (`id`, `salary_component_type_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Earning', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Deduction', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_tax_form`
--

CREATE TABLE IF NOT EXISTS `sales_tax_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sales_tax_form`
--

INSERT INTO `sales_tax_form` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'C', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'F', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'H', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'I', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'NA', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_tax_master`
--

CREATE TABLE IF NOT EXISTS `sales_tax_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cst` double NOT NULL,
  `st` double NOT NULL,
  `sales_tax_form_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sales_tax_master`
--

INSERT INTO `sales_tax_master` (`id`, `tax_type`, `cst`, `st`, `sales_tax_form_id`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'service', 122, 12, 3, 'test2', 1, 0, '2016-04-27 07:25:21', '2016-04-27 07:29:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_master`
--

CREATE TABLE IF NOT EXISTS `supplier_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_contact_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) NOT NULL,
  `web_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ecc_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_id` int(11) NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit_limit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_terms` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit_days` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remark_for_po` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` text COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` text COLLATE utf8_unicode_ci NOT NULL,
  `supplier_address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `supplier_master`
--

INSERT INTO `supplier_master` (`id`, `supplier_name`, `email`, `supplier_contact_no`, `contact_person_name`, `contact_person_no`, `additional_email`, `region_id`, `web_address`, `ecc_no`, `currency_id`, `bank_name`, `account_no`, `credit_limit`, `payment_terms`, `credit_days`, `remark_for_po`, `created_by`, `updated_by`, `supplier_address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Anand', 'anandogale9@gmail.com', '98644543344', 'Ashish', '7456534534', 'trnandan17@gmail.com', 1, '#21/7, 1st main,5th cross,byataranpura,mysore road,Bangalore,560026', '654645', 0, 'City Bank', '5646456', '564554', '556456', '555', 'test', '1', '1', '#21/7, 1st main,5th cross,byataranpura,mysore road,Bangalore,560026', '2016-04-28 04:49:36', '2016-04-28 06:42:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tax_mapping`
--

CREATE TABLE IF NOT EXISTS `tax_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `commercial_tax_id` int(11) NOT NULL,
  `excise_tax_id` int(11) NOT NULL,
  `sales_tax_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tax_mapping`
--

INSERT INTO `tax_mapping` (`id`, `supplier_id`, `customer_id`, `commercial_tax_id`, `excise_tax_id`, `sales_tax_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 0, 1, 1, 1, 1, '2016-04-28 04:41:56', '2016-04-28 04:41:56', NULL),
(5, 1, 0, 1, 1, 0, '2016-04-28 04:57:47', '2016-04-28 04:57:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transport_master`
--

CREATE TABLE IF NOT EXISTS `transport_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transport_type` text COLLATE utf8_unicode_ci NOT NULL,
  `delivery_type` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transport_master`
--

INSERT INTO `transport_master` (`id`, `transport_type`, `delivery_type`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'courier', 'Post', 'test', '2016-04-28 05:51:44', '2016-04-28 05:56:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_master`
--

CREATE TABLE IF NOT EXISTS `type_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tariff_classification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `type_master`
--

INSERT INTO `type_master` (`id`, `type_name`, `description`, `tariff_classification`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', 'test1', 'Test2', 1, 1, '2016-04-27 04:25:22', '2016-04-27 05:23:35', '2016-04-27 05:23:35'),
(2, 'parameter2', 'test', 'Test', 1, 0, '2016-04-27 05:11:50', '2016-04-27 05:23:10', NULL),
(3, 'test2', 'test', 'Test', 1, 0, '2016-04-27 05:12:34', '2016-04-27 05:23:16', '2016-04-27 05:23:16'),
(4, 'test', 'test', 'Test', 1, 0, '2016-04-27 05:13:09', '2016-04-27 05:22:59', NULL),
(5, 'test2', 'test3', 'Test2', 1, 1, '2016-04-27 05:13:44', '2016-04-27 05:20:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `uom_master`
--

CREATE TABLE IF NOT EXISTS `uom_master` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uom_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `uom_master`
--

INSERT INTO `uom_master` (`id`, `uom_name`, `uom_description`, `updated_by`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Length', 'in meter', '', '1', '2016-04-28 05:25:56', '2016-04-28 05:25:56', NULL),
(2, 'Height', 'in CM', '1', '1', '2016-04-28 05:26:21', '2016-04-28 05:29:25', NULL),
(3, 'Width', 'in meter', '', '1', '2016-04-28 05:30:56', '2016-04-28 05:31:03', '2016-04-28 05:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '', '$2y$10$87/FvIHWglP4oqpFLRMlRui9vxzdBzQgdJ1Nc62ZwWZsIYhLA8oBC', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Nandan', 'nandan123@gamil.com', '$2y$10$7SWJdqTddJDfgosZ34LlveKbUXU0HW8WgZAyE8u56tkZ5g2z3P.de', NULL, '2016-04-27 04:10:06', '2016-04-27 04:10:06', NULL),
(3, 'ShriRam', 'sriram@doozisoft.com', '$2y$10$bqmr9PP.nc0vF1k/CShbw.P3PNJ8UpeR46Q.57OLQK5Q..BrLeXHm', NULL, '2016-04-28 02:55:41', '2016-04-28 02:55:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `util`
--

CREATE TABLE IF NOT EXISTS `util` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prop` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `util`
--

INSERT INTO `util` (`id`, `prop`, `year`, `value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'po_number', '2016-17', 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
