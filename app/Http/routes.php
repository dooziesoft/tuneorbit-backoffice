<?php
//Common Routes
Route::when('*', ['middleware' => 'csrf'], ['POST', 'PUT', 'PATCH', 'DELETE']);

Route::get('/',['as' => 'home','uses'=>'tune_orbit_bo\DashboardController@index','middleware'=>'auth']);

//Auth
Route::group(['namespace'=>'Auth'], function () {
  Route::get('login',['as' => 'login','uses'=>'AuthController@showLogin']);
  Route::post('login',['as' => 'postLogin','uses'=>'AuthController@doLogin']);
  Route::get('sign_out',['as' => 'sign_out','uses'=>'AuthController@signOut']);
});

//tune_orbit-bo  Routes
Route::group(['prefix'=>'tune_orbit_bo','namespace'=>'tune_orbit_bo','middleware'=>'auth'],function(){
  //Dashboard
  Route::get('dashboard',['as' => 'dashboard','uses'=>'DashboardController@index']);



  //Service Page
  Route::group(['prefix'=>'service_page','namespace'=>'service_page'],function(){

    //Services
   Route::resource('services','ServicePageController');

   Route::post('/update_new_service_page/{services_id}', ['as' => 'service_page.services.updateServicePage', 'uses' => 'ServicePageController@updateServicePage']);
   Route::get('/get_specific_subservices/{services_id}', ['as' => 'service_page.services.getspecificSubservices', 'uses' => 'ServicePageController@getspecificSubservices']);
   Route::get('/serviceImg_delete/', ['as' => 'service_page.services.deleteSpecificImg', 'uses' => 'ServicePageController@deleteSpecificImg']);
   Route::get('/servicePagePhoto_delete/', ['as' => 'service_page.services.deleteSpecificServicePagePhoto', 'uses' => 'ServicePageController@deleteSpecificServicePagePhoto']);

   Route::resource('view_and_commit','ViewCommitController');
   Route::put('/update_service_page/{services_id}', ['as' => 'service_page.view_and_commit.updateServicePage', 'uses' => 'ViewCommitController@updateServicePage']);

   Route::resource('audit_and_commit','AuditCommitController');
   Route::post('/audit_and_commit/{audit_checkpoint_id}/{user_service_id}', ['as' => 'getspecificServiceAuditRemark', 'uses' => 'AuditCommitController@getspecificRemark']);
   Route::post('/updateOpenHours', ['as' => 'updateOpenHours', 'uses' => 'AuditCommitController@updateOpenHours']);

      Route::resource('manage_services','ManageServicePageController');
    Route::post('/manage_services/filter', ['as' => 'service_page.manage_services.filter', 'uses' => 'ManageServicePageController@filter']);

      Route::post('/update_manage_service_page/{services_id}', ['as' => 'service_page.manage_services.updateServicePage', 'uses' => 'ManageServicePageController@updateServicePage']);

      Route::get('/manage_services/push_pull_service/{services_id}', ['as' => 'service_page.manage_services.push_pull_service', 'uses' => 'ManageServicePageController@push_pull_service']);
 });

  Route::group(['prefix'=>'excel_import','namespace'=>'excel_import'],function(){

    //Services
   Route::resource('import','ExcelImportController');

   Route::post('/uploadExcel/', ['as' => 'tune_orbit_bo.excel_import.import.uploadExcel', 'uses' => 'ExcelImportController@uploadExcel']);

 });

//Masters
  Route::group(['prefix'=>'masters','namespace'=>'masters'],function(){

    //Services
   Route::resource('services','ServicesController');

   Route::post('/upload/', ['as' => 'tune_orbit_bo.masters.services.uploadImg', 'uses' => 'ServicesController@uploadImg']);

   Route::put('/uploadImage/', ['as' => 'tune_orbit_bo.masters.services.uploadImgForEdit', 'uses' => 'ServicesController@uploadImgForEdit']);

   Route::get('/delete_image/', ['as' => 'tune_orbit_bo.masters.services.deleteServiceImage', 'uses' => 'ServicesController@deleteServiceImage']);

   Route::get('/services/deactivate/{services_id}', ['as' => 'masters.services.deactivate', 'uses' => 'ServicesController@deactivate']);



    //Sub Services
   Route::resource('sub_services','SubServicesController');
   
   Route::get('/sub_services/deactivate/{sub_services_id}', ['as' => 'masters.sub_services.deactivate', 'uses' => 'SubServicesController@deactivate']); 

   Route::get('/sub_services/manage_gallery/{sub_services_id}', ['as' => 'masters.sub_services.manage_gallery', 'uses' => 'SubServicesController@manage_gallery']); 


   Route::post('/sub_services/{sub_services_id}', ['as' => 'sub_services.updateServiceImages', 'uses' => 'SubServicesController@updateServiceImages']);

   Route::get('/serviceImg_delete/', ['as' => 'masters.sub_services.deleteSpecificServiceImg', 'uses' => 'SubServicesController@deleteSpecificServiceImg']);

   Route::post('/upload_serviceImg/', ['as' => 'tune_orbit_bo.masters.sub_services.uploadserviceImg', 'uses' => 'SubServicesController@uploadserviceImg']);

   Route::post('/edit_upload_serviceImg/', ['as' => 'tune_orbit_bo.masters.sub_services.editUploadserviceImg', 'uses' => 'SubServicesController@editUploadserviceImg']);




 });



});

function getDateDifferenceInDays($date1,$date2){
  $date1=new Date($date1);
  $date2=new Date($date2);
  $diff = new Date($date1 - $date2);
  $days = $diff/1000/60/60/24;
  $x=($days.toString()).split('.');
  return $x[0];
}

function getFormatedDate($date){
  $formated_date=date("d F Y",strtotime($date));
  return $formated_date;
}


function getServiceUploadedTmpPath($file){
  $file_path='';
  if (App::environment('local')) {
    $file_path='/tune_orbit_bo/image/tmp/'.$file;
  } else{
    $file_path=ENV('DOMAIN_PATH').'/backoffice/tune_orbit_bo/image/tmp/'.$file;
  }
  return $file_path;
}

function getServicesDestinationPathToStore(){
  $file_path='';
  if (App::environment('local')) {
    $file_path='../../../dev/tune_orbit_web/app/images/services/';
  } else{
    $file_path=ENV('DOMAIN_PATH').'../images/services/';
  }
  return $file_path;
}


function getServicesUploadedPath($file){
  $file_path='';
  if (App::environment('local')) {
    $file_path='../../../dev/tune_orbit_web/app'.$file;
  } else{
    $file_path=ENV('DOMAIN_PATH').$file;
  }
  return $file_path;
}


function getSubServiceUploadedTmpPath($file){
  $file_path='';
  if (App::environment('local')) {
    $file_path='/tune_orbit_bo/image/tmp/'.$file;
  } else{
    $file_path=ENV('DOMAIN_PATH').'/backoffice/tune_orbit_bo/image/tmp/'.$file;
  }
  return $file_path;
}


function getServicesPageBannerPath(){
  $file_path='';
  if (App::environment('local')) {
    $file_path='/tune_orbit_bo/image/service_page_images/banner';
  } else{
    $file_path=ENV('DOMAIN_PATH');
  }
  return $file_path;
}

function getServicesPageThumbnailPath(){
  $file_path='';
  if (App::environment('local')) {
    $file_path='/tune_orbit_bo/image/service_page_images/thumbnail';
  } else{
    $file_path=ENV('DOMAIN_PATH');
  }
  return $file_path;
}

function getServicesPagePhotoPath(){
  $file_path='';
  if (App::environment('local')) {
    $file_path='/tune_orbit_bo/image/service_page_images';
  } else{
    $file_path=ENV('DOMAIN_PATH').'/tune_orbit_bo/image/services/'.$file;
  }
  return $file_path;
}



