<?php

namespace App\Http\Requests\tune_orbit_bo;

use App\Http\Requests\Request;
use App\Models\Masters\Client;


class ServicePageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        if ($this->method() == 'PUT')
        {
            // Update operation, exclude the record with id from the validation:
            $services_rule = 'required|unique:services,name,' . $this->get('id');
        }
        else
        {
            // Create operation. There is no id yet.
           $services_rule = 'required|unique:services,name';
        }
        
    
        return [
            'name'=>$services_rule,
            'thumbnail_path'=>'required',
            
        ];
    }
}
