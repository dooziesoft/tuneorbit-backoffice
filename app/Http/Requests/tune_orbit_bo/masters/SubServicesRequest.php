<?php

namespace App\Http\Requests\tune_orbit_bo\masters;

use App\Http\Requests\Request;
use App\Models\masters\MasterSubServices;


class SubServicesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        if ($this->method() == 'PUT')
        {
            // Update operation, exclude the record with id from the validation:
            $subservices_rule = 'required|unique:sub_services,name,' . $this->get('id');
        }
        else
        {
            // Create operation. There is no id yet.
           $subservices_rule = 'required|unique:sub_services,name';
        }
        
    
        return [
            'name'=>$subservices_rule,
            'service_id'=>'required',
            
        ];
    }
}
