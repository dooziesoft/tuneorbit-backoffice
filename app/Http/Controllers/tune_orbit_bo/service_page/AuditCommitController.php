<?php

namespace App\Http\Controllers\tune_orbit_bo\service_page;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\tune_orbit_bo\ServicePageRequest;
use App\Models\masters\MasterServices;
use App\Models\masters\MasterSubServices;
use App\Models\UserService;
use App\Models\ServiceAudit;
use App\Models\masters\MasterServiceImages;
use App\Models\masters\AuditCheckpoint;

use DB;
use Redirect;
use Auth;
use Input;


class AuditCommitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      
      $service_pages=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.user_id',0)->where('us.commit_flag',1)->where('us.audit_flag',0)->select('us.*','ss.name as sub_service_name','s.name as service_name')->orderBy('s.name','ASC')->get();
      return view('tune_orbit_bo.audit_and_commit.index')->with('service_pages',$service_pages);
    }

    public function create()
    {   
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
         // dd(json_decode($request->update_array));
      $service_aduit=new ServiceAudit;
      $service_aduit->storeServiceAudit(json_decode($request->checkpoint_array),$request->user_service_id);
      
     $servicesArray=json_decode($request->update_array);

         
      foreach($servicesArray as $k =>$value) {
        foreach($value as $k1 =>$value1) {
        UserService::where('id',$request->user_service_id)->update([$k1=>$value1]);
        }
      }

      UserService::where('id',$request->user_service_id)->update(['audit_flag'=>1]);
     
      return redirect()->route('tune_orbit_bo.service_page.audit_and_commit.index')->with('message','Successfully Added')->with('er_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // dd($id);
      $checkpoints=AuditCheckpoint::all();
      $mandatory_checkpoints=AuditCheckpoint::select()->where('mandatory_flag',1)->get();
        $UserService=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.id',$id)->select('us.*','ss.name as sub_service_name','s.name as service_name','s.id as service_id')->first();

// dd($UserService);
      return view('tune_orbit_bo.audit_and_commit.create')
              ->with('mandatory_checkpoints',count($mandatory_checkpoints))
              ->with('checkpoints',$checkpoints)->with('user_service_id',$id)->with('UserService',$UserService);
   }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }
    public function deactivate($id)
    {
    }

    public function updateOpenHours(Request $request){

      // dd($request->all());
      $service=UserService::where('id', '=', $request->user_service_id)->first();

        if(isset($request->open_24x7)){
        $service->open_24x7=1;
        $service->working_days='';
        $service->from_time='';
        $service->to_time='';
      }else{
        $service->open_24x7=0;

       $working_day_array=array();
       $from_time_array=array();
       $to_time_array=array();
       $wd=0;
       if(isset($request->mon)){
        $working_day_array[$wd]='{"mon":true,';
        if(isset($request->mon_open_24_hours)){
          $from_time_array[$wd]='{"mon":open_24_hours,';
          $to_time_array[$wd]='{"mon":open_24_hours,';
        }else{
            $from_time_array[$wd]='{"mon":'.$request->mon_from_time.',';
            $to_time_array[$wd]='{"mon":'.$request->mon_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='{"mon":false,';
        $from_time_array[$wd]='{"mon":false,';
        $to_time_array[$wd]='{"mon":false,';
        $wd++;
      }
      if(isset($request->tue)){
        $working_day_array[$wd]='"tue":true,';
        if(isset($request->tue_open_24_hours)){
          $from_time_array[$wd]='"tue":open_24_hours,';
          $to_time_array[$wd]='"tue":open_24_hours,';
        }else{
            $from_time_array[$wd]='"tue":'.$request->tue_from_time.',';
            $to_time_array[$wd]='"tue":'.$request->tue_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"tue":false,';
        $from_time_array[$wd]='"tue":false,';
        $to_time_array[$wd]='"tue":false,';
        $wd++;
      }
      if(isset($request->wed)){
        $working_day_array[$wd]='"wed":true,';
        if(isset($request->wed_open_24_hours)){
          $from_time_array[$wd]='"wed":open_24_hours,';
          $to_time_array[$wd]='"wed":open_24_hours,';
        }else{
            $from_time_array[$wd]='"wed":'.$request->wed_from_time.',';
            $to_time_array[$wd]='"wed":'.$request->wed_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"wed":false,';
        $from_time_array[$wd]='"wed":false,';
        $to_time_array[$wd]='"wed":false,';
        $wd++;
      }
      if(isset($request->thu)){
        $working_day_array[$wd]='"thu":true,';
        if(isset($request->thu_open_24_hours)){
          $from_time_array[$wd]='"thu":open_24_hours,';
          $to_time_array[$wd]='"thu":open_24_hours,';
        }else{
            $from_time_array[$wd]='"thu":'.$request->thu_from_time.',';
            $to_time_array[$wd]='"thu":'.$request->thu_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"thu":false,';
        $from_time_array[$wd]='"thu":false,';
        $to_time_array[$wd]='"thu":false,';
        $wd++;
      }
      if(isset($request->fri)){
        $working_day_array[$wd]='"fri":true,';
        if(isset($request->fri_open_24_hours)){
          $from_time_array[$wd]='"fri":open_24_hours,';
          $to_time_array[$wd]='"fri":open_24_hours,';
        }else{
            $from_time_array[$wd]='"fri":'.$request->fri_from_time.',';
            $to_time_array[$wd]='"fri":'.$request->fri_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"fri":false,';
        $from_time_array[$wd]='"fri":false,';
        $to_time_array[$wd]='"fri":false,';
        $wd++;
      }
      if(isset($request->sat)){
        $working_day_array[$wd]='"sat":true,';
        if(isset($request->sat_open_24_hours)){
          $from_time_array[$wd]='"sat":open_24_hours,';
          $to_time_array[$wd]='"sat":open_24_hours,';
        }else{
            $from_time_array[$wd]='"sat":'.$request->sat_from_time.',';
            $to_time_array[$wd]='"sat":'.$request->sat_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sat":false,';
        $from_time_array[$wd]='"sat":false,';
        $to_time_array[$wd]='"sat":false,';
        $wd++;
      }
      if(isset($request->sun)){
        $working_day_array[$wd]='"sun":true}';
        if(isset($request->sun_open_24_hours)){
          $from_time_array[$wd]='"sun":open_24_hours}';
          $to_time_array[$wd]='"sun":open_24_hours}';
        }else{
            $from_time_array[$wd]='"sun":'.$request->sun_from_time.'}';
            $to_time_array[$wd]='"sun":'.$request->sun_to_time.'}';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sun":false}';
        $from_time_array[$wd]='"sun":false}';
        $to_time_array[$wd]='"sun":false}';
        $wd++;
      }

      $working_days='';
      for($i=0;$i<count($working_day_array);$i++){
        $working_days.=$working_day_array[$i];
      }

      $from_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $from_time.=$from_time_array[$i];
      }

      $to_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $to_time.=$to_time_array[$i];
      }
      $service->working_days=$working_days;
      $service->from_time=$from_time;
      $service->to_time=$to_time;
      $service->update();

      return 1;
}

    }
    public function getspecificRemark(Request $request)
    {
        // dd($request->all());
       $audit_checkpoint_id=$request->audit_checkpoint_id;
       $user_service_id=$request->user_service_id;
           $checkpoints=DB::table('audit_checkpoints AS ac')
         ->join('service_audits AS sc','sc.audit_checkpoint_id','=','ac.id')
         ->where('sc.user_service_id','=',$user_service_id)
          ->where('ac.id','=',$audit_checkpoint_id)
         ->select('ac.*','sc.remarks','sc.id AS service_audit_id')
         ->first();
         return response()->json(array('checkpoints'=>$checkpoints));

    }

}
