<?php

namespace App\Http\Controllers\tune_orbit_bo\service_page;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\tune_orbit_bo\ServicePageRequest;
use App\Models\masters\MasterServices;
use App\Models\masters\MasterSubServices;
use App\Models\UserService;
use App\Models\masters\MasterServiceImages;

use DB;
use Redirect;
use Auth;
use Input;


class ServicePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      
      $service_pages=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.user_id',0)->select('us.*','ss.name as sub_service_name','s.name as service_name')->orderBy('s.name','ASC')->get();
      return view('tune_orbit_bo.service_page.index')->with('service_pages',$service_pages);
    }

    public function getspecificSubservices(Request $request)
   {
    // dd($request->all());
    $sub_services=MasterSubServices::where('service_id',$request->service_id)->select('id','name')->get();
     if($sub_services) {
      return response()->json(array('sub_services'=> $sub_services), 200);
    } else {
      return response()->json(false, 200);
    }
  }

  public function deleteSpecificImg(Request $request)
  {
    // dd($request->all());
    if(isset($request->service_id)){
      if($request->type == 1){
        $str=explode('/',$request->path);
        $path='../server/img/services/banners/'.end($str);
        unlink($path);
        DB::table('user_services')->where('id', '=', $request->service_id)->update(['banner_image' => '']);
      }else if($request->type == 2){
        $str=explode('/',$request->path);
        $path='../server/img/services/thumbnails/'.end($str);
        unlink($path);
        DB::table('user_services')->where('id', '=', $request->service_id)->update(['thumbnail_image' => '']);
      }
    }else{
      if(substr($request->path, 0, 5) == '/back'){
        unlink(ltrim($request->path, '/backoffice'));
      }else{
        unlink('..'.$request->path);
      }
    }
    return response()->json(1);
  }

  public function deleteSpecificServicePagePhoto(Request $request)
  {
    // dd($request->all());
    $path='../server/img/user_service/'.$request->path;
    unlink($path);
    DB::table('user_service_images')->where('id', '=', $request->user_service_img_id)->delete();

    return response()->json(1);
  }

    public function create()
    {

     return view('tune_orbit_bo.service_page.create');

   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());

      $request['user_id']=0;
      $phone_number=$request['country_code'].' '.$request['phone_number'];
      $request['phone_number']=$phone_number;
      $request['audit_flag']=0;
      $request['in_website']=0;
      $request['commit_flag']=0;

      $delete=array();
      if(isset($request->open_24x7)){
        $request['open_24x7']=1;
      }else{
       $working_day_array=array();
       $from_time_array=array();
       $to_time_array=array();
       $wd=0;
       if(isset($request->mon)){
        $working_day_array[$wd]='{"mon":true,';
        if(isset($request->mon_open_24_hours)){
          $from_time_array[$wd]='{"mon":open_24_hours,';
          $to_time_array[$wd]='{"mon":open_24_hours,';
        }else{
            $from_time_array[$wd]='{"mon":'.$request->mon_from_time.',';
            $to_time_array[$wd]='{"mon":'.$request->mon_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='{"mon":false,';
        $from_time_array[$wd]='{"mon":false,';
        $to_time_array[$wd]='{"mon":false,';
        $wd++;
      }
      if(isset($request->tue)){
        $working_day_array[$wd]='"tue":true,';
        if(isset($request->tue_open_24_hours)){
          $from_time_array[$wd]='"tue":open_24_hours,';
          $to_time_array[$wd]='"tue":open_24_hours,';
        }else{
            $from_time_array[$wd]='"tue":'.$request->tue_from_time.',';
            $to_time_array[$wd]='"tue":'.$request->tue_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"tue":false,';
        $from_time_array[$wd]='"tue":false,';
        $to_time_array[$wd]='"tue":false,';
        $wd++;
      }
      if(isset($request->wed)){
        $working_day_array[$wd]='"wed":true,';
        if(isset($request->wed_open_24_hours)){
          $from_time_array[$wd]='"wed":open_24_hours,';
          $to_time_array[$wd]='"wed":open_24_hours,';
        }else{
            $from_time_array[$wd]='"wed":'.$request->wed_from_time.',';
            $to_time_array[$wd]='"wed":'.$request->wed_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"wed":false,';
        $from_time_array[$wd]='"wed":false,';
        $to_time_array[$wd]='"wed":false,';
        $wd++;
      }
      if(isset($request->thu)){
        $working_day_array[$wd]='"thu":true,';
        if(isset($request->thu_open_24_hours)){
          $from_time_array[$wd]='"thu":open_24_hours,';
          $to_time_array[$wd]='"thu":open_24_hours,';
        }else{
            $from_time_array[$wd]='"thu":'.$request->thu_from_time.',';
            $to_time_array[$wd]='"thu":'.$request->thu_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"thu":false,';
        $from_time_array[$wd]='"thu":false,';
        $to_time_array[$wd]='"thu":false,';
        $wd++;
      }
      if(isset($request->fri)){
        $working_day_array[$wd]='"fri":true,';
        if(isset($request->fri_open_24_hours)){
          $from_time_array[$wd]='"fri":open_24_hours,';
          $to_time_array[$wd]='"fri":open_24_hours,';
        }else{
            $from_time_array[$wd]='"fri":'.$request->fri_from_time.',';
            $to_time_array[$wd]='"fri":'.$request->fri_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"fri":false,';
        $from_time_array[$wd]='"fri":false,';
        $to_time_array[$wd]='"fri":false,';
        $wd++;
      }
      if(isset($request->sat)){
        $working_day_array[$wd]='"sat":true,';
        if(isset($request->sat_open_24_hours)){
          $from_time_array[$wd]='"sat":open_24_hours,';
          $to_time_array[$wd]='"sat":open_24_hours,';
        }else{
            $from_time_array[$wd]='"sat":'.$request->sat_from_time.',';
            $to_time_array[$wd]='"sat":'.$request->sat_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sat":false,';
        $from_time_array[$wd]='"sat":false,';
        $to_time_array[$wd]='"sat":false,';
        $wd++;
      }
      if(isset($request->sun)){
        $working_day_array[$wd]='"sun":true}';
        if(isset($request->sun_open_24_hours)){
          $from_time_array[$wd]='"sun":open_24_hours}';
          $to_time_array[$wd]='"sun":open_24_hours}';
        }else{
            $from_time_array[$wd]='"sun":'.$request->sun_from_time.'}';
            $to_time_array[$wd]='"sun":'.$request->sun_to_time.'}';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sun":false}';
        $from_time_array[$wd]='"sun":false}';
        $to_time_array[$wd]='"sun":false}';
        $wd++;
      }

      $working_days='';
      for($i=0;$i<count($working_day_array);$i++){
        $working_days.=$working_day_array[$i];
      }

      $from_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $from_time.=$from_time_array[$i];
      }

      $to_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $to_time.=$to_time_array[$i];
      }

      $request['working_days']=$working_days;
      $request['from_time']=$from_time;
      $request['to_time']=$to_time;

    }
      $user_services=UserService::create($request->all());

      if($request->banner_file_path){
      $source = "tune_orbit_bo/image/tmp/";
      $destination = "../server/img/services/banners/";
      if (copy($source.$request->banner_file_path, $destination.$request->banner_file_path)) {
        $delete[] = $source.$request->banner_file_path;
      }
      $req['banner_image']='https://www.tuneorbit.com/server/img/services/banners/'.$request->banner_file_path;
      $user_services->update($req);
      }else{
        $banner_file_path=MasterServiceImages::where('sub_service_id',$request->sub_service_id)->select('path')->orderBy(DB::raw('RAND()'))->take(1)->first();
        if($banner_file_path){
        $str_array=explode('/',$banner_file_path['path']);
        $ext=explode('.', end($str_array));
        $file_name=round(microtime(true) * 1000) . '.' . end($ext);
        $source = '..'.$banner_file_path['path'];
        $destination = "../server/img/services/banners/";
        copy($source, $destination.$file_name);
        $req['banner_image']='https://www.tuneorbit.com/server/img/services/banners/'.$file_name;
        $user_services->update($req);
        }
      }

      if($request->thumbnail_file_path){
      $source = "tune_orbit_bo/image/tmp/";
      $destination = "../server/img/services/thumbnails/";
      if (copy($source.$request->thumbnail_file_path, $destination.$request->thumbnail_file_path)) {
        $delete[] = $source.$request->thumbnail_file_path;
      }
      $req['thumbnail_image']='https://www.tuneorbit.com/server/img/services/thumbnails/'.$request->thumbnail_file_path;
      $user_services->update($req);
      }else{
        $thumbnail_file_path=MasterServices::where('id',$request->service_id)->select('thumbnail_path')->first();
        if($thumbnail_file_path){
        $str_array=explode('/',$thumbnail_file_path->thumbnail_path);
        $ext=explode('.', end($str_array));
        $file_name=round(microtime(true) * 1000) . '.' . end($ext);
        $source = '..'.$thumbnail_file_path->thumbnail_path;
        $destination = "../server/img/services/thumbnails/";
        copy($source, $destination.$file_name);
        $req['thumbnail_image']='https://www.tuneorbit.com/server/img/services/thumbnails/'.$file_name;
        $user_services->update($req);
       }
      }

      if($request->image_array){
      foreach (json_decode($request->image_array) as $ia) {
        $source = "tune_orbit_bo/image/tmp/";
        // $destination = "tune_orbit_bo/image/service_page_images/";
        $destination = "../server/img/user_service/";
        if (copy($source.$ia, $destination.$ia)) {
          $delete[] = $source.$ia;
        }
        DB::table('user_service_images')->insert(['user_service_id' => $user_services->id,'image_path' => $ia,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]);
      }
    }

      if(count($delete) > 0){
      foreach ( $delete as $file ) {
        unlink( $file );
      }
    }

      return redirect()->route('tune_orbit_bo.service_page.services.index')->with('message','Successfully Added')->with('er_type','success');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $service_page=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.id',$id)->select('us.*','ss.name as sub_service_name','s.name as service_name')->first();
        $service_page_photos=DB::table('user_service_images')->where('user_service_id',$id)->get();
      return view('tune_orbit_bo.service_page.show')->with('service_page',$service_page)->with('service_page_photos',$service_page_photos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // dd($id);
      $service_page=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.id',$id)->select('us.*','ss.name as sub_service_name','s.name as service_name','s.id as service_id')->first();
      $sub_services=MasterSubServices::where('service_id',$service_page->service_id)->lists('name as sub_service_name','id as sub_service_id');
      $service_page_photos=DB::table('user_service_images')->where('user_service_id',$id)->get();
      return view('tune_orbit_bo.service_page.edit')->with('service_page',$service_page)->with('service_page_photos',$service_page_photos)->with('sub_services',$sub_services);
   }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateServicePage(Request $request, $id)
    {
       // dd($request->all());
      $service=UserService::where('id', '=', $id)->first();
      $request['updated_by']=Auth::user()->id;
      $phone_number=$request['country_code'].' '.$request['phone_number'];
      $request['phone_number']=$phone_number;

      $delete=array();
      if(isset($request->open_24x7)){
        $request['open_24x7']=1;
        $request['working_days']='';
        $request['from_time']='';
        $request['to_time']='';
      }else{
        $request['open_24x7']=0;
       $working_day_array=array();
       $from_time_array=array();
       $to_time_array=array();
       $wd=0;
       if(isset($request->mon)){
        $working_day_array[$wd]='{"mon":true,';
        if(isset($request->mon_open_24_hours)){
          $from_time_array[$wd]='{"mon":open_24_hours,';
          $to_time_array[$wd]='{"mon":open_24_hours,';
        }else{
            $from_time_array[$wd]='{"mon":'.$request->mon_from_time.',';
            $to_time_array[$wd]='{"mon":'.$request->mon_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='{"mon":false,';
        $from_time_array[$wd]='{"mon":false,';
        $to_time_array[$wd]='{"mon":false,';
        $wd++;
      }
      if(isset($request->tue)){
        $working_day_array[$wd]='"tue":true,';
        if(isset($request->tue_open_24_hours)){
          $from_time_array[$wd]='"tue":open_24_hours,';
          $to_time_array[$wd]='"tue":open_24_hours,';
        }else{
            $from_time_array[$wd]='"tue":'.$request->tue_from_time.',';
            $to_time_array[$wd]='"tue":'.$request->tue_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"tue":false,';
        $from_time_array[$wd]='"tue":false,';
        $to_time_array[$wd]='"tue":false,';
        $wd++;
      }
      if(isset($request->wed)){
        $working_day_array[$wd]='"wed":true,';
        if(isset($request->wed_open_24_hours)){
          $from_time_array[$wd]='"wed":open_24_hours,';
          $to_time_array[$wd]='"wed":open_24_hours,';
        }else{
            $from_time_array[$wd]='"wed":'.$request->wed_from_time.',';
            $to_time_array[$wd]='"wed":'.$request->wed_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"wed":false,';
        $from_time_array[$wd]='"wed":false,';
        $to_time_array[$wd]='"wed":false,';
        $wd++;
      }
      if(isset($request->thu)){
        $working_day_array[$wd]='"thu":true,';
        if(isset($request->thu_open_24_hours)){
          $from_time_array[$wd]='"thu":open_24_hours,';
          $to_time_array[$wd]='"thu":open_24_hours,';
        }else{
            $from_time_array[$wd]='"thu":'.$request->thu_from_time.',';
            $to_time_array[$wd]='"thu":'.$request->thu_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"thu":false,';
        $from_time_array[$wd]='"thu":false,';
        $to_time_array[$wd]='"thu":false,';
        $wd++;
      }
      if(isset($request->fri)){
        $working_day_array[$wd]='"fri":true,';
        if(isset($request->fri_open_24_hours)){
          $from_time_array[$wd]='"fri":open_24_hours,';
          $to_time_array[$wd]='"fri":open_24_hours,';
        }else{
            $from_time_array[$wd]='"fri":'.$request->fri_from_time.',';
            $to_time_array[$wd]='"fri":'.$request->fri_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"fri":false,';
        $from_time_array[$wd]='"fri":false,';
        $to_time_array[$wd]='"fri":false,';
        $wd++;
      }
      if(isset($request->sat)){
        $working_day_array[$wd]='"sat":true,';
        if(isset($request->sat_open_24_hours)){
          $from_time_array[$wd]='"sat":open_24_hours,';
          $to_time_array[$wd]='"sat":open_24_hours,';
        }else{
            $from_time_array[$wd]='"sat":'.$request->sat_from_time.',';
            $to_time_array[$wd]='"sat":'.$request->sat_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sat":false,';
        $from_time_array[$wd]='"sat":false,';
        $to_time_array[$wd]='"sat":false,';
        $wd++;
      }
      if(isset($request->sun)){
        $working_day_array[$wd]='"sun":true}';
        if(isset($request->sun_open_24_hours)){
          $from_time_array[$wd]='"sun":open_24_hours}';
          $to_time_array[$wd]='"sun":open_24_hours}';
        }else{
            $from_time_array[$wd]='"sun":'.$request->sun_from_time.'}';
            $to_time_array[$wd]='"sun":'.$request->sun_to_time.'}';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sun":false}';
        $from_time_array[$wd]='"sun":false}';
        $to_time_array[$wd]='"sun":false}';
        $wd++;
      }

      $working_days='';
      for($i=0;$i<count($working_day_array);$i++){
        $working_days.=$working_day_array[$i];
      }

      $from_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $from_time.=$from_time_array[$i];
      }

      $to_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $to_time.=$to_time_array[$i];
      }

      $request['working_days']=$working_days;
      $request['from_time']=$from_time;
      $request['to_time']=$to_time;
    }

      $service->update($request->all());
      return redirect()->route('tune_orbit_bo.service_page.services.index')->with('message','Successfully Updated')->with('er_type','success');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deactivate($id)
    {
     $services=MasterServices::where('id', '=', $id)->first();
     if($services){
      $services->delete();
      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully deactivated')->with('er_type','danger');
    }else{
      $services=MasterServices::onlyTrashed()->where('id', '=', $id)->first();
      $services->restore();
      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully activated')->with('er_type','success ');
    }
  }
}
