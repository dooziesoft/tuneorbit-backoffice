<?php

namespace App\Http\Controllers\tune_orbit_bo\service_page;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\tune_orbit_bo\ServicePageRequest;
use App\Models\masters\MasterServices;
use App\Models\masters\MasterSubServices;
use App\Models\UserService;
use App\Models\masters\MasterServiceImages;

use DB;
use Redirect;
use Auth;
use Input;


class ViewCommitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      
      $service_pages=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.user_id',0)->where('us.commit_flag',0)->select('us.*','ss.name as sub_service_name','s.name as service_name')->orderBy('us.id','DESC')->get();
      return view('tune_orbit_bo.view_and_commit.index')->with('service_pages',$service_pages);
    }

    public function getspecificSubservices(Request $request)
   {
    // dd($request->all());
    $sub_services=MasterSubServices::where('service_id',$request->service_id)->select('id','name')->get();
     if($sub_services) {
      return response()->json(array('sub_services'=> $sub_services), 200);
    } else {
      return response()->json(false, 200);
    }
  }

  public function deleteSpecificImg(Request $request)
  {
    // dd($request->all());
    if(isset($request->service_id)){
      if($request->type == 1){
        $str=explode('/',$request->path);
        $path='../server/img/services/banners/'.end($str);
        unlink($path);
        DB::table('user_services')->where('id', '=', $request->service_id)->update(['banner_image' => '']);
      }else if($request->type == 2){
        $str=explode('/',$request->path);
        $path='../server/img/services/thumbnails/'.end($str);
        unlink($path);
        DB::table('user_services')->where('id', '=', $request->service_id)->update(['thumbnail_image' => '']);
      }
    }else{
      if(substr($request->path, 0, 5) == '/back'){
        unlink(ltrim($request->path, '/backoffice'));
      }else{
        unlink('..'.$request->path);
      }
    }
    return response()->json(1);
  }

  public function deleteSpecificServicePagePhoto(Request $request)
  {
    // dd($request->all());
    $path='../server/img/user_service/'.$request->path;
    unlink($path);
    DB::table('user_service_images')->where('id', '=', $request->user_service_img_id)->delete();

    return response()->json(1);
  }

    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // dd($id);
      $service_page=DB::table('user_services as us')->join('sub_services as ss','ss.id','=','us.sub_service_id')->join('services as s','s.id','=','ss.service_id')->where('us.id',$id)->select('us.*','ss.name as sub_service_name','s.name as service_name','s.id as service_id')->first();

      $sub_services=MasterSubServices::where('service_id',$service_page->service_id)->lists('name as sub_service_name','id as sub_service_id');
      $service_page_photos=DB::table('user_service_images')->where('user_service_id',$id)->get();
      return view('tune_orbit_bo.view_and_commit.edit')->with('service_page',$service_page)->with('service_page_photos',$service_page_photos)->with('sub_services',$sub_services);
   }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateServicePage(Request $request, $id)
    {
       // dd($request->all());
      $service=UserService::where('id', '=', $id)->first();
      $request['updated_by']=Auth::user()->id;
      $request['commit_flag']=1;
      $request['in_website']=1;
      $phone_number=$request['country_code'].' '.$request['phone_number'];
      $request['phone_number']=$phone_number;

      $delete=array();
      if(isset($request->open_24x7)){
        $request['open_24x7']=1;
        $request['working_days']='';
        $request['from_time']='';
        $request['to_time']='';
      }else{
        $request['open_24x7']=0;
       $working_day_array=array();
       $from_time_array=array();
       $to_time_array=array();
       $wd=0;
       if(isset($request->mon)){
        $working_day_array[$wd]='{"mon":true,';
        if(isset($request->mon_open_24_hours)){
          $from_time_array[$wd]='{"mon":open_24_hours,';
          $to_time_array[$wd]='{"mon":open_24_hours,';
        }else{
            $from_time_array[$wd]='{"mon":'.$request->mon_from_time.',';
            $to_time_array[$wd]='{"mon":'.$request->mon_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='{"mon":false,';
        $from_time_array[$wd]='{"mon":false,';
        $to_time_array[$wd]='{"mon":false,';
        $wd++;
      }
      if(isset($request->tue)){
        $working_day_array[$wd]='"tue":true,';
        if(isset($request->tue_open_24_hours)){
          $from_time_array[$wd]='"tue":open_24_hours,';
          $to_time_array[$wd]='"tue":open_24_hours,';
        }else{
            $from_time_array[$wd]='"tue":'.$request->tue_from_time.',';
            $to_time_array[$wd]='"tue":'.$request->tue_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"tue":false,';
        $from_time_array[$wd]='"tue":false,';
        $to_time_array[$wd]='"tue":false,';
        $wd++;
      }
      if(isset($request->wed)){
        $working_day_array[$wd]='"wed":true,';
        if(isset($request->wed_open_24_hours)){
          $from_time_array[$wd]='"wed":open_24_hours,';
          $to_time_array[$wd]='"wed":open_24_hours,';
        }else{
            $from_time_array[$wd]='"wed":'.$request->wed_from_time.',';
            $to_time_array[$wd]='"wed":'.$request->wed_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"wed":false,';
        $from_time_array[$wd]='"wed":false,';
        $to_time_array[$wd]='"wed":false,';
        $wd++;
      }
      if(isset($request->thu)){
        $working_day_array[$wd]='"thu":true,';
        if(isset($request->thu_open_24_hours)){
          $from_time_array[$wd]='"thu":open_24_hours,';
          $to_time_array[$wd]='"thu":open_24_hours,';
        }else{
            $from_time_array[$wd]='"thu":'.$request->thu_from_time.',';
            $to_time_array[$wd]='"thu":'.$request->thu_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"thu":false,';
        $from_time_array[$wd]='"thu":false,';
        $to_time_array[$wd]='"thu":false,';
        $wd++;
      }
      if(isset($request->fri)){
        $working_day_array[$wd]='"fri":true,';
        if(isset($request->fri_open_24_hours)){
          $from_time_array[$wd]='"fri":open_24_hours,';
          $to_time_array[$wd]='"fri":open_24_hours,';
        }else{
            $from_time_array[$wd]='"fri":'.$request->fri_from_time.',';
            $to_time_array[$wd]='"fri":'.$request->fri_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"fri":false,';
        $from_time_array[$wd]='"fri":false,';
        $to_time_array[$wd]='"fri":false,';
        $wd++;
      }
      if(isset($request->sat)){
        $working_day_array[$wd]='"sat":true,';
        if(isset($request->sat_open_24_hours)){
          $from_time_array[$wd]='"sat":open_24_hours,';
          $to_time_array[$wd]='"sat":open_24_hours,';
        }else{
            $from_time_array[$wd]='"sat":'.$request->sat_from_time.',';
            $to_time_array[$wd]='"sat":'.$request->sat_to_time.',';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sat":false,';
        $from_time_array[$wd]='"sat":false,';
        $to_time_array[$wd]='"sat":false,';
        $wd++;
      }
      if(isset($request->sun)){
        $working_day_array[$wd]='"sun":true}';
        if(isset($request->sun_open_24_hours)){
          $from_time_array[$wd]='"sun":open_24_hours}';
          $to_time_array[$wd]='"sun":open_24_hours}';
        }else{
            $from_time_array[$wd]='"sun":'.$request->sun_from_time.'}';
            $to_time_array[$wd]='"sun":'.$request->sun_to_time.'}';
        }
        $wd++;
      }else{
        $working_day_array[$wd]='"sun":false}';
        $from_time_array[$wd]='"sun":false}';
        $to_time_array[$wd]='"sun":false}';
        $wd++;
      }

      $working_days='';
      for($i=0;$i<count($working_day_array);$i++){
        $working_days.=$working_day_array[$i];
      }

      $from_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $from_time.=$from_time_array[$i];
      }

      $to_time='';
      for($i=0;$i<count($working_day_array);$i++){
        $to_time.=$to_time_array[$i];
      }

      $request['working_days']=$working_days;
      $request['from_time']=$from_time;
      $request['to_time']=$to_time;
    }

      $service->update($request->all());
      return redirect()->route('tune_orbit_bo.service_page.view_and_commit.index')->with('message','Successfully Updated')->with('er_type','success');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deactivate($id)
    {
   
    }
}
