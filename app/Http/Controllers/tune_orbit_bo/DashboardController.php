<?php

namespace App\Http\Controllers\tune_orbit_bo;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $total_services=DB::table('user_services as us')->whereNull('us.deleted_at')->count();
        $pending_commit=DB::table('user_services as us')->where('us.commit_flag',0)->count();
        $pending_audit=DB::table('user_services as us')->where('us.commit_flag',1)->where('us.audit_flag',0)->count();
        $service_in_website=DB::table('user_services as us')->where('us.in_website',1)->count();
        $unaudited_service_in_website=DB::table('user_services as us')->where('us.in_website',1)->where('us.audit_flag',0)->count();
        // dd($total_services);
        return view('tune_orbit_bo.dashboard.index')->with('total_services',$total_services)->with('pending_commit',$pending_commit)->with('pending_audit',$pending_audit)->with('service_in_website',$service_in_website)->with('unaudited_service_in_website',$unaudited_service_in_website);
    }
}
