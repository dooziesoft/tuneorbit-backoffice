<?php

namespace App\Http\Controllers\tune_orbit_bo\excel_import;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\tune_orbit_bo\masters\ServicesRequest;
use App\Models\masters\MasterServices;

use DB;
use Redirect;
use Auth;
use Input;


class ExcelImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      return view('tune_orbit_bo.excel_import.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

     return view('tune_orbit_bo.masters.services.create');

   }


   public function uploadExcel(Request $request)
   {
// dd($request->all());
     if($request->file('file_name')) {
      //upload an image to the /img/tmp directory and return the filepath.
      $file = Input::file('file_name');
      $tmpFilePath = 'tune_orbit_bo/service_page_excels/';
      $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

      $tmpFileName = time() . '.' . $extension;
      $file = $file->move($tmpFilePath, $tmpFileName);
      $path = $tmpFileName;

      $file_path=$tmpFilePath.$path;

      return response()->json(array('path'=> $file_path), 200);
    } else {
      return response()->json(false, 200);
    }
  }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicesRequest $request)
    {
      // dd($request->all());

      $request['created_by']=Auth::user()->id;
      $request['thumbnail_path']='/images/services/'.$request->temp_file_path;
      $source = "tune_orbit_bo/image/tmp/";
      $destination = getServicesDestinationPathToStore();
      if (copy($source.$request->temp_file_path, $destination.$request->temp_file_path)) {
        unlink($source.$request->temp_file_path);
      }

      MasterServices::create($request->all());

      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully Added')->with('er_type','success');  
    }

    public function deleteServiceImage(Request $request)
    {
      // dd($request->all());
      DB::table('services')->where('id',$request->service_id)->update(['thumbnail_path' => '']);
      unlink('../'.$request->path);
      return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $services=MasterServices::where('id',$id)->first();

      if($services){
        return view('tune_orbit_bo.masters.services.edit')->with('services',$services);
      }else{

       return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Cannot Edit Deactivated services')->with('er_type','danger');
     }
   }


   public function uploadImgForEdit(Request $request)
  {
     //dd($request->all());

    if($request->file('file_name')) {
      //upload an image to the /img/tmp directory and return the filepath.
      $file = Input::file('file_name');
      $tmpFilePath = 'tune_orbit_bo/image/tmp/';
      $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

      $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
      $file = $file->move($tmpFilePath, $tmpFileName);
      $path = $tmpFileName;

      $file_path=getServiceUploadedTmpPath($path);

      return response()->json(array('path'=> $file_path), 200);
    } else {
      return response()->json(false, 200);
    }
  }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // dd($request->all());
      $services=MasterServices::where('id', '=', $id)->first();
      $request['updated_by']=Auth::user()->id;

      $request['thumbnail_path']='/images/services/'.$request->temp_file_path;
      $source = "tune_orbit_bo/image/tmp/";
      $destination = getServicesDestinationPathToStore();
      if (copy($source.$request->temp_file_path, $destination.$request->temp_file_path)) {
        unlink($source.$request->temp_file_path);
      }

      $services->update($request->all());
      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully Updated')->with('er_type','success');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deactivate($id)
    {
     $services=MasterServices::where('id', '=', $id)->first();
     if($services){
      $services->delete();
      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully deactivated')->with('er_type','danger');
    }else{
      $services=MasterServices::onlyTrashed()->where('id', '=', $id)->first();
      $services->restore();
      return redirect()->route('tune_orbit_bo.masters.services.index')->with('message','Successfully activated')->with('er_type','success ');
    }
  }
}
