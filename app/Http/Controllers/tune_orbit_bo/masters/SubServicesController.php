<?php

namespace App\Http\Controllers\tune_orbit_bo\masters;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\tune_orbit_bo\masters\SubServicesRequest;
use App\Models\masters\MasterSubServices;
use App\Models\masters\MasterServiceImages;
use App\Models\UserServiceImages;

use DB;
use Redirect;
use Auth;
use Input;


class SubServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      $sub_services=DB::table('sub_services AS sm')
      ->join('services AS s','sm.service_id','=','s.id')
      ->join('backoffice_users AS u','sm.created_by','=','u.id')
      ->leftjoin('backoffice_users AS u1','sm.updated_by','=','u1.id')
      ->select('sm.*','s.name as services_name','u.name AS created_by','u1.name AS updated_by')
      ->orderBy('sm.name','ASC')
      ->get();

      return view('tune_orbit_bo.masters.sub_services.index')->with('sub_services',$sub_services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
      return view('tune_orbit_bo.masters.sub_services.create');

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubServicesRequest $request)
    {
      //dd($request->all());

      $request['created_by']=Auth::user()->id;

      MasterSubServices::create($request->all());

      return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Successfully Added')->with('er_type','success');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $sub_services=MasterSubServices::where('id',$id)->first();

      if($sub_services){
        return view('tune_orbit_bo.masters.sub_services.edit')->with('sub_services',$sub_services);
      }else{

       return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Cannot Edit Deactivated Sub Services')->with('er_type','danger');
      }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubServicesRequest $request, $id)
    {
      //dd($request->all());
      $services=MasterSubServices::where('id', '=', $id)->first();
      $request['updated_by']=Auth::user()->id;


      $services->update($request->all());
      return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Successfully Updated')->with('er_type','success');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function deactivate($id)
    {
      $services=MasterSubServices::where('id', '=', $id)->first();
      if($services){
        $services->delete();
        return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Successfully deactivated')->with('er_type','danger');
      }else{
        $services=MasterSubServices::onlyTrashed()->where('id', '=', $id)->first();
        $services->restore();
        return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Successfully activated')->with('er_type','success ');
      }
    }


    public function manage_gallery($id)
    {

      //dd($id);
      $sub_service_id=$id;

      $service_images=DB::table('service_images')
        ->select('*')
        ->where('sub_service_id','=',$id)
        ->get();

      //dd($service_images);

      if($service_images){
        return view('tune_orbit_bo.masters.sub_services.manage_gallery')->with('service_images',$service_images)->with('sub_service_id',$sub_service_id);
      }else{

        // return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Cannot Manage Gallery Deactivated Sub Services')->with('er_type','danger');

        return view('tune_orbit_bo.masters.sub_services.manage_gallery')->with('sub_service_id',$sub_service_id);
      }
    }


    public function uploadserviceImg(Request $request)
    {

      if($request->file('file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('file_name');
        $tmpFilePath = 'tune_orbit_bo/image/tmp/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = $tmpFileName;

        $file_path=getSubServiceUploadedTmpPath($path);

        return response()->json(array('path'=> $file_path), 200);
      }else if($request->file('banner_file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('banner_file_name');
        $tmpFilePath = 'tune_orbit_bo/image/tmp/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = $tmpFileName;

        $file_path=getSubServiceUploadedTmpPath($path);

        return response()->json(array('path'=> $file_path), 200);
      }else if($request->file('thumbnail_file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('thumbnail_file_name');
        $tmpFilePath = 'tune_orbit_bo/image/tmp/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = $tmpFileName;

        $file_path=getSubServiceUploadedTmpPath($path);

        return response()->json(array('path'=> $file_path), 200);
      }else{
        return response()->json(false, 200);
      }
    }

    public function editUploadserviceImg(Request $request)
    {
      // dd($request->all());
      if($request->file('file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('file_name');
        $tmpFilePath = '../server/img/user_service/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = 'https://www.tuneorbit.com/server/img/user_service/'.$tmpFileName;

        $req['user_service_id']=$request->id;
        $req['image_path']=$tmpFileName;
        $user_service_img=UserServiceImages::create($req);

        return response()->json([array('path'=> $path),array('user_service_img_id'=> $user_service_img['id'])], 200);
      }else if($request->file('banner_file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('banner_file_name');
        $tmpFilePath = '../server/img/services/banners/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = 'https://www.tuneorbit.com/server/img/services/banners/'.$tmpFileName;

        DB::table('user_services')->where('id', '=', $request->id)->update(['banner_image' => $path]);

        return response()->json(array('path'=> $path), 200);
      }else if($request->file('thumbnail_file_name')) {

        //upload an image to the /img/tmp directory and return the filepath.
        $file = Input::file('thumbnail_file_name');
        $tmpFilePath = '../server/img/services/thumbnails/';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $tmpFileName = round(microtime(true) * 1000) . '.' . $extension;
        $file = $file->move($tmpFilePath, $tmpFileName);
        $path = 'https://www.tuneorbit.com/server/img/services/thumbnails/'.$tmpFileName;

        DB::table('user_services')->where('id', '=', $request->id)->update(['thumbnail_image' => $path]);

        return response()->json(array('path'=> $path), 200);
      }else{
        return response()->json(false, 200);
      }
    }


    public function updateServiceImages(Request $request, $id)
    {
       //dd($request->all());

      foreach(json_decode($request->image_array) as $ia){
        $request['sub_service_id']=$request->sub_service_id;
        $request['path']='/images/services/home/'.$ia;

        // Move all images files
        $source = "tune_orbit_bo/image/tmp/";
        // $destination = "tune_orbit_bo/image/service_gallery/";
        $destination = "../images/services/home/";
        if (copy($source.$ia, $destination.$ia)) {
          $delete[] = $source.$ia;
        }

        MasterServiceImages::create($request->all());
      }

      foreach ( $delete as $file ) {
        unlink( $file );
      }

      return redirect()->route('tune_orbit_bo.masters.sub_services.index')->with('message','Successfully Manage Gallery is created')->with('er_type','success');

    }



    public function deleteSpecificServiceImg(Request $request)
    {
      //dd($request->all());
      if($request->type==1){

        $services=DB::table('service_images')->where('id', '=', $request->id)->delete();


        $gallery_path='../images/services/home/'.$request->path;
        unlink( $gallery_path );

        return response()->json(2);
      
      }else{
        
        //dd($request->type);

        $temp_path='tune_orbit_bo/image/tmp/'.$request->path;

        unlink($temp_path);

        return response()->json(1);
      }
    }


}
