<?php

namespace App\Models\masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterServiceImages extends Model
{
      use SoftDeletes;
   protected $table='service_images';
   protected $dates = ['deleted_at'];

   protected $fillable = [
   'sub_service_id','path'
   ];
}
