<?php

namespace App\Models\masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditCheckpoint extends Model
{
   use SoftDeletes;
   protected $dates = ['deleted_at'];
   protected $fillable = ['checkpoint','mandatory_flag'];
}
