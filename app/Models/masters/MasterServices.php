<?php

namespace App\Models\masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterServices extends Model
{
      use SoftDeletes;
   protected $table='services';
   protected $dates = ['deleted_at'];

   protected $fillable = [
   'name','description','thumbnail_path','created_by','updated_by'
   ];
}
