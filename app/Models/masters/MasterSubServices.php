<?php

namespace App\Models\masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterSubServices extends Model
{
      use SoftDeletes;
   protected $table='sub_services';
   protected $dates = ['deleted_at'];

   protected $fillable = [
   'name','service_id','description','thumbnail_path','created_by','updated_by'
   ];
}
