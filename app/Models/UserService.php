<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserService extends Model
{
    use SoftDeletes;
    protected $table='user_services';
    protected $dates = ['deleted_at'];
    protected $fillable = array('user_id','sub_service_id','business_name','phone_number','website','address','locality','location_lat','location_long','from_time','to_time','working_days','about','banner_image','thumbnail_image','open_24x7','commit_flag','audit_flag','in_website','zip_code');

    protected $casts = [
        'working_days' => 'array', // Will convarted to (Array)
        'service_images' => 'array',
    ];

	public function getFromTimeAttribute($value)
    {	
    	 
        return date('g:i a', strtotime($value));
    }
    public function getToTimeAttribute($value)
    {	
    	 
        return date('g:i a', strtotime($value));
    }
}
