<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserServiceImages extends Model
{
    protected $table='user_service_images';
    protected $dates = ['deleted_at'];
    protected $fillable = array('user_service_id','image_path');
}
