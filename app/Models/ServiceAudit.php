<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class ServiceAudit extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['user_service_id','audit_checkpoint_id','remarks','created_by','updated_by','check_flag'];

public function storeServiceAudit($checkpoint_array,$user_service_id){

    foreach($checkpoint_array as $k) {

        $req['created_by']=Auth::user()->id;
        $req['user_service_id']=$user_service_id;
        $req['audit_checkpoint_id']=$k->audit_checkpoint_id;
        $req['check_flag']=$k->select_flag;
        $this->create($req);
      }

      return;
}

}
