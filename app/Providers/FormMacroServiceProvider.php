<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;
use DB;
use View;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
       
        //Services Select
        Form::macro('servicesSelect', function($name,$selected)
        {
            $services=['0' => 'Select Services'] + DB::table('services')->where('deleted_at','=',NULL)->lists('name', 'id');
            return Form::select($name, $services, $selected, ['class' => 'form-control required','id'=>'service_id','notequal'=>'0','data-live-search'=>'true']);
        });
        

        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
