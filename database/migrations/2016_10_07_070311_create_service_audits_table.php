<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_service_id')->unsigned();
            $table->integer('audit_checkpoint_id');
            $table->string('remarks');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_service_id')->references('id')->on('user_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_audits');
    }
}
